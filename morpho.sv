<?xml version="1.0" encoding="UTF-8"?>
<description lang="sv">
  <!-- INVARIABLE -->
  <table name="inv" rads="..*" fast="-">
    <form suffix="" tag=""/>
  </table>
  <table name="pref" rads="..*" fast="-">
    <form suffix="_" tag=""/>
  </table>
  <table name="AB1" rads=".*">
    <!-- 533 members -->
    <form  suffix="" tag="ABinvar"/>
  </table>
  <table name="AB2" rads=".*">
    <!-- 508 members -->
    <form  suffix="" tag="ABc"/>
    <form  suffix="" tag="ABinvar"/>
    <form  suffix="-" tag="ABc"/>
    <form  suffix="-" tag="ABsms"/>
  </table>
  <table name="AB3" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="" tag="ABsuper"/>
  </table>
  <table name="AB4" rads=".*(tidigare|vidare|senare)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="ABkomp"/>
  </table>
  <table name="ABA1" rads=".*" fast="-">
    <!-- 21 members -->
    <form  suffix="" tag="ABAinvar"/>
  </table>
  <table name="AV1" rads=".*">
    <!-- 5508 members -->
    <form  suffix="" tag="AVc"/>
    <form  suffix="" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="-" tag="AVc"/>
    <form  suffix="-" tag="AVsms"/>
    <form  suffix="a" tag="AVpos,def,pl,nom"/>
    <form  suffix="a" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="a" tag="AVpos,indef,pl,nom"/>
    <form  suffix="are" tag="AVkomp,nom"/>
    <form  suffix="ares" tag="AVkomp,gen"/>
    <form  suffix="as" tag="AVpos,def,pl,gen"/>
    <form  suffix="as" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="as" tag="AVpos,indef,pl,gen"/>
    <form  suffix="ast" tag="AVsuper,indef,nom"/>
    <form  suffix="aste" tag="AVsuper,def,masc,nom"/>
    <form  suffix="aste" tag="AVsuper,def,no_masc,nom"/>
    <form  suffix="astes" tag="AVsuper,def,masc,gen"/>
    <form  suffix="astes" tag="AVsuper,def,no_masc,gen"/>
    <form  suffix="asts" tag="AVsuper,indef,gen"/>
    <form  suffix="e" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="es" tag="AVpos,def,sg,masc,gen"/>
    <form  suffix="s" tag="AVpos,indef,sg,u,gen"/>
    <form  suffix="t" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="ts" tag="AVpos,indef,sg,n,gen"/>
  </table>
  <table name="AV2" rads=".*">
    <!-- 1208 members -->
    <form  suffix="d" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="de" tag="AVpos,def,pl,nom"/>
    <form  suffix="de" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="de" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="de" tag="AVpos,indef,pl,nom"/>
    <form  suffix="des" tag="AVpos,def,pl,gen"/>
    <form  suffix="des" tag="AVpos,def,sg,masc,gen"/>
    <form  suffix="des" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="des" tag="AVpos,indef,pl,gen"/>
    <form  suffix="ds" tag="AVpos,indef,sg,u,gen"/>
    <form  suffix="t" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="ts" tag="AVpos,indef,sg,n,gen"/>
  </table>
  <table name="AV3" rads=".*">
    <!-- 1061 members -->
    <form  suffix="" tag="AVinvar"/>
  </table>
  <table name="AV4" rads=".*">
    <!-- 590 members -->
    <form  suffix="" tag="AVc"/>
    <form  suffix="" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="-" tag="AVc"/>
    <form  suffix="-" tag="AVsms"/>
    <form  suffix="a" tag="AVpos,def,pl,nom"/>
    <form  suffix="a" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="a" tag="AVpos,indef,pl,nom"/>
    <form  suffix="are" tag="AVkomp,nom"/>
    <form  suffix="ares" tag="AVkomp,gen"/>
    <form  suffix="as" tag="AVpos,def,pl,gen"/>
    <form  suffix="as" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="as" tag="AVpos,indef,pl,gen"/>
    <form  suffix="ast" tag="AVsuper,indef,nom"/>
    <form  suffix="aste" tag="AVsuper,def,masc,nom"/>
    <form  suffix="aste" tag="AVsuper,def,no_masc,nom"/>
    <form  suffix="astes" tag="AVsuper,def,masc,gen"/>
    <form  suffix="astes" tag="AVsuper,def,no_masc,gen"/>
    <form  suffix="asts" tag="AVsuper,indef,gen"/>
    <form  suffix="e" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="es" tag="AVpos,def,sg,masc,gen"/>
    <form  suffix="s" tag="AVpos,indef,sg,n,gen"/>
    <form  suffix="s" tag="AVpos,indef,sg,u,gen"/>
  </table>
  <table name="AV5" rads=".*">
    <!-- 552 members -->
    <form  suffix="en" tag="AVc"/>
    <form  suffix="en" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="en-" tag="AVc"/>
    <form  suffix="en-" tag="AVsms"/>
    <form  suffix="ens" tag="AVpos,indef,sg,u,gen"/>
    <form  suffix="et" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="ets" tag="AVpos,indef,sg,n,gen"/>
    <form  suffix="na" tag="AVpos,def,pl,nom"/>
    <form  suffix="na" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="na" tag="AVpos,indef,pl,nom"/>
    <form  suffix="nare" tag="AVkomp,nom"/>
    <form  suffix="nares" tag="AVkomp,gen"/>
    <form  suffix="nas" tag="AVpos,def,pl,gen"/>
    <form  suffix="nas" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="nas" tag="AVpos,indef,pl,gen"/>
    <form  suffix="nast" tag="AVsuper,indef,nom"/>
    <form  suffix="naste" tag="AVsuper,def,masc,nom"/>
    <form  suffix="naste" tag="AVsuper,def,no_masc,nom"/>
    <form  suffix="nastes" tag="AVsuper,def,masc,gen"/>
    <form  suffix="nastes" tag="AVsuper,def,no_masc,gen"/>
    <form  suffix="nasts" tag="AVsuper,indef,gen"/>
    <form  suffix="ne" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="nes" tag="AVpos,def,sg,masc,gen"/>
  </table>
  <table name="AV6" rads=".*">
    <!-- 473 members -->
    <form  suffix="d" tag="AVc"/>
    <form  suffix="d" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="d-" tag="AVc"/>
    <form  suffix="d-" tag="AVsms"/>
    <form  suffix="da" tag="AVpos,def,pl,nom"/>
    <form  suffix="da" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="da" tag="AVpos,indef,pl,nom"/>
    <form  suffix="dare" tag="AVkomp,nom"/>
    <form  suffix="dares" tag="AVkomp,gen"/>
    <form  suffix="das" tag="AVpos,def,pl,gen"/>
    <form  suffix="das" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="das" tag="AVpos,indef,pl,gen"/>
    <form  suffix="dast" tag="AVsuper,indef,nom"/>
    <form  suffix="daste" tag="AVsuper,def,masc,nom"/>
    <form  suffix="daste" tag="AVsuper,def,no_masc,nom"/>
    <form  suffix="dastes" tag="AVsuper,def,masc,gen"/>
    <form  suffix="dastes" tag="AVsuper,def,no_masc,gen"/>
    <form  suffix="dasts" tag="AVsuper,indef,gen"/>
    <form  suffix="de" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="des" tag="AVpos,def,sg,masc,gen"/>
    <form  suffix="ds" tag="AVpos,indef,sg,u,gen"/>
    <form  suffix="t" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="ts" tag="AVpos,indef,sg,n,gen"/>
  </table>
  <table name="AV7" rads=".*">
    <!-- 304 members -->
    <form  suffix="" tag="AVc"/>
    <form  suffix="" tag="AVpos,indef,sg,u,gen"/>
    <form  suffix="" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="-" tag="AVc"/>
    <form  suffix="-" tag="AVsms"/>
    <form  suffix="a" tag="AVpos,def,pl,nom"/>
    <form  suffix="a" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="a" tag="AVpos,indef,pl,nom"/>
    <form  suffix="are" tag="AVkomp,nom"/>
    <form  suffix="ares" tag="AVkomp,gen"/>
    <form  suffix="as" tag="AVpos,def,pl,gen"/>
    <form  suffix="as" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="as" tag="AVpos,indef,pl,gen"/>
    <form  suffix="ast" tag="AVsuper,indef,nom"/>
    <form  suffix="aste" tag="AVsuper,def,masc,nom"/>
    <form  suffix="aste" tag="AVsuper,def,no_masc,nom"/>
    <form  suffix="astes" tag="AVsuper,def,masc,gen"/>
    <form  suffix="astes" tag="AVsuper,def,no_masc,gen"/>
    <form  suffix="asts" tag="AVsuper,indef,gen"/>
    <form  suffix="e" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="es" tag="AVpos,def,sg,masc,gen"/>
    <form  suffix="t" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="ts" tag="AVpos,indef,sg,n,gen"/>
  </table>
  <table name="AV8" rads=".*">
    <!-- 144 members -->
    <form  suffix="" tag="AVc"/>
    <form  suffix="" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="-" tag="AVc"/>
    <form  suffix="-" tag="AVsms"/>
    <form  suffix="ma" tag="AVpos,def,pl,nom"/>
    <form  suffix="ma" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="ma" tag="AVpos,indef,pl,nom"/>
    <form  suffix="mare" tag="AVkomp,nom"/>
    <form  suffix="mares" tag="AVkomp,gen"/>
    <form  suffix="mas" tag="AVpos,def,pl,gen"/>
    <form  suffix="mas" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="mas" tag="AVpos,indef,pl,gen"/>
    <form  suffix="mast" tag="AVsuper,indef,nom"/>
    <form  suffix="maste" tag="AVsuper,def,masc,nom"/>
    <form  suffix="maste" tag="AVsuper,def,no_masc,nom"/>
    <form  suffix="mastes" tag="AVsuper,def,masc,gen"/>
    <form  suffix="mastes" tag="AVsuper,def,no_masc,gen"/>
    <form  suffix="masts" tag="AVsuper,indef,gen"/>
    <form  suffix="me" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="mes" tag="AVpos,def,sg,masc,gen"/>
    <form  suffix="s" tag="AVpos,indef,sg,u,gen"/>
    <form  suffix="t" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="ts" tag="AVpos,indef,sg,n,gen"/>
  </table>
  <table name="AV9" rads=".*">
    <!-- 106 members -->
    <form  suffix="dd" tag="AVc"/>
    <form  suffix="dd" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="dd-" tag="AVc"/>
    <form  suffix="dd-" tag="AVsms"/>
    <form  suffix="dda" tag="AVpos,def,pl,nom"/>
    <form  suffix="dda" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="dda" tag="AVpos,indef,pl,nom"/>
    <form  suffix="ddare" tag="AVkomp,nom"/>
    <form  suffix="ddares" tag="AVkomp,gen"/>
    <form  suffix="ddas" tag="AVpos,def,pl,gen"/>
    <form  suffix="ddas" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="ddas" tag="AVpos,indef,pl,gen"/>
    <form  suffix="ddast" tag="AVsuper,indef,nom"/>
    <form  suffix="ddaste" tag="AVsuper,def,masc,nom"/>
    <form  suffix="ddaste" tag="AVsuper,def,no_masc,nom"/>
    <form  suffix="ddastes" tag="AVsuper,def,masc,gen"/>
    <form  suffix="ddastes" tag="AVsuper,def,no_masc,gen"/>
    <form  suffix="ddasts" tag="AVsuper,indef,gen"/>
    <form  suffix="dde" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="ddes" tag="AVpos,def,sg,masc,gen"/>
    <form  suffix="dds" tag="AVpos,indef,sg,u,gen"/>
    <form  suffix="tt" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="tts" tag="AVpos,indef,sg,n,gen"/>
  </table>
  <table name="AV10" rads=".*">
    <!-- 87 members -->
    <form  suffix="el" tag="AVc"/>
    <form  suffix="el" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="el-" tag="AVc"/>
    <form  suffix="el-" tag="AVsms"/>
    <form  suffix="els" tag="AVpos,indef,sg,u,gen"/>
    <form  suffix="elt" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="elts" tag="AVpos,indef,sg,n,gen"/>
    <form  suffix="la" tag="AVpos,def,pl,nom"/>
    <form  suffix="la" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="la" tag="AVpos,indef,pl,nom"/>
    <form  suffix="lare" tag="AVkomp,nom"/>
    <form  suffix="lares" tag="AVkomp,gen"/>
    <form  suffix="las" tag="AVpos,def,pl,gen"/>
    <form  suffix="las" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="las" tag="AVpos,indef,pl,gen"/>
    <form  suffix="last" tag="AVsuper,indef,nom"/>
    <form  suffix="laste" tag="AVsuper,def,masc,nom"/>
    <form  suffix="laste" tag="AVsuper,def,no_masc,nom"/>
    <form  suffix="lastes" tag="AVsuper,def,masc,gen"/>
    <form  suffix="lastes" tag="AVsuper,def,no_masc,gen"/>
    <form  suffix="lasts" tag="AVsuper,indef,gen"/>
    <form  suffix="le" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="les" tag="AVpos,def,sg,masc,gen"/>
  </table>
  <table name="AV11" rads=".*">
    <!-- 81 members -->
    <form  suffix="" tag="AVc"/>
    <form  suffix="" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="-" tag="AVc"/>
    <form  suffix="-" tag="AVsms"/>
    <form  suffix="a" tag="AVpos,def,pl,nom"/>
    <form  suffix="a" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="a" tag="AVpos,indef,pl,nom"/>
    <form  suffix="are" tag="AVkomp,nom"/>
    <form  suffix="ares" tag="AVkomp,gen"/>
    <form  suffix="as" tag="AVpos,def,pl,gen"/>
    <form  suffix="as" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="as" tag="AVpos,indef,pl,gen"/>
    <form  suffix="ast" tag="AVsuper,indef,nom"/>
    <form  suffix="aste" tag="AVsuper,def,masc,nom"/>
    <form  suffix="aste" tag="AVsuper,def,no_masc,nom"/>
    <form  suffix="astes" tag="AVsuper,def,masc,gen"/>
    <form  suffix="astes" tag="AVsuper,def,no_masc,gen"/>
    <form  suffix="asts" tag="AVsuper,indef,gen"/>
    <form  suffix="e" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="es" tag="AVpos,def,sg,masc,gen"/>
    <form  suffix="s" tag="AVpos,indef,sg,u,gen"/>
    <form  suffix="tt" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="tts" tag="AVpos,indef,sg,n,gen"/>
  </table>
  <table name="AV12" rads=".*">
    <!-- 74 members -->
    <form  suffix="" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="a" tag="AVpos,def,pl,nom"/>
    <form  suffix="a" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="a" tag="AVpos,indef,pl,nom"/>
    <form  suffix="as" tag="AVpos,def,pl,gen"/>
    <form  suffix="as" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="as" tag="AVpos,indef,pl,gen"/>
    <form  suffix="e" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="es" tag="AVpos,def,sg,masc,gen"/>
    <form  suffix="s" tag="AVpos,indef,sg,u,gen"/>
    <form  suffix="t" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="ts" tag="AVpos,indef,sg,n,gen"/>
  </table>
  <table name="AV13" rads=".*">
    <!-- 68 members -->
    <form  suffix="er" tag="AVc"/>
    <form  suffix="er" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="er-" tag="AVc"/>
    <form  suffix="er-" tag="AVsms"/>
    <form  suffix="ers" tag="AVpos,indef,sg,u,gen"/>
    <form  suffix="ert" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="erts" tag="AVpos,indef,sg,n,gen"/>
    <form  suffix="ra" tag="AVpos,def,pl,nom"/>
    <form  suffix="ra" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="ra" tag="AVpos,indef,pl,nom"/>
    <form  suffix="rare" tag="AVkomp,nom"/>
    <form  suffix="rares" tag="AVkomp,gen"/>
    <form  suffix="ras" tag="AVpos,def,pl,gen"/>
    <form  suffix="ras" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="ras" tag="AVpos,indef,pl,gen"/>
    <form  suffix="rast" tag="AVsuper,indef,nom"/>
    <form  suffix="raste" tag="AVsuper,def,masc,nom"/>
    <form  suffix="raste" tag="AVsuper,def,no_masc,nom"/>
    <form  suffix="rastes" tag="AVsuper,def,masc,gen"/>
    <form  suffix="rastes" tag="AVsuper,def,no_masc,gen"/>
    <form  suffix="rasts" tag="AVsuper,indef,gen"/>
    <form  suffix="re" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="res" tag="AVpos,def,sg,masc,gen"/>
  </table>
  <table name="AV14" rads=".*">
    <!-- 63 members -->
    <form  suffix="d" tag="AVc"/>
    <form  suffix="d" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="d-" tag="AVc"/>
    <form  suffix="d-" tag="AVsms"/>
    <form  suffix="da" tag="AVpos,def,pl,nom"/>
    <form  suffix="da" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="da" tag="AVpos,indef,pl,nom"/>
    <form  suffix="dare" tag="AVkomp,nom"/>
    <form  suffix="dares" tag="AVkomp,gen"/>
    <form  suffix="das" tag="AVpos,def,pl,gen"/>
    <form  suffix="das" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="das" tag="AVpos,indef,pl,gen"/>
    <form  suffix="dast" tag="AVsuper,indef,nom"/>
    <form  suffix="daste" tag="AVsuper,def,masc,nom"/>
    <form  suffix="daste" tag="AVsuper,def,no_masc,nom"/>
    <form  suffix="dastes" tag="AVsuper,def,masc,gen"/>
    <form  suffix="dastes" tag="AVsuper,def,no_masc,gen"/>
    <form  suffix="dasts" tag="AVsuper,indef,gen"/>
    <form  suffix="de" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="des" tag="AVpos,def,sg,masc,gen"/>
    <form  suffix="ds" tag="AVpos,indef,sg,u,gen"/>
    <form  suffix="tt" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="tts" tag="AVpos,indef,sg,n,gen"/>
  </table>
  <table name="AV15" rads=".*">
    <!-- 51 members -->
    <form  suffix="" tag="AVc"/>
    <form  suffix="" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="-" tag="AVc"/>
    <form  suffix="-" tag="AVsms"/>
    <form  suffix="a" tag="AVpos,def,pl,nom"/>
    <form  suffix="a" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="a" tag="AVpos,indef,pl,nom"/>
    <form  suffix="are" tag="AVkomp,nom"/>
    <form  suffix="ares" tag="AVkomp,gen"/>
    <form  suffix="as" tag="AVpos,def,pl,gen"/>
    <form  suffix="as" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="as" tag="AVpos,indef,pl,gen"/>
    <form  suffix="ast" tag="AVsuper,indef,nom"/>
    <form  suffix="aste" tag="AVsuper,def,masc,nom"/>
    <form  suffix="aste" tag="AVsuper,def,no_masc,nom"/>
    <form  suffix="astes" tag="AVsuper,def,masc,gen"/>
    <form  suffix="astes" tag="AVsuper,def,no_masc,gen"/>
    <form  suffix="asts" tag="AVsuper,indef,gen"/>
    <form  suffix="e" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="es" tag="AVpos,def,sg,masc,gen"/>
    <form  suffix="s" tag="AVpos,indef,sg,u,gen"/>
  </table>
  <table name="AV16" rads=".*">
    <!-- 42 members -->
    <form  suffix="" tag="AVc"/>
    <form  suffix="" tag="AVpos,def,pl,nom"/>
    <form  suffix="" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="" tag="AVpos,indef,pl,nom"/>
    <form  suffix="" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="-" tag="AVc"/>
    <form  suffix="-" tag="AVsms"/>
    <form  suffix="a" tag="AVpos,def,pl,nom"/>
    <form  suffix="a" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="a" tag="AVpos,indef,pl,nom"/>
    <form  suffix="are" tag="AVkomp,nom"/>
    <form  suffix="ares" tag="AVkomp,gen"/>
    <form  suffix="as" tag="AVpos,def,pl,gen"/>
    <form  suffix="as" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="as" tag="AVpos,indef,pl,gen"/>
    <form  suffix="ast" tag="AVsuper,indef,nom"/>
    <form  suffix="aste" tag="AVsuper,def,masc,nom"/>
    <form  suffix="aste" tag="AVsuper,def,no_masc,nom"/>
    <form  suffix="astes" tag="AVsuper,def,masc,gen"/>
    <form  suffix="astes" tag="AVsuper,def,no_masc,gen"/>
    <form  suffix="asts" tag="AVsuper,indef,gen"/>
    <form  suffix="e" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="es" tag="AVpos,def,sg,masc,gen"/>
    <form  suffix="s" tag="AVpos,def,pl,gen"/>
    <form  suffix="s" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="s" tag="AVpos,indef,pl,gen"/>
    <form  suffix="s" tag="AVpos,indef,sg,u,gen"/>
    <form  suffix="tt" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="tts" tag="AVpos,indef,sg,n,gen"/>
  </table>
  <table name="AV17" rads=".*" fast="-">
    <!-- 22 members -->
    <form  suffix="" tag="AVc"/>
    <form  suffix="" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="-" tag="AVc"/>
    <form  suffix="-" tag="AVsms"/>
    <form  suffix="a" tag="AVpos,def,pl,nom"/>
    <form  suffix="a" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="a" tag="AVpos,indef,pl,nom"/>
    <form  suffix="as" tag="AVpos,def,pl,gen"/>
    <form  suffix="as" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="as" tag="AVpos,indef,pl,gen"/>
    <form  suffix="e" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="es" tag="AVpos,def,sg,masc,gen"/>
    <form  suffix="s" tag="AVpos,indef,sg,n,gen"/>
    <form  suffix="s" tag="AVpos,indef,sg,u,gen"/>
  </table>
  <table name="AV18" rads=".*" fast="-">
    <!-- 22 members -->
    <form  suffix="n" tag="AVc"/>
    <form  suffix="n" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="n-" tag="AVc"/>
    <form  suffix="n-" tag="AVsms"/>
    <form  suffix="na" tag="AVpos,def,pl,nom"/>
    <form  suffix="na" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="na" tag="AVpos,indef,pl,nom"/>
    <form  suffix="nare" tag="AVkomp,nom"/>
    <form  suffix="nares" tag="AVkomp,gen"/>
    <form  suffix="nas" tag="AVpos,def,pl,gen"/>
    <form  suffix="nas" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="nas" tag="AVpos,indef,pl,gen"/>
    <form  suffix="nast" tag="AVsuper,indef,nom"/>
    <form  suffix="naste" tag="AVsuper,def,masc,nom"/>
    <form  suffix="naste" tag="AVsuper,def,no_masc,nom"/>
    <form  suffix="nastes" tag="AVsuper,def,masc,gen"/>
    <form  suffix="nastes" tag="AVsuper,def,no_masc,gen"/>
    <form  suffix="nasts" tag="AVsuper,indef,gen"/>
    <form  suffix="ne" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="nes" tag="AVpos,def,sg,masc,gen"/>
    <form  suffix="ns" tag="AVpos,indef,sg,u,gen"/>
    <form  suffix="t" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="ts" tag="AVpos,indef,sg,n,gen"/>
  </table>
  <table name="AV19" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="en" tag="AVc"/>
    <form  suffix="a" tag="AVpos,def,pl,nom"/>
    <form  suffix="a" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="a" tag="AVpos,indef,pl,nom"/>
    <form  suffix="are" tag="AVkomp,nom"/>
    <form  suffix="ares" tag="AVkomp,gen"/>
    <form  suffix="as" tag="AVpos,def,pl,gen"/>
    <form  suffix="as" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="as" tag="AVpos,indef,pl,gen"/>
    <form  suffix="ast" tag="AVsuper,indef,nom"/>
    <form  suffix="aste" tag="AVsuper,def,masc,nom"/>
    <form  suffix="aste" tag="AVsuper,def,no_masc,nom"/>
    <form  suffix="astes" tag="AVsuper,def,masc,gen"/>
    <form  suffix="astes" tag="AVsuper,def,no_masc,gen"/>
    <form  suffix="asts" tag="AVsuper,indef,gen"/>
    <form  suffix="e" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="en" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="en-" tag="AVc"/>
    <form  suffix="en-" tag="AVsms"/>
    <form  suffix="ens" tag="AVpos,indef,sg,u,gen"/>
    <form  suffix="es" tag="AVpos,def,sg,masc,gen"/>
    <form  suffix="et" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="ets" tag="AVpos,indef,sg,n,gen"/>
  </table>
  <table name="AV20" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="en" tag="AVc"/>
    <form  suffix="en" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="en-" tag="AVc"/>
    <form  suffix="en-" tag="AVsms"/>
    <form  suffix="ens" tag="AVpos,indef,sg,u,gen"/>
    <form  suffix="et" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="ets" tag="AVpos,indef,sg,n,gen"/>
    <form  suffix="na" tag="AVpos,def,pl,nom"/>
    <form  suffix="na" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="na" tag="AVpos,indef,pl,nom"/>
    <form  suffix="nas" tag="AVpos,def,pl,gen"/>
    <form  suffix="nas" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="nas" tag="AVpos,indef,pl,gen"/>
    <form  suffix="ne" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="nes" tag="AVpos,def,sg,masc,gen"/>
  </table>
  <table name="AV21" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="men" tag="AVc"/>
    <form  suffix="men" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="men-" tag="AVc"/>
    <form  suffix="men-" tag="AVsms"/>
    <form  suffix="mens" tag="AVpos,indef,sg,u,gen"/>
    <form  suffix="met" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="mets" tag="AVpos,indef,sg,n,gen"/>
    <form  suffix="na" tag="AVpos,def,pl,nom"/>
    <form  suffix="na" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="na" tag="AVpos,indef,pl,nom"/>
    <form  suffix="nare" tag="AVkomp,nom"/>
    <form  suffix="nares" tag="AVkomp,gen"/>
    <form  suffix="nas" tag="AVpos,def,pl,gen"/>
    <form  suffix="nas" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="nas" tag="AVpos,indef,pl,gen"/>
    <form  suffix="nast" tag="AVsuper,indef,nom"/>
    <form  suffix="naste" tag="AVsuper,def,masc,nom"/>
    <form  suffix="naste" tag="AVsuper,def,no_masc,nom"/>
    <form  suffix="nastes" tag="AVsuper,def,masc,gen"/>
    <form  suffix="nastes" tag="AVsuper,def,no_masc,gen"/>
    <form  suffix="nasts" tag="AVsuper,indef,gen"/>
    <form  suffix="ne" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="nes" tag="AVpos,def,sg,masc,gen"/>
  </table>
  <table name="AV22" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="a" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="as" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="e" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="es" tag="AVpos,def,sg,masc,gen"/>
  </table>
  <table name="AV23" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="mal" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="la" tag="AVpos,def,pl,nom"/>
    <form  suffix="la" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="la" tag="AVpos,indef,pl,nom"/>
    <form  suffix="las" tag="AVpos,def,pl,gen"/>
    <form  suffix="las" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="las" tag="AVpos,indef,pl,gen"/>
    <form  suffix="le" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="les" tag="AVpos,def,sg,masc,gen"/>
    <form  suffix="mals" tag="AVpos,indef,sg,u,gen"/>
    <form  suffix="malt" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="malts" tag="AVpos,indef,sg,n,gen"/>
  </table>
  <table name="AV24" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="" tag="AVpos,indef,sg,n,nom"/>
  </table>
  <table name="AV25" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="re" tag="AVkomp,nom"/>
    <form  suffix="ersta" tag="AVsuper,def,no_masc,nom"/>
    <form  suffix="ersta" tag="AVsuper,indef,nom"/>
    <form  suffix="erstas" tag="AVsuper,def,no_masc,gen"/>
    <form  suffix="erstas" tag="AVsuper,indef,gen"/>
    <form  suffix="erste" tag="AVsuper,def,masc,nom"/>
    <form  suffix="erstes" tag="AVsuper,def,masc,gen"/>
    <form  suffix="res" tag="AVkomp,gen"/>
  </table>
  <table name="AV26" rads=".*(norra|västra|östra|nordvästra|sydöstra|södra)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="AVpos,def"/>
  </table>
  <table name="AV27" rads=".*(innersta|mittersta|allernådigste|mellersta)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="AVsuper,indef,nom"/>
    <form  suffix="s" tag="AVsuper,indef,gen"/>
  </table>
  <table name="AV28" rads=".*(förbemälde|förstbemälde|bemälde)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="AVpos,def,sg,masc,nom"/>
  </table>
  <table name="AV29" rads=".*(orang|smutsorang|knallorang)" fast="-">
    <!-- 3 members -->
    <form  suffix="e" tag="AVc"/>
    <form  suffix="a" tag="AVpos,def,pl,nom"/>
    <form  suffix="a" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="a" tag="AVpos,indef,pl,nom"/>
    <form  suffix="are" tag="AVkomp,nom"/>
    <form  suffix="ares" tag="AVkomp,gen"/>
    <form  suffix="as" tag="AVpos,def,pl,gen"/>
    <form  suffix="as" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="as" tag="AVpos,indef,pl,gen"/>
    <form  suffix="ast" tag="AVsuper,indef,nom"/>
    <form  suffix="aste" tag="AVsuper,def,masc,nom"/>
    <form  suffix="aste" tag="AVsuper,def,no_masc,nom"/>
    <form  suffix="astes" tag="AVsuper,def,masc,gen"/>
    <form  suffix="astes" tag="AVsuper,def,no_masc,gen"/>
    <form  suffix="asts" tag="AVsuper,indef,gen"/>
    <form  suffix="e" tag="AVpos,def,pl,nom"/>
    <form  suffix="e" tag="AVpos,def,sg,masc,nom"/>
    <form  suffix="e" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="e" tag="AVpos,indef,pl,nom"/>
    <form  suffix="e" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="e" tag="AVpos,indef,sg,u,nom"/>
    <form  suffix="e-" tag="AVc"/>
    <form  suffix="e-" tag="AVsms"/>
    <form  suffix="ea" tag="AVpos,def,pl,nom"/>
    <form  suffix="ea" tag="AVpos,def,sg,no_masc,nom"/>
    <form  suffix="ea" tag="AVpos,indef,pl,nom"/>
    <form  suffix="eare" tag="AVkomp,nom"/>
    <form  suffix="eares" tag="AVkomp,gen"/>
    <form  suffix="eas" tag="AVpos,def,pl,gen"/>
    <form  suffix="eas" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="eas" tag="AVpos,indef,pl,gen"/>
    <form  suffix="east" tag="AVsuper,indef,nom"/>
    <form  suffix="easte" tag="AVsuper,def,masc,nom"/>
    <form  suffix="easte" tag="AVsuper,def,no_masc,nom"/>
    <form  suffix="eastes" tag="AVsuper,def,masc,gen"/>
    <form  suffix="eastes" tag="AVsuper,def,no_masc,gen"/>
    <form  suffix="easts" tag="AVsuper,indef,gen"/>
    <form  suffix="es" tag="AVpos,def,pl,gen"/>
    <form  suffix="es" tag="AVpos,def,sg,masc,gen"/>
    <form  suffix="es" tag="AVpos,def,sg,no_masc,gen"/>
    <form  suffix="es" tag="AVpos,indef,pl,gen"/>
    <form  suffix="es" tag="AVpos,indef,sg,n,gen"/>
    <form  suffix="es" tag="AVpos,indef,sg,u,gen"/>
    <form  suffix="et" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="ets" tag="AVpos,indef,sg,n,gen"/>
    <form  suffix="t" tag="AVpos,indef,sg,n,nom"/>
    <form  suffix="ts" tag="AVpos,indef,sg,n,gen"/>
  </table>
  <table name="AVA1" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="" tag="AVAinvar"/>
    <form  suffix="-" tag="AVAc"/>
    <form  suffix="-" tag="AVAsms"/>
  </table>
  <table name="AVH1" rads=".*[il][gl]" fast="-">
    <!-- 11 members -->
    <form  suffix="" tag="AVHc"/>
    <form  suffix="" tag="AVHpos,indef,sg,u,nom"/>
    <form  suffix="-" tag="AVHc"/>
    <form  suffix="-" tag="AVHsms"/>
    <form  suffix="a" tag="AVHpos,def,pl,nom"/>
    <form  suffix="a" tag="AVHpos,def,sg,no_masc,nom"/>
    <form  suffix="a" tag="AVHpos,indef,pl,nom"/>
    <form  suffix="are" tag="AVHkomp,nom"/>
    <form  suffix="ares" tag="AVHkomp,gen"/>
    <form  suffix="as" tag="AVHpos,def,pl,gen"/>
    <form  suffix="as" tag="AVHpos,def,sg,no_masc,gen"/>
    <form  suffix="as" tag="AVHpos,indef,pl,gen"/>
    <form  suffix="ast" tag="AVHsuper,indef,nom"/>
    <form  suffix="aste" tag="AVHsuper,def,masc,nom"/>
    <form  suffix="aste" tag="AVHsuper,def,no_masc,nom"/>
    <form  suffix="astes" tag="AVHsuper,def,masc,gen"/>
    <form  suffix="astes" tag="AVHsuper,def,no_masc,gen"/>
    <form  suffix="asts" tag="AVHsuper,indef,gen"/>
    <form  suffix="e" tag="AVHpos,def,sg,masc,nom"/>
    <form  suffix="es" tag="AVHpos,def,sg,masc,gen"/>
    <form  suffix="s" tag="AVHpos,indef,sg,u,gen"/>
    <form  suffix="t" tag="AVHpos,indef,sg,n,nom"/>
    <form  suffix="ts" tag="AVHpos,indef,sg,n,gen"/>
  </table>
  <table name="IN1" rads=".*">
    <!-- 178 members -->
    <form  suffix="" tag="INinvar"/>
  </table>
  <table name="KN1" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="" tag="KNinvar"/>
  </table>
  <table name="KNA1" rads=".*(resp|\&|o)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="KNAinvar"/>
    <form  suffix="-" tag="KNAc"/>
    <form  suffix="-" tag="KNAsms"/>
  </table>
  <table name="MXC1" rads=".*" fast="-">
    <!-- 21 members -->
    <form  suffix="" tag="MXCc"/>
    <form  suffix="-" tag="MXCc"/>
    <form  suffix="-" tag="MXCsms"/>
  </table>
  <table name="NL1" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="" tag="NLc"/>
    <form  suffix="" tag="NLnom,num,n"/>
    <form  suffix="" tag="NLnom,num,u"/>
    <form  suffix="-" tag="NLc"/>
    <form  suffix="de" tag="NLc"/>
    <form  suffix="de" tag="NLnom,ord,masc"/>
    <form  suffix="de" tag="NLnom,ord,no_masc"/>
    <form  suffix="de-" tag="NLc"/>
    <form  suffix="des" tag="NLgen,ord,masc"/>
    <form  suffix="des" tag="NLgen,ord,no_masc"/>
    <form  suffix="s" tag="NLgen,num,n"/>
    <form  suffix="s" tag="NLgen,num,u"/>
  </table>
  <table name="NL2" rads=".*[tnsu][ijg][ou]" fast="-">
    <!-- 13 members -->
    <form  suffix="" tag="NLc"/>
    <form  suffix="" tag="NLnom,num,n"/>
    <form  suffix="" tag="NLnom,num,u"/>
    <form  suffix="-" tag="NLc"/>
    <form  suffix="nde" tag="NLc"/>
    <form  suffix="nde" tag="NLnom,ord,masc"/>
    <form  suffix="nde" tag="NLnom,ord,no_masc"/>
    <form  suffix="nde-" tag="NLc"/>
    <form  suffix="ndes" tag="NLgen,ord,masc"/>
    <form  suffix="ndes" tag="NLgen,ord,no_masc"/>
    <form  suffix="s" tag="NLgen,num,n"/>
    <form  suffix="s" tag="NLgen,num,u"/>
  </table>
  <table name="NL3" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="" tag="NLc"/>
    <form  suffix="" tag="NLnom,num,n"/>
    <form  suffix="" tag="NLnom,num,u"/>
    <form  suffix="-" tag="NLc"/>
    <form  suffix="s" tag="NLgen,num,n"/>
    <form  suffix="s" tag="NLgen,num,u"/>
  </table>
  <table name="NL4" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="" tag="NLnom,num,n"/>
    <form  suffix="" tag="NLnom,num,u"/>
  </table>
  <table name="NL5" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="" tag="NLc"/>
    <form  suffix="" tag="NLnom,num,n"/>
    <form  suffix="" tag="NLnom,num,u"/>
    <form  suffix="-" tag="NLc"/>
    <form  suffix="s" tag="NLgen,num,n"/>
    <form  suffix="s" tag="NLgen,num,u"/>
    <form  suffix="te" tag="NLc"/>
    <form  suffix="te" tag="NLnom,ord,masc"/>
    <form  suffix="te" tag="NLnom,ord,no_masc"/>
    <form  suffix="te-" tag="NLc"/>
    <form  suffix="tes" tag="NLgen,ord,masc"/>
    <form  suffix="tes" tag="NLgen,ord,no_masc"/>
  </table>
  <table name="NL6" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="NLgen,num,n"/>
    <form  suffix="" tag="NLgen,num,u"/>
    <form  suffix="" tag="NLnom,num,n"/>
    <form  suffix="" tag="NLnom,num,u"/>
  </table>
  <table name="NN1" rads=".*">
    <!-- 7076 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="n" tag="NNpl,indef,nom"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
    <form  suffix="ns" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="t" tag="NNsg,def,nom"/>
    <form  suffix="ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN2" rads=".*">
    <!-- 5366 members -->
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN3" rads=".*">
    <!-- 5097 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN4" rads=".*">
    <!-- 4226 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN5" rads=".*">
    <!-- 3583 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN6" rads=".*">
    <!-- 2469 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN7" rads=".*">
    <!-- 2365 members -->
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN8" rads=".*">
    <!-- 2092 members -->
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN9" rads=".*">
    <!-- 1975 members -->
    <form  suffix="e" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="e" tag="NNsg,indef,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="es" tag="NNpl,indef,gen"/>
    <form  suffix="es" tag="NNsg,indef,gen"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
  </table>
  <table name="NN10" rads=".*">
    <!-- 1168 members -->
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN11" rads=".*">
    <!-- 1091 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN12" rads=".*">
    <!-- 1007 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN13" rads=".*">
    <!-- 946 members -->
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="en" tag="NNpl,indef,nom"/>
    <form  suffix="ena" tag="NNpl,def,nom"/>
    <form  suffix="enas" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN14" rads=".*">
    <!-- 887 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN15" rads=".*">
    <!-- 718 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="r" tag="NNpl,indef,nom"/>
    <form  suffix="rna" tag="NNpl,def,nom"/>
    <form  suffix="rnas" tag="NNpl,def,gen"/>
    <form  suffix="rs" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN16" rads=".*">
    <!-- 653 members -->
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="e" tag="NNci"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e-" tag="NNci"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="e-" tag="NNsms"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN17" rads=".*">
    <!-- 637 members -->
    <form  suffix="el" tag="NNci"/>
    <form  suffix="el" tag="NNcm"/>
    <form  suffix="el" tag="NNsg,indef,nom"/>
    <form  suffix="el-" tag="NNci"/>
    <form  suffix="el-" tag="NNcm"/>
    <form  suffix="el-" tag="NNsms"/>
    <form  suffix="eln" tag="NNsg,def,nom"/>
    <form  suffix="elns" tag="NNsg,def,gen"/>
    <form  suffix="els" tag="NNcm"/>
    <form  suffix="els" tag="NNsg,indef,gen"/>
    <form  suffix="els-" tag="NNcm"/>
    <form  suffix="lar" tag="NNpl,indef,nom"/>
    <form  suffix="larna" tag="NNpl,def,nom"/>
    <form  suffix="larnas" tag="NNpl,def,gen"/>
    <form  suffix="lars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN18" rads=".*">
    <!-- 617 members -->
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN19" rads=".*[eä]">
    <!-- 579 members -->
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="n" tag="NNpl,indef,nom"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
    <form  suffix="ns" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
    <form  suffix="t" tag="NNsg,def,nom"/>
    <form  suffix="ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN20" rads=".*">
    <!-- 553 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN21" rads=".*">
    <!-- 503 members -->
    <form  suffix="e" tag="NNsg,indef,nom"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="es" tag="NNcm"/>
    <form  suffix="es" tag="NNsg,indef,gen"/>
    <form  suffix="es-" tag="NNcm"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN22" rads=".*">
    <!-- 491 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
  </table>
  <table name="NN23" rads=".*">
    <!-- 449 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,gen"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN24" rads=".*">
    <!-- 420 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
  </table>
  <table name="NN25" rads=".*">
    <!-- 411 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN26" rads=".*">
    <!-- 400 members -->
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN27" rads=".*">
    <!-- 393 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="t" tag="NNsg,def,nom"/>
    <form  suffix="ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN28" rads=".*">
    <!-- 359 members -->
    <form  suffix="er" tag="NNci"/>
    <form  suffix="er" tag="NNcm"/>
    <form  suffix="er" tag="NNsg,indef,nom"/>
    <form  suffix="er-" tag="NNci"/>
    <form  suffix="er-" tag="NNcm"/>
    <form  suffix="er-" tag="NNsms"/>
    <form  suffix="ern" tag="NNsg,def,nom"/>
    <form  suffix="erns" tag="NNsg,def,gen"/>
    <form  suffix="ers" tag="NNcm"/>
    <form  suffix="ers" tag="NNsg,indef,gen"/>
    <form  suffix="ers-" tag="NNcm"/>
    <form  suffix="rar" tag="NNpl,indef,nom"/>
    <form  suffix="rarna" tag="NNpl,def,nom"/>
    <form  suffix="rarnas" tag="NNpl,def,gen"/>
    <form  suffix="rars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN29" rads=".*">
    <!-- 358 members -->
    <form  suffix="e" tag="NNsg,indef,nom"/>
    <form  suffix="en" tag="NNpl,indef,nom"/>
    <form  suffix="ena" tag="NNpl,def,nom"/>
    <form  suffix="enas" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNpl,indef,gen"/>
    <form  suffix="es" tag="NNsg,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN30" rads=".*">
    <!-- 343 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN31" rads=".*">
    <!-- 323 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN32" rads=".*">
    <!-- 292 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="t" tag="NNsg,def,nom"/>
    <form  suffix="ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN33" rads=".*">
    <!-- 272 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN34" rads=".*">
    <!-- 262 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN35" rads=".*">
    <!-- 236 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN36" rads=".*">
    <!-- 211 members -->
    <form  suffix="er" tag="NNci"/>
    <form  suffix="er" tag="NNcm"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="er" tag="NNsg,indef,nom"/>
    <form  suffix="er-" tag="NNci"/>
    <form  suffix="er-" tag="NNcm"/>
    <form  suffix="er-" tag="NNsms"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="ers" tag="NNsg,indef,gen"/>
    <form  suffix="ren" tag="NNpl,def,nom"/>
    <form  suffix="rens" tag="NNpl,def,gen"/>
    <form  suffix="ret" tag="NNsg,def,nom"/>
    <form  suffix="rets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN37" rads=".*">
    <!-- 188 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN38" rads=".*">
    <!-- 174 members -->
    <form  suffix="el" tag="NNci"/>
    <form  suffix="el" tag="NNcm"/>
    <form  suffix="el" tag="NNpl,indef,nom"/>
    <form  suffix="el" tag="NNsg,indef,nom"/>
    <form  suffix="el-" tag="NNci"/>
    <form  suffix="el-" tag="NNcm"/>
    <form  suffix="el-" tag="NNsms"/>
    <form  suffix="els" tag="NNcm"/>
    <form  suffix="els" tag="NNpl,indef,gen"/>
    <form  suffix="els" tag="NNsg,indef,gen"/>
    <form  suffix="els-" tag="NNcm"/>
    <form  suffix="len" tag="NNpl,def,nom"/>
    <form  suffix="lens" tag="NNpl,def,gen"/>
    <form  suffix="let" tag="NNsg,def,nom"/>
    <form  suffix="lets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN39" rads=".*">
    <!-- 170 members -->
    <form  suffix="an" tag="NNci"/>
    <form  suffix="an" tag="NNcm"/>
    <form  suffix="an" tag="NNsg,indef,nom"/>
    <form  suffix="an-" tag="NNci"/>
    <form  suffix="an-" tag="NNcm"/>
    <form  suffix="an-" tag="NNsms"/>
    <form  suffix="annen" tag="NNsg,def,nom"/>
    <form  suffix="annens" tag="NNsg,def,gen"/>
    <form  suffix="ans" tag="NNcm"/>
    <form  suffix="ans" tag="NNsg,indef,gen"/>
    <form  suffix="ans-" tag="NNcm"/>
    <form  suffix="än" tag="NNpl,indef,nom"/>
    <form  suffix="ännen" tag="NNpl,def,nom"/>
    <form  suffix="ännens" tag="NNpl,def,gen"/>
    <form  suffix="äns" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN40" rads=".*">
    <!-- 161 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN41" rads=".*">
    <!-- 153 members -->
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="e" tag="NNci"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e-" tag="NNci"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="e-" tag="NNsms"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN42" rads=".*">
    <!-- 153 members -->
    <form  suffix="e" tag="NNci"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e" tag="NNsg,indef,nom"/>
    <form  suffix="e-" tag="NNci"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="e-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="es" tag="NNcm"/>
    <form  suffix="es" tag="NNsg,indef,gen"/>
    <form  suffix="es-" tag="NNcm"/>
  </table>
  <table name="NN43" rads=".*">
    <!-- 150 members -->
    <form  suffix="" tag="NNsg,def,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN44" rads=".*">
    <!-- 146 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN45" rads=".*">
    <!-- 139 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN46" rads=".*">
    <!-- 135 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="mar" tag="NNpl,indef,nom"/>
    <form  suffix="marna" tag="NNpl,def,nom"/>
    <form  suffix="marnas" tag="NNpl,def,gen"/>
    <form  suffix="mars" tag="NNpl,indef,gen"/>
    <form  suffix="men" tag="NNsg,def,nom"/>
    <form  suffix="mens" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN47" rads=".*">
    <!-- 135 members -->
    <form  suffix="en" tag="NNci"/>
    <form  suffix="en" tag="NNcm"/>
    <form  suffix="en" tag="NNpl,indef,nom"/>
    <form  suffix="en" tag="NNsg,indef,nom"/>
    <form  suffix="en-" tag="NNci"/>
    <form  suffix="en-" tag="NNcm"/>
    <form  suffix="en-" tag="NNsms"/>
    <form  suffix="ens" tag="NNcm"/>
    <form  suffix="ens" tag="NNpl,indef,gen"/>
    <form  suffix="ens" tag="NNsg,indef,gen"/>
    <form  suffix="ens-" tag="NNcm"/>
    <form  suffix="nen" tag="NNpl,def,nom"/>
    <form  suffix="nens" tag="NNpl,def,gen"/>
    <form  suffix="net" tag="NNsg,def,nom"/>
    <form  suffix="nets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN48" rads=".*">
    <!-- 129 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN49" rads=".*">
    <!-- 128 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN50" rads=".*">
    <!-- 128 members -->
    <form  suffix="um" tag="NNci"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="um" tag="NNcm"/>
    <form  suffix="um" tag="NNsg,indef,nom"/>
    <form  suffix="um-" tag="NNci"/>
    <form  suffix="um-" tag="NNcm"/>
    <form  suffix="um-" tag="NNsms"/>
    <form  suffix="ums" tag="NNcm"/>
    <form  suffix="ums" tag="NNsg,indef,gen"/>
    <form  suffix="ums-" tag="NNcm"/>
  </table>
  <table name="NN51" rads=".*">
    <!-- 127 members -->
    <form  suffix="er" tag="NNci"/>
    <form  suffix="er" tag="NNcm"/>
    <form  suffix="er" tag="NNsg,indef,nom"/>
    <form  suffix="er-" tag="NNci"/>
    <form  suffix="er-" tag="NNcm"/>
    <form  suffix="er-" tag="NNsms"/>
    <form  suffix="ers" tag="NNsg,indef,gen"/>
    <form  suffix="ret" tag="NNsg,def,nom"/>
    <form  suffix="rets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN52" rads=".*">
    <!-- 125 members -->
    <form  suffix="a" tag="NNci"/>
    <form  suffix="a" tag="NNcm"/>
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="a-" tag="NNci"/>
    <form  suffix="a-" tag="NNcm"/>
    <form  suffix="a-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN53" rads=".*">
    <!-- 116 members -->
    <form  suffix="e" tag="NNsg,indef,nom"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="es" tag="NNcm"/>
    <form  suffix="es" tag="NNsg,indef,gen"/>
    <form  suffix="es-" tag="NNcm"/>
  </table>
  <table name="NN54" rads=".*">
    <!-- 110 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN55" rads=".*">
    <!-- 109 members -->
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="men" tag="NNpl,def,nom"/>
    <form  suffix="mens" tag="NNpl,def,gen"/>
    <form  suffix="met" tag="NNsg,def,nom"/>
    <form  suffix="mets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN56" rads=".*">
    <!-- 97 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="t" tag="NNsg,def,nom"/>
    <form  suffix="ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN57" rads=".*">
    <!-- 96 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN58" rads=".*">
    <!-- 96 members -->
    <form  suffix="a" tag="NNci"/>
    <form  suffix="a" tag="NNcm"/>
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="a-" tag="NNci"/>
    <form  suffix="a-" tag="NNcm"/>
    <form  suffix="a-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="e" tag="NNci"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e-" tag="NNci"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="e-" tag="NNsms"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN59" rads=".*">
    <!-- 93 members -->
    <form  suffix="ok" tag="NNci"/>
    <form  suffix="ok" tag="NNcm"/>
    <form  suffix="ok" tag="NNsg,indef,nom"/>
    <form  suffix="ok-" tag="NNci"/>
    <form  suffix="ok-" tag="NNcm"/>
    <form  suffix="ok-" tag="NNsms"/>
    <form  suffix="oken" tag="NNsg,def,nom"/>
    <form  suffix="okens" tag="NNsg,def,gen"/>
    <form  suffix="oks" tag="NNcm"/>
    <form  suffix="oks" tag="NNsg,indef,gen"/>
    <form  suffix="oks-" tag="NNcm"/>
    <form  suffix="öcker" tag="NNpl,indef,nom"/>
    <form  suffix="öckerna" tag="NNpl,def,nom"/>
    <form  suffix="öckernas" tag="NNpl,def,gen"/>
    <form  suffix="öckers" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN60" rads=".*">
    <!-- 92 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN61" rads=".*">
    <!-- 90 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN62" rads=".*">
    <!-- 87 members -->
    <form  suffix="a" tag="NNci"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="a" tag="NNcm"/>
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="a-" tag="NNci"/>
    <form  suffix="a-" tag="NNcm"/>
    <form  suffix="a-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="e" tag="NNci"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e-" tag="NNci"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="e-" tag="NNsms"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN63" rads=".*">
    <!-- 84 members -->
    <form  suffix="ma" tag="NNsg,indef,nom"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="man" tag="NNsg,def,nom"/>
    <form  suffix="mans" tag="NNsg,def,gen"/>
    <form  suffix="mas" tag="NNsg,indef,gen"/>
    <form  suffix="me" tag="NNcm"/>
    <form  suffix="me-" tag="NNcm"/>
    <form  suffix="mor" tag="NNpl,indef,nom"/>
    <form  suffix="morna" tag="NNpl,def,nom"/>
    <form  suffix="mornas" tag="NNpl,def,gen"/>
    <form  suffix="mors" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN64" rads=".*">
    <!-- 82 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN65" rads=".*">
    <!-- 80 members -->
    <form  suffix="" tag="NNsg,def,nom"/>
    <form  suffix="s" tag="NNsg,def,gen"/>
  </table>
  <table name="NN66" rads=".*">
    <!-- 74 members -->
    <form  suffix="el" tag="NNci"/>
    <form  suffix="el" tag="NNcm"/>
    <form  suffix="el" tag="NNsg,indef,nom"/>
    <form  suffix="el-" tag="NNci"/>
    <form  suffix="el-" tag="NNcm"/>
    <form  suffix="el-" tag="NNsms"/>
    <form  suffix="eln" tag="NNsg,def,nom"/>
    <form  suffix="elns" tag="NNsg,def,gen"/>
    <form  suffix="els" tag="NNcm"/>
    <form  suffix="els" tag="NNsg,indef,gen"/>
    <form  suffix="els-" tag="NNcm"/>
    <form  suffix="ler" tag="NNpl,indef,nom"/>
    <form  suffix="lerna" tag="NNpl,def,nom"/>
    <form  suffix="lernas" tag="NNpl,def,gen"/>
    <form  suffix="lers" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN67" rads=".*">
    <!-- 69 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN68" rads=".*">
    <!-- 68 members -->
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="e" tag="NNci"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e-" tag="NNci"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="e-" tag="NNsms"/>
    <form  suffix="o" tag="NNci"/>
    <form  suffix="o" tag="NNcm"/>
    <form  suffix="o-" tag="NNci"/>
    <form  suffix="o-" tag="NNcm"/>
    <form  suffix="o-" tag="NNsms"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN69" rads=".*">
    <!-- 67 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN70" rads=".*">
    <!-- 67 members -->
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN71" rads=".*">
    <!-- 62 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="men" tag="NNpl,def,nom"/>
    <form  suffix="mens" tag="NNpl,def,gen"/>
    <form  suffix="met" tag="NNsg,def,nom"/>
    <form  suffix="mets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN72" rads=".*">
    <!-- 62 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN73" rads=".*">
    <!-- 60 members -->
    <form  suffix="um" tag="NNci"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="um" tag="NNcm"/>
    <form  suffix="um" tag="NNsg,def,nom"/>
    <form  suffix="um" tag="NNsg,indef,nom"/>
    <form  suffix="um-" tag="NNci"/>
    <form  suffix="um-" tag="NNcm"/>
    <form  suffix="um-" tag="NNsms"/>
    <form  suffix="umet" tag="NNsg,def,nom"/>
    <form  suffix="umets" tag="NNsg,def,gen"/>
    <form  suffix="ums" tag="NNsg,def,gen"/>
    <form  suffix="ums" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN74" rads=".*">
    <!-- 59 members -->
    <form  suffix="e" tag="NNci"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e" tag="NNsg,indef,nom"/>
    <form  suffix="e-" tag="NNci"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="e-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="es" tag="NNci"/>
    <form  suffix="es" tag="NNcm"/>
    <form  suffix="es" tag="NNsg,indef,gen"/>
    <form  suffix="es-" tag="NNci"/>
    <form  suffix="es-" tag="NNcm"/>
    <form  suffix="es-" tag="NNsms"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN75" rads=".*">
    <!-- 58 members -->
    <form  suffix="el" tag="NNci"/>
    <form  suffix="el" tag="NNcm"/>
    <form  suffix="el" tag="NNsg,indef,nom"/>
    <form  suffix="el-" tag="NNci"/>
    <form  suffix="el-" tag="NNcm"/>
    <form  suffix="el-" tag="NNsms"/>
    <form  suffix="els" tag="NNsg,indef,gen"/>
    <form  suffix="let" tag="NNsg,def,nom"/>
    <form  suffix="lets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN76" rads=".*">
    <!-- 55 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN77" rads=".*">
    <!-- 54 members -->
    <form  suffix="and" tag="NNci"/>
    <form  suffix="and" tag="NNcm"/>
    <form  suffix="and" tag="NNsg,indef,nom"/>
    <form  suffix="and-" tag="NNci"/>
    <form  suffix="and-" tag="NNcm"/>
    <form  suffix="and-" tag="NNsms"/>
    <form  suffix="anden" tag="NNsg,def,nom"/>
    <form  suffix="andens" tag="NNsg,def,gen"/>
    <form  suffix="ands" tag="NNcm"/>
    <form  suffix="ands" tag="NNsg,indef,gen"/>
    <form  suffix="ands-" tag="NNcm"/>
    <form  suffix="änder" tag="NNpl,indef,nom"/>
    <form  suffix="änderna" tag="NNpl,def,nom"/>
    <form  suffix="ändernas" tag="NNpl,def,gen"/>
    <form  suffix="änders" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN78" rads=".*">
    <!-- 53 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN79" rads=".*">
    <!-- 53 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN80" rads=".*">
    <!-- 52 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN81" rads=".*">
    <!-- 52 members -->
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="e" tag="NNci"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e-" tag="NNci"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="e-" tag="NNsms"/>
  </table>
  <table name="NN82" rads=".*">
    <!-- 50 members -->
    <form  suffix="e" tag="NNsg,indef,nom"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,indef,nom"/>
    <form  suffix="ena" tag="NNpl,def,nom"/>
    <form  suffix="enas" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNpl,indef,gen"/>
    <form  suffix="es" tag="NNsg,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN83" rads=".*">
    <!-- 50 members -->
    <form  suffix="ot" tag="NNci"/>
    <form  suffix="ot" tag="NNcm"/>
    <form  suffix="ot" tag="NNsg,indef,nom"/>
    <form  suffix="ot-" tag="NNci"/>
    <form  suffix="ot-" tag="NNcm"/>
    <form  suffix="ot-" tag="NNsms"/>
    <form  suffix="oten" tag="NNsg,def,nom"/>
    <form  suffix="otens" tag="NNsg,def,gen"/>
    <form  suffix="ots" tag="NNci"/>
    <form  suffix="ots" tag="NNcm"/>
    <form  suffix="ots" tag="NNsg,indef,gen"/>
    <form  suffix="ots-" tag="NNci"/>
    <form  suffix="ots-" tag="NNcm"/>
    <form  suffix="ots-" tag="NNsms"/>
    <form  suffix="ötter" tag="NNpl,indef,nom"/>
    <form  suffix="ötterna" tag="NNpl,def,nom"/>
    <form  suffix="ötternas" tag="NNpl,def,gen"/>
    <form  suffix="ötters" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN84" rads=".*">
    <!-- 49 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,gen"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN85" rads=".*">
    <!-- 46 members -->
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN86" rads=".*">
    <!-- 46 members -->
    <form  suffix="ång" tag="NNci"/>
    <form  suffix="änger" tag="NNpl,indef,nom"/>
    <form  suffix="ängerna" tag="NNpl,def,nom"/>
    <form  suffix="ängernas" tag="NNpl,def,gen"/>
    <form  suffix="ängers" tag="NNpl,indef,gen"/>
    <form  suffix="ång" tag="NNcm"/>
    <form  suffix="ång" tag="NNsg,indef,nom"/>
    <form  suffix="ång-" tag="NNci"/>
    <form  suffix="ång-" tag="NNcm"/>
    <form  suffix="ång-" tag="NNsms"/>
    <form  suffix="ången" tag="NNsg,def,nom"/>
    <form  suffix="ångens" tag="NNsg,def,gen"/>
    <form  suffix="ångs" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN87" rads=".*">
    <!-- 44 members -->
    <form  suffix="a" tag="NNci"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="a" tag="NNcm"/>
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="a-" tag="NNci"/>
    <form  suffix="a-" tag="NNcm"/>
    <form  suffix="a-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN88" rads=".*">
    <!-- 44 members -->
    <form  suffix="la" tag="NNsg,indef,nom"/>
    <form  suffix="el" tag="NNci"/>
    <form  suffix="el" tag="NNcm"/>
    <form  suffix="el-" tag="NNci"/>
    <form  suffix="el-" tag="NNcm"/>
    <form  suffix="el-" tag="NNsms"/>
    <form  suffix="lan" tag="NNsg,def,nom"/>
    <form  suffix="lans" tag="NNsg,def,gen"/>
    <form  suffix="las" tag="NNsg,indef,gen"/>
    <form  suffix="le" tag="NNci"/>
    <form  suffix="le" tag="NNcm"/>
    <form  suffix="le-" tag="NNci"/>
    <form  suffix="le-" tag="NNcm"/>
    <form  suffix="le-" tag="NNsms"/>
    <form  suffix="lor" tag="NNpl,indef,nom"/>
    <form  suffix="lorna" tag="NNpl,def,nom"/>
    <form  suffix="lornas" tag="NNpl,def,gen"/>
    <form  suffix="lors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN89" rads=".*">
    <!-- 43 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="t" tag="NNsg,def,nom"/>
    <form  suffix="ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN90" rads=".*">
    <!-- 43 members -->
    <form  suffix="mer" tag="NNci"/>
    <form  suffix="mer" tag="NNcm"/>
    <form  suffix="mer" tag="NNpl,indef,nom"/>
    <form  suffix="mer" tag="NNsg,indef,nom"/>
    <form  suffix="mer-" tag="NNci"/>
    <form  suffix="mer-" tag="NNcm"/>
    <form  suffix="mer-" tag="NNsms"/>
    <form  suffix="merna" tag="NNpl,def,nom"/>
    <form  suffix="mernas" tag="NNpl,def,gen"/>
    <form  suffix="mers" tag="NNpl,indef,gen"/>
    <form  suffix="mers" tag="NNsg,indef,gen"/>
    <form  suffix="ren" tag="NNpl,def,nom"/>
    <form  suffix="rens" tag="NNpl,def,gen"/>
    <form  suffix="ret" tag="NNsg,def,nom"/>
    <form  suffix="rets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN91" rads=".*">
    <!-- 42 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="n" tag="NNpl,indef,nom"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
    <form  suffix="ns" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="t" tag="NNsg,def,nom"/>
    <form  suffix="ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN92" rads=".*">
    <!-- 41 members -->
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNci"/>
    <form  suffix="as" tag="NNcm"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="as-" tag="NNci"/>
    <form  suffix="as-" tag="NNcm"/>
    <form  suffix="as-" tag="NNsms"/>
    <form  suffix="e" tag="NNci"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e-" tag="NNci"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="e-" tag="NNsms"/>
  </table>
  <table name="NN93" rads=".*">
    <!-- 40 members -->
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="o" tag="NNci"/>
    <form  suffix="o" tag="NNcm"/>
    <form  suffix="o-" tag="NNci"/>
    <form  suffix="o-" tag="NNcm"/>
    <form  suffix="o-" tag="NNsms"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN94" rads=".*">
    <!-- 39 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="n" tag="NNpl,def,nom"/>
    <form  suffix="ns" tag="NNpl,def,gen"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="t" tag="NNsg,def,nom"/>
    <form  suffix="ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN95" rads=".*">
    <!-- 39 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="n" tag="NNpl,indef,nom"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
    <form  suffix="ns" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="t" tag="NNsg,def,nom"/>
    <form  suffix="ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN96" rads=".*">
    <!-- 39 members -->
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="e" tag="NNci"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e-" tag="NNci"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="e-" tag="NNsms"/>
    <form  suffix="es" tag="NNci"/>
    <form  suffix="es" tag="NNcm"/>
    <form  suffix="es-" tag="NNci"/>
    <form  suffix="es-" tag="NNcm"/>
    <form  suffix="es-" tag="NNsms"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN97" rads=".*">
    <!-- 38 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN98" rads=".*">
    <!-- 38 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="met" tag="NNsg,def,nom"/>
    <form  suffix="mets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN99" rads=".*">
    <!-- 37 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,indef,nom"/>
    <form  suffix="ena" tag="NNpl,def,nom"/>
    <form  suffix="enas" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN100" rads=".*">
    <!-- 36 members -->
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="at" tag="NNsg,def,nom"/>
    <form  suffix="ats" tag="NNsg,def,gen"/>
    <form  suffix="on" tag="NNci"/>
    <form  suffix="on" tag="NNcm"/>
    <form  suffix="on" tag="NNpl,indef,nom"/>
    <form  suffix="on-" tag="NNci"/>
    <form  suffix="on-" tag="NNcm"/>
    <form  suffix="on-" tag="NNsms"/>
    <form  suffix="onen" tag="NNpl,def,nom"/>
    <form  suffix="onens" tag="NNpl,def,gen"/>
    <form  suffix="ons" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN101" rads=".*">
    <!-- 36 members -->
    <form  suffix="mare" tag="NNpl,indef,nom"/>
    <form  suffix="mar" tag="NNci"/>
    <form  suffix="mar" tag="NNcm"/>
    <form  suffix="mar-" tag="NNci"/>
    <form  suffix="mar-" tag="NNcm"/>
    <form  suffix="mar-" tag="NNsms"/>
    <form  suffix="mare" tag="NNsg,indef,nom"/>
    <form  suffix="maren" tag="NNsg,def,nom"/>
    <form  suffix="marens" tag="NNsg,def,gen"/>
    <form  suffix="mares" tag="NNpl,indef,gen"/>
    <form  suffix="mares" tag="NNsg,indef,gen"/>
    <form  suffix="rar" tag="NNpl,indef,nom"/>
    <form  suffix="rarna" tag="NNpl,def,nom"/>
    <form  suffix="rarnas" tag="NNpl,def,gen"/>
    <form  suffix="rars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN102" rads=".*">
    <!-- 35 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNpl,indef,nom"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="sarna" tag="NNpl,def,nom"/>
    <form  suffix="sarnas" tag="NNpl,def,gen"/>
    <form  suffix="sen" tag="NNpl,def,nom"/>
    <form  suffix="sens" tag="NNpl,def,gen"/>
  </table>
  <table name="NN103" rads=".*">
    <!-- 35 members -->
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN104" rads=".*">
    <!-- 35 members -->
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN105" rads=".*">
    <!-- 34 members -->
    <form  suffix="en" tag="NNci"/>
    <form  suffix="en" tag="NNcm"/>
    <form  suffix="en" tag="NNsg,indef,nom"/>
    <form  suffix="en-" tag="NNci"/>
    <form  suffix="en-" tag="NNcm"/>
    <form  suffix="en-" tag="NNsms"/>
    <form  suffix="ens" tag="NNcm"/>
    <form  suffix="ens" tag="NNsg,indef,gen"/>
    <form  suffix="ens-" tag="NNcm"/>
    <form  suffix="nar" tag="NNpl,indef,nom"/>
    <form  suffix="narna" tag="NNpl,def,nom"/>
    <form  suffix="narnas" tag="NNpl,def,gen"/>
    <form  suffix="nars" tag="NNpl,indef,gen"/>
    <form  suffix="nen" tag="NNsg,def,nom"/>
    <form  suffix="nens" tag="NNsg,def,gen"/>
  </table>
  <table name="NN106" rads=".*">
    <!-- 33 members -->
    <form  suffix="er" tag="NNci"/>
    <form  suffix="er" tag="NNcm"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="er" tag="NNsg,indef,nom"/>
    <form  suffix="er-" tag="NNci"/>
    <form  suffix="er-" tag="NNcm"/>
    <form  suffix="er-" tag="NNsms"/>
    <form  suffix="ern" tag="NNsg,def,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="erns" tag="NNsg,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="ers" tag="NNsg,indef,gen"/>
    <form  suffix="rar" tag="NNpl,indef,nom"/>
    <form  suffix="rarna" tag="NNpl,def,nom"/>
    <form  suffix="rarnas" tag="NNpl,def,gen"/>
    <form  suffix="rars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN107" rads=".*">
    <!-- 33 members -->
    <form  suffix="r" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="rna" tag="NNpl,def,nom"/>
    <form  suffix="rnas" tag="NNpl,def,gen"/>
    <form  suffix="rs" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN108" rads=".*">
    <!-- 31 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN109" rads=".*">
    <!-- 31 members -->
    <form  suffix="and" tag="NNsg,indef,nom"/>
    <form  suffix="andet" tag="NNsg,def,nom"/>
    <form  suffix="andets" tag="NNsg,def,gen"/>
    <form  suffix="ands" tag="NNci"/>
    <form  suffix="ands" tag="NNcm"/>
    <form  suffix="ands" tag="NNsg,indef,gen"/>
    <form  suffix="ands-" tag="NNci"/>
    <form  suffix="ands-" tag="NNcm"/>
    <form  suffix="ands-" tag="NNsms"/>
    <form  suffix="änder" tag="NNpl,indef,nom"/>
    <form  suffix="änderna" tag="NNpl,def,nom"/>
    <form  suffix="ändernas" tag="NNpl,def,gen"/>
    <form  suffix="änders" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN110" rads=".*">
    <!-- 30 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN111" rads=".*">
    <!-- 30 members -->
    <form  suffix="er" tag="NNci"/>
    <form  suffix="er" tag="NNcm"/>
    <form  suffix="er" tag="NNsg,indef,nom"/>
    <form  suffix="er-" tag="NNci"/>
    <form  suffix="er-" tag="NNcm"/>
    <form  suffix="er-" tag="NNsms"/>
    <form  suffix="ern" tag="NNsg,def,nom"/>
    <form  suffix="erns" tag="NNsg,def,gen"/>
    <form  suffix="ers" tag="NNcm"/>
    <form  suffix="ers" tag="NNsg,indef,gen"/>
    <form  suffix="ers-" tag="NNcm"/>
    <form  suffix="rer" tag="NNpl,indef,nom"/>
    <form  suffix="rerna" tag="NNpl,def,nom"/>
    <form  suffix="rernas" tag="NNpl,def,gen"/>
    <form  suffix="rers" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN112" rads=".*">
    <!-- 30 members -->
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN113" rads=".*" fast="-">
    <!-- 29 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="n" tag="NNpl,indef,nom"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
    <form  suffix="ns" tag="NNpl,indef,gen"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN114" rads=".*" fast="-">
    <!-- 29 members -->
    <form  suffix="a" tag="NNci"/>
    <form  suffix="a" tag="NNcm"/>
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="a-" tag="NNci"/>
    <form  suffix="a-" tag="NNcm"/>
    <form  suffix="a-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="e" tag="NNci"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e-" tag="NNci"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="e-" tag="NNsms"/>
  </table>
  <table name="NN115" rads=".*" fast="-">
    <!-- 28 members -->
    <form  suffix="er" tag="NNci"/>
    <form  suffix="er" tag="NNcm"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="er" tag="NNsg,indef,nom"/>
    <form  suffix="er-" tag="NNci"/>
    <form  suffix="er-" tag="NNcm"/>
    <form  suffix="er-" tag="NNsms"/>
    <form  suffix="ers" tag="NNcm"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="ers" tag="NNsg,indef,gen"/>
    <form  suffix="ers-" tag="NNcm"/>
    <form  suffix="ren" tag="NNpl,def,nom"/>
    <form  suffix="rens" tag="NNpl,def,gen"/>
    <form  suffix="ret" tag="NNsg,def,nom"/>
    <form  suffix="rets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN116" rads=".*" fast="-">
    <!-- 27 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
  </table>
  <table name="NN117" rads=".*" fast="-">
    <!-- 27 members -->
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="e" tag="NNci"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e-" tag="NNci"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="e-" tag="NNsms"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
    <form  suffix="u" tag="NNci"/>
    <form  suffix="u" tag="NNcm"/>
    <form  suffix="u-" tag="NNci"/>
    <form  suffix="u-" tag="NNcm"/>
    <form  suffix="u-" tag="NNsms"/>
  </table>
  <table name="NN118" rads=".*" fast="-">
    <!-- 27 members -->
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="o" tag="NNci"/>
    <form  suffix="o" tag="NNcm"/>
    <form  suffix="o-" tag="NNci"/>
    <form  suffix="o-" tag="NNcm"/>
    <form  suffix="o-" tag="NNsms"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN119" rads=".*" fast="-">
    <!-- 27 members -->
    <form  suffix="en" tag="NNci"/>
    <form  suffix="en" tag="NNcm"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="en" tag="NNsg,indef,nom"/>
    <form  suffix="en-" tag="NNci"/>
    <form  suffix="en-" tag="NNcm"/>
    <form  suffix="en-" tag="NNsms"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="ens" tag="NNsg,indef,gen"/>
    <form  suffix="nar" tag="NNpl,indef,nom"/>
    <form  suffix="narna" tag="NNpl,def,nom"/>
    <form  suffix="narnas" tag="NNpl,def,gen"/>
    <form  suffix="nars" tag="NNpl,indef,gen"/>
    <form  suffix="nen" tag="NNsg,def,nom"/>
    <form  suffix="nens" tag="NNsg,def,gen"/>
  </table>
  <table name="NN120" rads=".*" fast="-">
    <!-- 27 members -->
    <form  suffix="en" tag="NNci"/>
    <form  suffix="en" tag="NNcm"/>
    <form  suffix="en" tag="NNsg,indef,nom"/>
    <form  suffix="en-" tag="NNci"/>
    <form  suffix="en-" tag="NNcm"/>
    <form  suffix="en-" tag="NNsms"/>
    <form  suffix="ens" tag="NNsg,indef,gen"/>
    <form  suffix="net" tag="NNsg,def,nom"/>
    <form  suffix="nets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN121" rads=".*" fast="-">
    <!-- 27 members -->
    <form  suffix="er" tag="NNci"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="er" tag="NNsg,indef,nom"/>
    <form  suffix="er-" tag="NNci"/>
    <form  suffix="er-" tag="NNsms"/>
    <form  suffix="eren" tag="NNpl,def,nom"/>
    <form  suffix="erens" tag="NNpl,def,gen"/>
    <form  suffix="eret" tag="NNsg,def,nom"/>
    <form  suffix="erets" tag="NNsg,def,gen"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNcm"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="ers" tag="NNsg,indef,gen"/>
    <form  suffix="ers-" tag="NNcm"/>
    <form  suffix="ren" tag="NNpl,def,nom"/>
    <form  suffix="rens" tag="NNpl,def,gen"/>
    <form  suffix="ret" tag="NNsg,def,nom"/>
    <form  suffix="rets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN122" rads=".*" fast="-">
    <!-- 27 members -->
    <form  suffix="um" tag="NNci"/>
    <form  suffix="a" tag="NNpl,indef,nom"/>
    <form  suffix="ana" tag="NNpl,def,nom"/>
    <form  suffix="anas" tag="NNpl,def,gen"/>
    <form  suffix="as" tag="NNpl,indef,gen"/>
    <form  suffix="um" tag="NNcm"/>
    <form  suffix="um" tag="NNpl,indef,nom"/>
    <form  suffix="um" tag="NNsg,def,nom"/>
    <form  suffix="um" tag="NNsg,indef,nom"/>
    <form  suffix="um-" tag="NNci"/>
    <form  suffix="um-" tag="NNcm"/>
    <form  suffix="um-" tag="NNsms"/>
    <form  suffix="umen" tag="NNpl,def,nom"/>
    <form  suffix="umens" tag="NNpl,def,gen"/>
    <form  suffix="umet" tag="NNsg,def,nom"/>
    <form  suffix="umets" tag="NNsg,def,gen"/>
    <form  suffix="ums" tag="NNpl,indef,gen"/>
    <form  suffix="ums" tag="NNsg,def,gen"/>
    <form  suffix="ums" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN123" rads=".*" fast="-">
    <!-- 26 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN124" rads=".*" fast="-">
    <!-- 25 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNpl,indef,nom"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="sarna" tag="NNpl,def,nom"/>
    <form  suffix="sarnas" tag="NNpl,def,gen"/>
    <form  suffix="sen" tag="NNpl,def,nom"/>
    <form  suffix="sens" tag="NNpl,def,gen"/>
  </table>
  <table name="NN125" rads=".*" fast="-">
    <!-- 25 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN126" rads=".*" fast="-">
    <!-- 25 members -->
    <form  suffix="ad" tag="NNci"/>
    <form  suffix="ad" tag="NNcm"/>
    <form  suffix="ad" tag="NNsg,indef,nom"/>
    <form  suffix="ad-" tag="NNci"/>
    <form  suffix="ad-" tag="NNcm"/>
    <form  suffix="ad-" tag="NNsms"/>
    <form  suffix="aden" tag="NNsg,def,nom"/>
    <form  suffix="adens" tag="NNsg,def,gen"/>
    <form  suffix="ads" tag="NNcm"/>
    <form  suffix="ads" tag="NNsg,indef,gen"/>
    <form  suffix="ads-" tag="NNcm"/>
    <form  suffix="äder" tag="NNpl,indef,nom"/>
    <form  suffix="äderna" tag="NNpl,def,nom"/>
    <form  suffix="ädernas" tag="NNpl,def,gen"/>
    <form  suffix="äders" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN127" rads=".*" fast="-">
    <!-- 24 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="r" tag="NNpl,indef,nom"/>
    <form  suffix="rna" tag="NNpl,def,nom"/>
    <form  suffix="rnas" tag="NNpl,def,gen"/>
    <form  suffix="rs" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNpl,indef,nom"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="sen" tag="NNpl,def,nom"/>
    <form  suffix="sens" tag="NNpl,def,gen"/>
  </table>
  <table name="NN128" rads=".*" fast="-">
    <!-- 23 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN129" rads=".*" fast="-">
    <!-- 23 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN130" rads=".*" fast="-">
    <!-- 23 members -->
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="es" tag="NNci"/>
    <form  suffix="es" tag="NNcm"/>
    <form  suffix="es-" tag="NNci"/>
    <form  suffix="es-" tag="NNcm"/>
    <form  suffix="es-" tag="NNsms"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN131" rads=".*" fast="-">
    <!-- 22 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN132" rads=".*" fast="-">
    <!-- 22 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN133" rads=".*" fast="-">
    <!-- 22 members -->
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN134" rads=".*" fast="-">
    <!-- 22 members -->
    <form  suffix="er" tag="NNci"/>
    <form  suffix="er" tag="NNcm"/>
    <form  suffix="er" tag="NNsg,indef,nom"/>
    <form  suffix="er-" tag="NNci"/>
    <form  suffix="er-" tag="NNcm"/>
    <form  suffix="er-" tag="NNsms"/>
    <form  suffix="ern" tag="NNsg,def,nom"/>
    <form  suffix="erns" tag="NNsg,def,gen"/>
    <form  suffix="ers" tag="NNcm"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="ers" tag="NNpl,indef,nom"/>
    <form  suffix="ers" tag="NNsg,indef,gen"/>
    <form  suffix="ers-" tag="NNcm"/>
    <form  suffix="ersarna" tag="NNpl,def,nom"/>
    <form  suffix="ersarnas" tag="NNpl,def,gen"/>
    <form  suffix="ersen" tag="NNpl,def,nom"/>
    <form  suffix="ersens" tag="NNpl,def,gen"/>
    <form  suffix="rar" tag="NNpl,indef,nom"/>
    <form  suffix="rarna" tag="NNpl,def,nom"/>
    <form  suffix="rarnas" tag="NNpl,def,gen"/>
    <form  suffix="rars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN135" rads=".*" fast="-">
    <!-- 21 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN136" rads=".*" fast="-">
    <!-- 20 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
  </table>
  <table name="NN137" rads=".*" fast="-">
    <!-- 20 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN138" rads=".*" fast="-">
    <!-- 20 members -->
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="-et" tag="NNsg,def,nom"/>
    <form  suffix="-ets" tag="NNsg,def,gen"/>
    <form  suffix="-n" tag="NNpl,indef,nom"/>
    <form  suffix="-na" tag="NNpl,def,nom"/>
    <form  suffix="-nas" tag="NNpl,def,gen"/>
    <form  suffix="-ns" tag="NNpl,indef,gen"/>
    <form  suffix="-s" tag="NNsg,indef,gen"/>
    <form  suffix="-t" tag="NNsg,def,nom"/>
    <form  suffix="-ts" tag="NNsg,def,gen"/>
    <form  suffix=":et" tag="NNsg,def,nom"/>
    <form  suffix=":ets" tag="NNsg,def,gen"/>
    <form  suffix=":n" tag="NNpl,indef,nom"/>
    <form  suffix=":na" tag="NNpl,def,nom"/>
    <form  suffix=":nas" tag="NNpl,def,gen"/>
    <form  suffix=":ns" tag="NNpl,indef,gen"/>
    <form  suffix=":s" tag="NNsg,indef,gen"/>
    <form  suffix=":t" tag="NNsg,def,nom"/>
    <form  suffix=":ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN139" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="t" tag="NNsg,def,nom"/>
    <form  suffix="ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN140" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN141" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="la" tag="NNsg,indef,nom"/>
    <form  suffix="el" tag="NNci"/>
    <form  suffix="el" tag="NNcm"/>
    <form  suffix="el-" tag="NNci"/>
    <form  suffix="el-" tag="NNcm"/>
    <form  suffix="el-" tag="NNsms"/>
    <form  suffix="lan" tag="NNsg,def,nom"/>
    <form  suffix="lans" tag="NNsg,def,gen"/>
    <form  suffix="las" tag="NNsg,indef,gen"/>
    <form  suffix="le" tag="NNcm"/>
    <form  suffix="le-" tag="NNcm"/>
    <form  suffix="lor" tag="NNpl,indef,nom"/>
    <form  suffix="lorna" tag="NNpl,def,nom"/>
    <form  suffix="lornas" tag="NNpl,def,gen"/>
    <form  suffix="lors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN142" rads=".*" fast="-">
    <!-- 18 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
    <form  suffix="ter" tag="NNpl,indef,nom"/>
    <form  suffix="terna" tag="NNpl,def,nom"/>
    <form  suffix="ternas" tag="NNpl,def,gen"/>
    <form  suffix="ters" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN143" rads=".*" fast="-">
    <!-- 18 members -->
    <form  suffix="e" tag="NNsg,indef,nom"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="es" tag="NNcm"/>
    <form  suffix="es" tag="NNsg,indef,gen"/>
    <form  suffix="es-" tag="NNcm"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN144" rads=".*" fast="-">
    <!-- 18 members -->
    <form  suffix="us" tag="NNci"/>
    <form  suffix="us" tag="NNcm"/>
    <form  suffix="us" tag="NNsg,indef,gen"/>
    <form  suffix="us" tag="NNsg,indef,nom"/>
    <form  suffix="us-" tag="NNci"/>
    <form  suffix="us-" tag="NNcm"/>
    <form  suffix="us-" tag="NNsms"/>
    <form  suffix="usen" tag="NNsg,def,nom"/>
    <form  suffix="usens" tag="NNsg,def,gen"/>
    <form  suffix="öss" tag="NNpl,indef,gen"/>
    <form  suffix="öss" tag="NNpl,indef,nom"/>
    <form  suffix="össen" tag="NNpl,def,nom"/>
    <form  suffix="össens" tag="NNpl,def,gen"/>
  </table>
  <table name="NN145" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="n" tag="NNpl,indef,nom"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
    <form  suffix="ns" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="t" tag="NNsg,def,nom"/>
    <form  suffix="ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN146" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNpl,indef,nom"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="sarna" tag="NNpl,def,nom"/>
    <form  suffix="sarnas" tag="NNpl,def,gen"/>
    <form  suffix="sen" tag="NNpl,def,nom"/>
    <form  suffix="sens" tag="NNpl,def,gen"/>
  </table>
  <table name="NN147" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="" tag="NNpl,indef,gen"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
  </table>
  <table name="NN148" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN149" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN150" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN151" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN152" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNpl,indef,nom"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="sarna" tag="NNpl,def,nom"/>
    <form  suffix="sarnas" tag="NNpl,def,gen"/>
    <form  suffix="sen" tag="NNpl,def,nom"/>
    <form  suffix="sens" tag="NNpl,def,gen"/>
  </table>
  <table name="NN153" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="er" tag="NNci"/>
    <form  suffix="er" tag="NNcm"/>
    <form  suffix="er" tag="NNsg,indef,nom"/>
    <form  suffix="er-" tag="NNci"/>
    <form  suffix="er-" tag="NNcm"/>
    <form  suffix="er-" tag="NNsms"/>
    <form  suffix="eret" tag="NNsg,def,nom"/>
    <form  suffix="erets" tag="NNsg,def,gen"/>
    <form  suffix="ers" tag="NNsg,indef,gen"/>
    <form  suffix="ret" tag="NNsg,def,nom"/>
    <form  suffix="rets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN154" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="onde" tag="NNci"/>
    <form  suffix="ond" tag="NNci"/>
    <form  suffix="ond-" tag="NNci"/>
    <form  suffix="ond-" tag="NNsms"/>
    <form  suffix="onde" tag="NNcm"/>
    <form  suffix="onde" tag="NNsg,indef,nom"/>
    <form  suffix="onde-" tag="NNci"/>
    <form  suffix="onde-" tag="NNcm"/>
    <form  suffix="onde-" tag="NNsms"/>
    <form  suffix="onden" tag="NNsg,def,nom"/>
    <form  suffix="ondens" tag="NNsg,def,gen"/>
    <form  suffix="ondes" tag="NNcm"/>
    <form  suffix="ondes" tag="NNsg,indef,gen"/>
    <form  suffix="ondes-" tag="NNcm"/>
    <form  suffix="onds" tag="NNcm"/>
    <form  suffix="onds-" tag="NNcm"/>
    <form  suffix="önder" tag="NNpl,indef,nom"/>
    <form  suffix="önderna" tag="NNpl,def,nom"/>
    <form  suffix="öndernas" tag="NNpl,def,gen"/>
    <form  suffix="önders" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN155" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="det" tag="NNsg,def,nom"/>
    <form  suffix="dets" tag="NNsg,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN156" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="e" tag="NNci"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e-" tag="NNci"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="e-" tag="NNsms"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN157" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="el" tag="NNci"/>
    <form  suffix="el" tag="NNcm"/>
    <form  suffix="el" tag="NNsg,indef,nom"/>
    <form  suffix="el-" tag="NNci"/>
    <form  suffix="el-" tag="NNcm"/>
    <form  suffix="el-" tag="NNsms"/>
    <form  suffix="eln" tag="NNsg,def,nom"/>
    <form  suffix="elns" tag="NNsg,def,gen"/>
    <form  suffix="els" tag="NNcm"/>
    <form  suffix="els" tag="NNsg,indef,gen"/>
    <form  suffix="els-" tag="NNcm"/>
    <form  suffix="lar" tag="NNpl,indef,nom"/>
    <form  suffix="larna" tag="NNpl,def,nom"/>
    <form  suffix="larnas" tag="NNpl,def,gen"/>
    <form  suffix="lars" tag="NNpl,indef,gen"/>
    <form  suffix="ler" tag="NNpl,indef,nom"/>
    <form  suffix="lerna" tag="NNpl,def,nom"/>
    <form  suffix="lernas" tag="NNpl,def,gen"/>
    <form  suffix="lers" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN158" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="me" tag="NNsg,indef,nom"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="mar" tag="NNpl,indef,nom"/>
    <form  suffix="marna" tag="NNpl,def,nom"/>
    <form  suffix="marnas" tag="NNpl,def,gen"/>
    <form  suffix="mars" tag="NNpl,indef,gen"/>
    <form  suffix="men" tag="NNsg,def,nom"/>
    <form  suffix="mens" tag="NNsg,def,gen"/>
    <form  suffix="mes" tag="NNcm"/>
    <form  suffix="mes" tag="NNsg,indef,gen"/>
    <form  suffix="mes-" tag="NNcm"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN159" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="nen" tag="NNsg,def,nom"/>
    <form  suffix="nens" tag="NNsg,def,gen"/>
    <form  suffix="ner" tag="NNpl,indef,nom"/>
    <form  suffix="nerna" tag="NNpl,def,nom"/>
    <form  suffix="nernas" tag="NNpl,def,gen"/>
    <form  suffix="ners" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN160" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="a" tag="NNci"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="a" tag="NNcm"/>
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="a-" tag="NNci"/>
    <form  suffix="a-" tag="NNcm"/>
    <form  suffix="a-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN161" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="a" tag="NNci"/>
    <form  suffix="a" tag="NNcm"/>
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="a-" tag="NNci"/>
    <form  suffix="a-" tag="NNcm"/>
    <form  suffix="a-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
  </table>
  <table name="NN162" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="er" tag="NNsg,indef,nom"/>
    <form  suffix="ern" tag="NNsg,def,nom"/>
    <form  suffix="erns" tag="NNsg,def,gen"/>
    <form  suffix="ers" tag="NNci"/>
    <form  suffix="ers" tag="NNcm"/>
    <form  suffix="ers" tag="NNsg,indef,gen"/>
    <form  suffix="ers-" tag="NNci"/>
    <form  suffix="ers-" tag="NNcm"/>
    <form  suffix="ers-" tag="NNsms"/>
    <form  suffix="rar" tag="NNpl,indef,nom"/>
    <form  suffix="rarna" tag="NNpl,def,nom"/>
    <form  suffix="rarnas" tag="NNpl,def,gen"/>
    <form  suffix="rars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN163" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="r" tag="NNpl,indef,nom"/>
    <form  suffix="rna" tag="NNpl,def,nom"/>
    <form  suffix="rnas" tag="NNpl,def,gen"/>
    <form  suffix="rs" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN164" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="e" tag="NNci"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e-" tag="NNci"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="e-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN165" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="a" tag="NNci"/>
    <form  suffix="a" tag="NNcm"/>
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="a-" tag="NNci"/>
    <form  suffix="a-" tag="NNcm"/>
    <form  suffix="a-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN166" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="o" tag="NNci"/>
    <form  suffix="o" tag="NNcm"/>
    <form  suffix="o-" tag="NNci"/>
    <form  suffix="o-" tag="NNcm"/>
    <form  suffix="o-" tag="NNsms"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN167" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="ad" tag="NNsg,indef,nom"/>
    <form  suffix="aden" tag="NNsg,def,nom"/>
    <form  suffix="adens" tag="NNsg,def,gen"/>
    <form  suffix="ads" tag="NNci"/>
    <form  suffix="ads" tag="NNcm"/>
    <form  suffix="ads" tag="NNsg,indef,gen"/>
    <form  suffix="ads-" tag="NNci"/>
    <form  suffix="ads-" tag="NNcm"/>
    <form  suffix="ads-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="äder" tag="NNpl,indef,nom"/>
    <form  suffix="äderna" tag="NNpl,def,nom"/>
    <form  suffix="ädernas" tag="NNpl,def,gen"/>
    <form  suffix="äders" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN168" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="er" tag="NNci"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="er" tag="NNcm"/>
    <form  suffix="er" tag="NNsg,indef,nom"/>
    <form  suffix="er-" tag="NNci"/>
    <form  suffix="er-" tag="NNcm"/>
    <form  suffix="er-" tag="NNsms"/>
    <form  suffix="ern" tag="NNsg,def,nom"/>
    <form  suffix="erns" tag="NNsg,def,gen"/>
    <form  suffix="ers" tag="NNsg,indef,gen"/>
    <form  suffix="rar" tag="NNpl,indef,nom"/>
    <form  suffix="rarna" tag="NNpl,def,nom"/>
    <form  suffix="rarnas" tag="NNpl,def,gen"/>
    <form  suffix="rars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN169" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="oder" tag="NNci"/>
    <form  suffix="oder" tag="NNcm"/>
    <form  suffix="oder" tag="NNsg,indef,nom"/>
    <form  suffix="oder-" tag="NNci"/>
    <form  suffix="oder-" tag="NNcm"/>
    <form  suffix="oder-" tag="NNsms"/>
    <form  suffix="odern" tag="NNsg,def,nom"/>
    <form  suffix="oderns" tag="NNsg,def,gen"/>
    <form  suffix="oders" tag="NNci"/>
    <form  suffix="oders" tag="NNcm"/>
    <form  suffix="oders" tag="NNsg,indef,gen"/>
    <form  suffix="oders-" tag="NNci"/>
    <form  suffix="oders-" tag="NNcm"/>
    <form  suffix="oders-" tag="NNsms"/>
    <form  suffix="öder" tag="NNpl,indef,nom"/>
    <form  suffix="öderna" tag="NNpl,def,nom"/>
    <form  suffix="ödernas" tag="NNpl,def,gen"/>
    <form  suffix="öders" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN170" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="um" tag="NNci"/>
    <form  suffix="a" tag="NNpl,indef,nom"/>
    <form  suffix="ana" tag="NNpl,def,nom"/>
    <form  suffix="anas" tag="NNpl,def,gen"/>
    <form  suffix="as" tag="NNpl,indef,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="um" tag="NNcm"/>
    <form  suffix="um" tag="NNpl,indef,nom"/>
    <form  suffix="um" tag="NNsg,def,nom"/>
    <form  suffix="um" tag="NNsg,indef,nom"/>
    <form  suffix="um-" tag="NNci"/>
    <form  suffix="um-" tag="NNcm"/>
    <form  suffix="um-" tag="NNsms"/>
    <form  suffix="umen" tag="NNpl,def,nom"/>
    <form  suffix="umens" tag="NNpl,def,gen"/>
    <form  suffix="umet" tag="NNsg,def,nom"/>
    <form  suffix="umets" tag="NNsg,def,gen"/>
    <form  suffix="ums" tag="NNcm"/>
    <form  suffix="ums" tag="NNpl,indef,gen"/>
    <form  suffix="ums" tag="NNsg,def,gen"/>
    <form  suffix="ums" tag="NNsg,indef,gen"/>
    <form  suffix="ums-" tag="NNcm"/>
  </table>
  <table name="NN171" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,def,gen"/>
    <form  suffix="" tag="NNsg,def,nom"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
  </table>
  <table name="NN172" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="a" tag="NNci"/>
    <form  suffix="a" tag="NNcm"/>
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="a-" tag="NNci"/>
    <form  suffix="a-" tag="NNcm"/>
    <form  suffix="a-" tag="NNsms"/>
    <form  suffix="an" tag="NNpl,indef,nom"/>
    <form  suffix="ana" tag="NNpl,def,nom"/>
    <form  suffix="anas" tag="NNpl,def,gen"/>
    <form  suffix="ans" tag="NNpl,indef,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="at" tag="NNsg,def,nom"/>
    <form  suffix="ats" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN173" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="el" tag="NNsg,indef,nom"/>
    <form  suffix="eln" tag="NNsg,def,nom"/>
    <form  suffix="elns" tag="NNsg,def,gen"/>
    <form  suffix="els" tag="NNci"/>
    <form  suffix="els" tag="NNcm"/>
    <form  suffix="els" tag="NNsg,indef,gen"/>
    <form  suffix="els-" tag="NNci"/>
    <form  suffix="els-" tag="NNcm"/>
    <form  suffix="els-" tag="NNsms"/>
    <form  suffix="lar" tag="NNpl,indef,nom"/>
    <form  suffix="larna" tag="NNpl,def,nom"/>
    <form  suffix="larnas" tag="NNpl,def,gen"/>
    <form  suffix="lars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN174" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="er" tag="NNci"/>
    <form  suffix="er" tag="NNcm"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="er" tag="NNsg,indef,nom"/>
    <form  suffix="er-" tag="NNci"/>
    <form  suffix="er-" tag="NNcm"/>
    <form  suffix="er-" tag="NNsms"/>
    <form  suffix="ern" tag="NNsg,def,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="erns" tag="NNsg,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="ers" tag="NNpl,indef,nom"/>
    <form  suffix="ers" tag="NNsg,indef,gen"/>
    <form  suffix="ersarna" tag="NNpl,def,nom"/>
    <form  suffix="ersarnas" tag="NNpl,def,gen"/>
    <form  suffix="rar" tag="NNpl,indef,nom"/>
    <form  suffix="rarna" tag="NNpl,def,nom"/>
    <form  suffix="rarnas" tag="NNpl,def,gen"/>
    <form  suffix="rars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN175" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN176" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="ra" tag="NNsg,indef,nom"/>
    <form  suffix="er" tag="NNci"/>
    <form  suffix="er" tag="NNcm"/>
    <form  suffix="er-" tag="NNci"/>
    <form  suffix="er-" tag="NNcm"/>
    <form  suffix="er-" tag="NNsms"/>
    <form  suffix="ran" tag="NNsg,def,nom"/>
    <form  suffix="rans" tag="NNsg,def,gen"/>
    <form  suffix="ras" tag="NNsg,indef,gen"/>
    <form  suffix="re" tag="NNci"/>
    <form  suffix="re" tag="NNcm"/>
    <form  suffix="re-" tag="NNci"/>
    <form  suffix="re-" tag="NNcm"/>
    <form  suffix="re-" tag="NNsms"/>
    <form  suffix="ror" tag="NNpl,indef,nom"/>
    <form  suffix="rorna" tag="NNpl,def,nom"/>
    <form  suffix="rornas" tag="NNpl,def,gen"/>
    <form  suffix="rors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN177" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="um" tag="NNci"/>
    <form  suffix="a" tag="NNpl,indef,nom"/>
    <form  suffix="ana" tag="NNpl,def,nom"/>
    <form  suffix="anas" tag="NNpl,def,gen"/>
    <form  suffix="as" tag="NNpl,indef,gen"/>
    <form  suffix="ena" tag="NNpl,def,nom"/>
    <form  suffix="enas" tag="NNpl,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="um" tag="NNcm"/>
    <form  suffix="um" tag="NNpl,indef,nom"/>
    <form  suffix="um" tag="NNsg,def,nom"/>
    <form  suffix="um" tag="NNsg,indef,nom"/>
    <form  suffix="um-" tag="NNci"/>
    <form  suffix="um-" tag="NNcm"/>
    <form  suffix="um-" tag="NNsms"/>
    <form  suffix="umen" tag="NNpl,def,nom"/>
    <form  suffix="umens" tag="NNpl,def,gen"/>
    <form  suffix="umet" tag="NNsg,def,nom"/>
    <form  suffix="umets" tag="NNsg,def,gen"/>
    <form  suffix="ums" tag="NNcm"/>
    <form  suffix="ums" tag="NNpl,indef,gen"/>
    <form  suffix="ums" tag="NNsg,def,gen"/>
    <form  suffix="ums" tag="NNsg,indef,gen"/>
    <form  suffix="ums-" tag="NNcm"/>
  </table>
  <table name="NN178" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="n" tag="NNpl,indef,nom"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
    <form  suffix="ns" tag="NNpl,indef,gen"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="t" tag="NNsg,def,nom"/>
    <form  suffix="ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN179" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,def,gen"/>
    <form  suffix="" tag="NNsg,def,nom"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
  </table>
  <table name="NN180" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN181" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN182" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN183" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
    <form  suffix="u" tag="NNci"/>
    <form  suffix="u" tag="NNcm"/>
    <form  suffix="u-" tag="NNci"/>
    <form  suffix="u-" tag="NNcm"/>
    <form  suffix="u-" tag="NNsms"/>
  </table>
  <table name="NN184" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="ar" tag="NNci"/>
    <form  suffix="ader" tag="NNci"/>
    <form  suffix="ader" tag="NNcm"/>
    <form  suffix="ader" tag="NNsg,indef,nom"/>
    <form  suffix="ader-" tag="NNci"/>
    <form  suffix="ader-" tag="NNcm"/>
    <form  suffix="ader-" tag="NNsms"/>
    <form  suffix="adern" tag="NNsg,def,nom"/>
    <form  suffix="aderns" tag="NNsg,def,gen"/>
    <form  suffix="aders" tag="NNsg,indef,gen"/>
    <form  suffix="ar" tag="NNcm"/>
    <form  suffix="ar" tag="NNsg,indef,nom"/>
    <form  suffix="ar-" tag="NNci"/>
    <form  suffix="ar-" tag="NNcm"/>
    <form  suffix="ar-" tag="NNsms"/>
    <form  suffix="ars" tag="NNci"/>
    <form  suffix="ars" tag="NNcm"/>
    <form  suffix="ars" tag="NNsg,indef,gen"/>
    <form  suffix="ars-" tag="NNci"/>
    <form  suffix="ars-" tag="NNcm"/>
    <form  suffix="ars-" tag="NNsms"/>
    <form  suffix="asder" tag="NNci"/>
    <form  suffix="asder" tag="NNcm"/>
    <form  suffix="asder-" tag="NNci"/>
    <form  suffix="asder-" tag="NNcm"/>
    <form  suffix="asder-" tag="NNsms"/>
    <form  suffix="äder" tag="NNpl,indef,nom"/>
    <form  suffix="äderna" tag="NNpl,def,nom"/>
    <form  suffix="ädernas" tag="NNpl,def,gen"/>
    <form  suffix="äders" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN185" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="en" tag="NNci"/>
    <form  suffix="en" tag="NNcm"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="en" tag="NNsg,indef,nom"/>
    <form  suffix="en-" tag="NNci"/>
    <form  suffix="en-" tag="NNcm"/>
    <form  suffix="en-" tag="NNsms"/>
    <form  suffix="ens" tag="NNcm"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="ens" tag="NNsg,indef,gen"/>
    <form  suffix="ens-" tag="NNcm"/>
    <form  suffix="ina" tag="NNpl,def,nom"/>
    <form  suffix="ina" tag="NNpl,indef,nom"/>
    <form  suffix="inas" tag="NNpl,def,gen"/>
    <form  suffix="inas" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN186" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="g" tag="NNci"/>
    <form  suffix="g" tag="NNcm"/>
    <form  suffix="g" tag="NNsg,indef,nom"/>
    <form  suffix="g-" tag="NNci"/>
    <form  suffix="g-" tag="NNcm"/>
    <form  suffix="g-" tag="NNsms"/>
    <form  suffix="gar" tag="NNpl,indef,nom"/>
    <form  suffix="garna" tag="NNpl,def,nom"/>
    <form  suffix="garnas" tag="NNpl,def,gen"/>
    <form  suffix="gars" tag="NNpl,indef,gen"/>
    <form  suffix="gen" tag="NNsg,def,nom"/>
    <form  suffix="gens" tag="NNsg,def,gen"/>
    <form  suffix="gs" tag="NNsg,indef,gen"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="r" tag="NNpl,indef,nom"/>
    <form  suffix="rna" tag="NNpl,def,nom"/>
    <form  suffix="rnas" tag="NNpl,def,gen"/>
    <form  suffix="rs" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN187" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="gon" tag="NNci"/>
    <form  suffix="gnar" tag="NNpl,indef,nom"/>
    <form  suffix="gnarna" tag="NNpl,def,nom"/>
    <form  suffix="gnarnas" tag="NNpl,def,gen"/>
    <form  suffix="gnars" tag="NNpl,indef,gen"/>
    <form  suffix="gon" tag="NNcm"/>
    <form  suffix="gon" tag="NNsg,indef,nom"/>
    <form  suffix="gon-" tag="NNci"/>
    <form  suffix="gon-" tag="NNcm"/>
    <form  suffix="gon-" tag="NNsms"/>
    <form  suffix="gonen" tag="NNsg,def,nom"/>
    <form  suffix="gonens" tag="NNsg,def,gen"/>
    <form  suffix="gons" tag="NNsg,indef,gen"/>
    <form  suffix="nar" tag="NNpl,indef,nom"/>
    <form  suffix="narna" tag="NNpl,def,nom"/>
    <form  suffix="narnas" tag="NNpl,def,gen"/>
    <form  suffix="nars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN188" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="on" tag="NNci"/>
    <form  suffix="nar" tag="NNpl,indef,nom"/>
    <form  suffix="narna" tag="NNpl,def,nom"/>
    <form  suffix="narnas" tag="NNpl,def,gen"/>
    <form  suffix="nars" tag="NNpl,indef,gen"/>
    <form  suffix="on" tag="NNcm"/>
    <form  suffix="on" tag="NNsg,indef,nom"/>
    <form  suffix="on-" tag="NNci"/>
    <form  suffix="on-" tag="NNcm"/>
    <form  suffix="on-" tag="NNsms"/>
    <form  suffix="onen" tag="NNsg,def,nom"/>
    <form  suffix="onens" tag="NNsg,def,gen"/>
    <form  suffix="ons" tag="NNcm"/>
    <form  suffix="ons" tag="NNsg,indef,gen"/>
    <form  suffix="ons-" tag="NNcm"/>
  </table>
  <table name="NN189" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="otter" tag="NNci"/>
    <form  suffix="otter" tag="NNcm"/>
    <form  suffix="otter" tag="NNsg,indef,nom"/>
    <form  suffix="otter-" tag="NNci"/>
    <form  suffix="otter-" tag="NNcm"/>
    <form  suffix="otter-" tag="NNsms"/>
    <form  suffix="ottern" tag="NNsg,def,nom"/>
    <form  suffix="otterns" tag="NNsg,def,gen"/>
    <form  suffix="otters" tag="NNcm"/>
    <form  suffix="otters" tag="NNsg,indef,gen"/>
    <form  suffix="otters-" tag="NNcm"/>
    <form  suffix="öttrar" tag="NNpl,indef,nom"/>
    <form  suffix="öttrarna" tag="NNpl,def,nom"/>
    <form  suffix="öttrarnas" tag="NNpl,def,gen"/>
    <form  suffix="öttrars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN190" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="are" tag="NNpl,indef,nom"/>
    <form  suffix="ares" tag="NNpl,indef,gen"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN191" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="es" tag="NNci"/>
    <form  suffix="es" tag="NNcm"/>
    <form  suffix="es-" tag="NNci"/>
    <form  suffix="es-" tag="NNcm"/>
    <form  suffix="es-" tag="NNsms"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN192" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="att" tag="NNci"/>
    <form  suffix="att" tag="NNcm"/>
    <form  suffix="att" tag="NNsg,indef,nom"/>
    <form  suffix="att-" tag="NNci"/>
    <form  suffix="att-" tag="NNcm"/>
    <form  suffix="att-" tag="NNsms"/>
    <form  suffix="atten" tag="NNsg,def,nom"/>
    <form  suffix="attens" tag="NNsg,def,gen"/>
    <form  suffix="atts" tag="NNcm"/>
    <form  suffix="atts" tag="NNsg,indef,gen"/>
    <form  suffix="atts-" tag="NNcm"/>
    <form  suffix="ätter" tag="NNpl,indef,nom"/>
    <form  suffix="ätterna" tag="NNpl,def,nom"/>
    <form  suffix="ätternas" tag="NNpl,def,gen"/>
    <form  suffix="ätters" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN193" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="d" tag="NNci"/>
    <form  suffix="d" tag="NNcm"/>
    <form  suffix="d" tag="NNpl,indef,nom"/>
    <form  suffix="d" tag="NNsg,indef,nom"/>
    <form  suffix="d-" tag="NNci"/>
    <form  suffix="d-" tag="NNcm"/>
    <form  suffix="d-" tag="NNsms"/>
    <form  suffix="den" tag="NNpl,def,nom"/>
    <form  suffix="dens" tag="NNpl,def,gen"/>
    <form  suffix="det" tag="NNsg,def,nom"/>
    <form  suffix="dets" tag="NNsg,def,gen"/>
    <form  suffix="ds" tag="NNpl,indef,gen"/>
    <form  suffix="ds" tag="NNsg,indef,gen"/>
    <form  suffix="n" tag="NNpl,indef,nom"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
    <form  suffix="ns" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN194" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="en" tag="NNci"/>
    <form  suffix="en" tag="NNcm"/>
    <form  suffix="en" tag="NNsg,indef,nom"/>
    <form  suffix="en-" tag="NNci"/>
    <form  suffix="en-" tag="NNcm"/>
    <form  suffix="en-" tag="NNsms"/>
    <form  suffix="enn" tag="NNsg,def,nom"/>
    <form  suffix="enns" tag="NNsg,def,gen"/>
    <form  suffix="ens" tag="NNcm"/>
    <form  suffix="ens" tag="NNsg,indef,gen"/>
    <form  suffix="ens-" tag="NNcm"/>
    <form  suffix="nar" tag="NNpl,indef,nom"/>
    <form  suffix="narna" tag="NNpl,def,nom"/>
    <form  suffix="narnas" tag="NNpl,def,gen"/>
    <form  suffix="nars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN195" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN196" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN197" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="n" tag="NNpl,indef,nom"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
    <form  suffix="ns" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="t" tag="NNsg,def,nom"/>
    <form  suffix="ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN198" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN199" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="men" tag="NNsg,def,nom"/>
    <form  suffix="mens" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN200" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="nar" tag="NNpl,indef,nom"/>
    <form  suffix="narna" tag="NNpl,def,nom"/>
    <form  suffix="narnas" tag="NNpl,def,gen"/>
    <form  suffix="nars" tag="NNpl,indef,gen"/>
    <form  suffix="nen" tag="NNsg,def,nom"/>
    <form  suffix="nens" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN201" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="-en" tag="NNpl,def,nom"/>
    <form  suffix="-ens" tag="NNpl,def,gen"/>
    <form  suffix="-et" tag="NNsg,def,nom"/>
    <form  suffix="-ets" tag="NNsg,def,gen"/>
    <form  suffix="-s" tag="NNpl,indef,gen"/>
    <form  suffix="-s" tag="NNsg,indef,gen"/>
    <form  suffix=":en" tag="NNpl,def,nom"/>
    <form  suffix=":ens" tag="NNpl,def,gen"/>
    <form  suffix=":et" tag="NNsg,def,nom"/>
    <form  suffix=":ets" tag="NNsg,def,gen"/>
    <form  suffix=":s" tag="NNpl,indef,gen"/>
    <form  suffix=":s" tag="NNsg,indef,gen"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN202" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="a" tag="NNci"/>
    <form  suffix="a" tag="NNcm"/>
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="a-" tag="NNci"/>
    <form  suffix="a-" tag="NNcm"/>
    <form  suffix="a-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN203" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="a" tag="NNci"/>
    <form  suffix="a" tag="NNcm"/>
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="a-" tag="NNci"/>
    <form  suffix="a-" tag="NNcm"/>
    <form  suffix="a-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN204" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="an" tag="NNsg,indef,nom"/>
    <form  suffix="ans" tag="NNci"/>
    <form  suffix="ans" tag="NNcm"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="ans" tag="NNsg,indef,gen"/>
    <form  suffix="ans-" tag="NNci"/>
    <form  suffix="ans-" tag="NNcm"/>
    <form  suffix="ans-" tag="NNsms"/>
    <form  suffix="ingar" tag="NNpl,indef,nom"/>
    <form  suffix="ingarna" tag="NNpl,def,nom"/>
    <form  suffix="ingarnas" tag="NNpl,def,gen"/>
    <form  suffix="ingars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN205" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="e" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="e" tag="NNsg,indef,nom"/>
    <form  suffix="en" tag="NNpl,indef,nom"/>
    <form  suffix="ens" tag="NNpl,indef,gen"/>
    <form  suffix="es" tag="NNpl,indef,gen"/>
    <form  suffix="es" tag="NNsg,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
  </table>
  <table name="NN206" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="er" tag="NNci"/>
    <form  suffix="er" tag="NNcm"/>
    <form  suffix="er" tag="NNsg,indef,nom"/>
    <form  suffix="er-" tag="NNci"/>
    <form  suffix="er-" tag="NNcm"/>
    <form  suffix="er-" tag="NNsms"/>
    <form  suffix="ern" tag="NNsg,def,nom"/>
    <form  suffix="erns" tag="NNsg,def,gen"/>
    <form  suffix="ers" tag="NNcm"/>
    <form  suffix="ers" tag="NNsg,indef,gen"/>
    <form  suffix="ers-" tag="NNcm"/>
    <form  suffix="rar" tag="NNpl,indef,nom"/>
    <form  suffix="rarna" tag="NNpl,def,nom"/>
    <form  suffix="rarnas" tag="NNpl,def,gen"/>
    <form  suffix="rars" tag="NNpl,indef,gen"/>
    <form  suffix="rer" tag="NNpl,indef,nom"/>
    <form  suffix="rerna" tag="NNpl,def,nom"/>
    <form  suffix="rernas" tag="NNpl,def,gen"/>
    <form  suffix="rers" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN207" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="mel" tag="NNci"/>
    <form  suffix="let" tag="NNsg,def,nom"/>
    <form  suffix="lets" tag="NNsg,def,gen"/>
    <form  suffix="mel" tag="NNcm"/>
    <form  suffix="mel" tag="NNsg,indef,nom"/>
    <form  suffix="mel-" tag="NNci"/>
    <form  suffix="mel-" tag="NNcm"/>
    <form  suffix="mel-" tag="NNsms"/>
    <form  suffix="mels" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN208" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="on" tag="NNci"/>
    <form  suffix="on" tag="NNcm"/>
    <form  suffix="on" tag="NNsg,indef,nom"/>
    <form  suffix="on-" tag="NNci"/>
    <form  suffix="on-" tag="NNcm"/>
    <form  suffix="on-" tag="NNsms"/>
    <form  suffix="onen" tag="NNsg,def,nom"/>
    <form  suffix="onens" tag="NNsg,def,gen"/>
    <form  suffix="ons" tag="NNsg,indef,gen"/>
    <form  suffix="öner" tag="NNpl,indef,nom"/>
    <form  suffix="önerna" tag="NNpl,def,nom"/>
    <form  suffix="önernas" tag="NNpl,def,gen"/>
    <form  suffix="öners" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN209" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="um" tag="NNci"/>
    <form  suffix="a" tag="NNpl,indef,nom"/>
    <form  suffix="ana" tag="NNpl,def,nom"/>
    <form  suffix="anas" tag="NNpl,def,gen"/>
    <form  suffix="as" tag="NNpl,indef,gen"/>
    <form  suffix="um" tag="NNcm"/>
    <form  suffix="um" tag="NNsg,def,nom"/>
    <form  suffix="um" tag="NNsg,indef,nom"/>
    <form  suffix="um-" tag="NNci"/>
    <form  suffix="um-" tag="NNcm"/>
    <form  suffix="um-" tag="NNsms"/>
    <form  suffix="umet" tag="NNsg,def,nom"/>
    <form  suffix="umets" tag="NNsg,def,gen"/>
    <form  suffix="ums" tag="NNsg,def,gen"/>
    <form  suffix="ums" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN210" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="y" tag="NNci"/>
    <form  suffix="ier" tag="NNpl,indef,nom"/>
    <form  suffix="ierna" tag="NNpl,def,nom"/>
    <form  suffix="iernas" tag="NNpl,def,gen"/>
    <form  suffix="iers" tag="NNpl,indef,gen"/>
    <form  suffix="ies" tag="NNpl,indef,gen"/>
    <form  suffix="ies" tag="NNpl,indef,nom"/>
    <form  suffix="iesarna" tag="NNpl,def,nom"/>
    <form  suffix="iesarnas" tag="NNpl,def,gen"/>
    <form  suffix="y" tag="NNcm"/>
    <form  suffix="y" tag="NNsg,indef,nom"/>
    <form  suffix="y-" tag="NNci"/>
    <form  suffix="y-" tag="NNcm"/>
    <form  suffix="y-" tag="NNsms"/>
    <form  suffix="yer" tag="NNpl,indef,nom"/>
    <form  suffix="yerna" tag="NNpl,def,nom"/>
    <form  suffix="yernas" tag="NNpl,def,gen"/>
    <form  suffix="yers" tag="NNpl,indef,gen"/>
    <form  suffix="yn" tag="NNsg,def,nom"/>
    <form  suffix="yns" tag="NNsg,def,gen"/>
    <form  suffix="ys" tag="NNsg,indef,gen"/>
    <form  suffix="ysar" tag="NNpl,indef,nom"/>
    <form  suffix="ysarna" tag="NNpl,def,nom"/>
    <form  suffix="ysarnas" tag="NNpl,def,gen"/>
    <form  suffix="ysars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN211" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,gen"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN212" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="a" tag="NNci"/>
    <form  suffix="a" tag="NNcm"/>
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="a-" tag="NNci"/>
    <form  suffix="a-" tag="NNcm"/>
    <form  suffix="a-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN213" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="ader" tag="NNci"/>
    <form  suffix="ader" tag="NNcm"/>
    <form  suffix="ader" tag="NNsg,indef,nom"/>
    <form  suffix="ader-" tag="NNci"/>
    <form  suffix="ader-" tag="NNcm"/>
    <form  suffix="ader-" tag="NNsms"/>
    <form  suffix="adern" tag="NNsg,def,nom"/>
    <form  suffix="aderns" tag="NNsg,def,gen"/>
    <form  suffix="aders" tag="NNci"/>
    <form  suffix="aders" tag="NNcm"/>
    <form  suffix="aders" tag="NNsg,indef,gen"/>
    <form  suffix="aders-" tag="NNci"/>
    <form  suffix="aders-" tag="NNcm"/>
    <form  suffix="aders-" tag="NNsms"/>
    <form  suffix="äder" tag="NNpl,indef,nom"/>
    <form  suffix="äderna" tag="NNpl,def,nom"/>
    <form  suffix="ädernas" tag="NNpl,def,gen"/>
    <form  suffix="äders" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN214" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="an" tag="NNsg,indef,nom"/>
    <form  suffix="ans" tag="NNci"/>
    <form  suffix="ans" tag="NNcm"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="ans" tag="NNsg,indef,gen"/>
    <form  suffix="ans-" tag="NNci"/>
    <form  suffix="ans-" tag="NNcm"/>
    <form  suffix="ans-" tag="NNsms"/>
    <form  suffix="ningar" tag="NNpl,indef,nom"/>
    <form  suffix="ningarna" tag="NNpl,def,nom"/>
    <form  suffix="ningarnas" tag="NNpl,def,gen"/>
    <form  suffix="ningars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN215" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="ra" tag="NNsg,indef,nom"/>
    <form  suffix="er" tag="NNci"/>
    <form  suffix="er" tag="NNcm"/>
    <form  suffix="er-" tag="NNci"/>
    <form  suffix="er-" tag="NNcm"/>
    <form  suffix="er-" tag="NNsms"/>
    <form  suffix="ran" tag="NNsg,def,nom"/>
    <form  suffix="rans" tag="NNsg,def,gen"/>
    <form  suffix="ras" tag="NNsg,indef,gen"/>
    <form  suffix="re" tag="NNcm"/>
    <form  suffix="re-" tag="NNcm"/>
    <form  suffix="ror" tag="NNpl,indef,nom"/>
    <form  suffix="rorna" tag="NNpl,def,nom"/>
    <form  suffix="rornas" tag="NNpl,def,gen"/>
    <form  suffix="rors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN216" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="um" tag="NNci"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="um" tag="NNcm"/>
    <form  suffix="um" tag="NNsg,def,nom"/>
    <form  suffix="um" tag="NNsg,indef,nom"/>
    <form  suffix="um-" tag="NNci"/>
    <form  suffix="um-" tag="NNcm"/>
    <form  suffix="um-" tag="NNsms"/>
    <form  suffix="umet" tag="NNsg,def,nom"/>
    <form  suffix="umets" tag="NNsg,def,gen"/>
    <form  suffix="ums" tag="NNsg,def,gen"/>
    <form  suffix="ums" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN217" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="ås" tag="NNci"/>
    <form  suffix="äss" tag="NNpl,indef,gen"/>
    <form  suffix="äss" tag="NNpl,indef,nom"/>
    <form  suffix="ässen" tag="NNpl,def,nom"/>
    <form  suffix="ässens" tag="NNpl,def,gen"/>
    <form  suffix="ås" tag="NNcm"/>
    <form  suffix="ås" tag="NNsg,indef,gen"/>
    <form  suffix="ås" tag="NNsg,indef,nom"/>
    <form  suffix="ås-" tag="NNci"/>
    <form  suffix="ås-" tag="NNcm"/>
    <form  suffix="ås-" tag="NNsms"/>
    <form  suffix="åsen" tag="NNsg,def,nom"/>
    <form  suffix="åsens" tag="NNsg,def,gen"/>
  </table>
  <table name="NN218" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,gen"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN219" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN220" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="e" tag="NNpl,indef,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ena" tag="NNpl,def,nom"/>
    <form  suffix="enas" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="es" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN221" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNpl,indef,nom"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN222" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="r" tag="NNpl,indef,nom"/>
    <form  suffix="rna" tag="NNpl,def,nom"/>
    <form  suffix="rnas" tag="NNpl,def,gen"/>
    <form  suffix="rs" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="t" tag="NNsg,def,nom"/>
    <form  suffix="ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN223" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="r" tag="NNpl,indef,nom"/>
    <form  suffix="rna" tag="NNpl,def,nom"/>
    <form  suffix="rnas" tag="NNpl,def,gen"/>
    <form  suffix="rs" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="t" tag="NNsg,def,nom"/>
    <form  suffix="ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN224" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="o" tag="NNci"/>
    <form  suffix="o" tag="NNcm"/>
    <form  suffix="o-" tag="NNci"/>
    <form  suffix="o-" tag="NNcm"/>
    <form  suffix="o-" tag="NNsms"/>
  </table>
  <table name="NN225" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="av" tag="NNci"/>
    <form  suffix="av" tag="NNcm"/>
    <form  suffix="av" tag="NNsg,indef,nom"/>
    <form  suffix="av-" tag="NNci"/>
    <form  suffix="av-" tag="NNcm"/>
    <form  suffix="av-" tag="NNsms"/>
    <form  suffix="aven" tag="NNsg,def,nom"/>
    <form  suffix="avens" tag="NNsg,def,gen"/>
    <form  suffix="avs" tag="NNcm"/>
    <form  suffix="avs" tag="NNsg,indef,gen"/>
    <form  suffix="avs-" tag="NNcm"/>
    <form  suffix="äver" tag="NNpl,indef,nom"/>
    <form  suffix="äverna" tag="NNpl,def,nom"/>
    <form  suffix="ävernas" tag="NNpl,def,gen"/>
    <form  suffix="ävers" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN226" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="el" tag="NNci"/>
    <form  suffix="el" tag="NNcm"/>
    <form  suffix="el" tag="NNpl,indef,nom"/>
    <form  suffix="el" tag="NNsg,indef,nom"/>
    <form  suffix="el-" tag="NNci"/>
    <form  suffix="el-" tag="NNcm"/>
    <form  suffix="el-" tag="NNsms"/>
    <form  suffix="els" tag="NNpl,indef,gen"/>
    <form  suffix="els" tag="NNsg,indef,gen"/>
    <form  suffix="len" tag="NNpl,def,nom"/>
    <form  suffix="lens" tag="NNpl,def,gen"/>
    <form  suffix="ler" tag="NNpl,indef,nom"/>
    <form  suffix="lerna" tag="NNpl,def,nom"/>
    <form  suffix="lernas" tag="NNpl,def,gen"/>
    <form  suffix="lers" tag="NNpl,indef,gen"/>
    <form  suffix="let" tag="NNsg,def,nom"/>
    <form  suffix="lets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN227" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="mel" tag="NNci"/>
    <form  suffix="lar" tag="NNpl,indef,nom"/>
    <form  suffix="larna" tag="NNpl,def,nom"/>
    <form  suffix="larnas" tag="NNpl,def,gen"/>
    <form  suffix="lars" tag="NNpl,indef,gen"/>
    <form  suffix="len" tag="NNsg,def,nom"/>
    <form  suffix="lens" tag="NNsg,def,gen"/>
    <form  suffix="mel" tag="NNcm"/>
    <form  suffix="mel" tag="NNsg,indef,nom"/>
    <form  suffix="mel-" tag="NNci"/>
    <form  suffix="mel-" tag="NNcm"/>
    <form  suffix="mel-" tag="NNsms"/>
    <form  suffix="melen" tag="NNsg,def,nom"/>
    <form  suffix="melens" tag="NNsg,def,gen"/>
    <form  suffix="meln" tag="NNsg,def,nom"/>
    <form  suffix="melns" tag="NNsg,def,gen"/>
    <form  suffix="mels" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN228" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="mel" tag="NNci"/>
    <form  suffix="lar" tag="NNpl,indef,nom"/>
    <form  suffix="larna" tag="NNpl,def,nom"/>
    <form  suffix="larnas" tag="NNpl,def,gen"/>
    <form  suffix="lars" tag="NNpl,indef,gen"/>
    <form  suffix="mel" tag="NNcm"/>
    <form  suffix="mel" tag="NNsg,indef,nom"/>
    <form  suffix="mel-" tag="NNci"/>
    <form  suffix="mel-" tag="NNcm"/>
    <form  suffix="mel-" tag="NNsms"/>
    <form  suffix="meln" tag="NNsg,def,nom"/>
    <form  suffix="melns" tag="NNsg,def,gen"/>
    <form  suffix="mels" tag="NNcm"/>
    <form  suffix="mels" tag="NNsg,indef,gen"/>
    <form  suffix="mels-" tag="NNcm"/>
    <form  suffix="mlar" tag="NNpl,indef,nom"/>
    <form  suffix="mlarna" tag="NNpl,def,nom"/>
    <form  suffix="mlarnas" tag="NNpl,def,gen"/>
    <form  suffix="mlars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN229" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="on" tag="NNci"/>
    <form  suffix="a" tag="NNpl,indef,nom"/>
    <form  suffix="as" tag="NNpl,indef,gen"/>
    <form  suffix="on" tag="NNcm"/>
    <form  suffix="on" tag="NNpl,indef,nom"/>
    <form  suffix="on" tag="NNsg,indef,nom"/>
    <form  suffix="on-" tag="NNci"/>
    <form  suffix="on-" tag="NNcm"/>
    <form  suffix="on-" tag="NNsms"/>
    <form  suffix="onen" tag="NNpl,def,nom"/>
    <form  suffix="onens" tag="NNpl,def,gen"/>
    <form  suffix="onet" tag="NNsg,def,nom"/>
    <form  suffix="onets" tag="NNsg,def,gen"/>
    <form  suffix="ons" tag="NNpl,indef,gen"/>
    <form  suffix="ons" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN230" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="ot" tag="NNci"/>
    <form  suffix="ot" tag="NNcm"/>
    <form  suffix="ot" tag="NNsg,indef,nom"/>
    <form  suffix="ot-" tag="NNci"/>
    <form  suffix="ot-" tag="NNcm"/>
    <form  suffix="ot-" tag="NNsms"/>
    <form  suffix="oten" tag="NNsg,def,nom"/>
    <form  suffix="otens" tag="NNsg,def,gen"/>
    <form  suffix="ots" tag="NNsg,indef,gen"/>
    <form  suffix="öter" tag="NNpl,indef,nom"/>
    <form  suffix="öterna" tag="NNpl,def,nom"/>
    <form  suffix="öternas" tag="NNpl,def,gen"/>
    <form  suffix="öters" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN231" rads=".*(gauss|muffins|hertz|hammerless|siemens|lux)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,gen"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
  </table>
  <table name="NN232" rads=".*(deficit|vakuum|vacuum|korum|tedeum|universum)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,def,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN233" rads=".*(franc|gallon|yard|skateboard|schweizerfranc|shilling)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNpl,indef,nom"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="sen" tag="NNpl,def,nom"/>
    <form  suffix="sens" tag="NNpl,def,gen"/>
  </table>
  <table name="NN234" rads=".*(halvtum|tum|glim|nattglim|smällglim|verktum)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="men" tag="NNpl,def,nom"/>
    <form  suffix="men" tag="NNsg,def,nom"/>
    <form  suffix="mens" tag="NNpl,def,gen"/>
    <form  suffix="mens" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN235" rads=".*(registerton|gigaton|bruttoregisterton|kiloton|megaton|nettoregisterton)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="nen" tag="NNpl,def,nom"/>
    <form  suffix="nens" tag="NNpl,def,gen"/>
    <form  suffix="net" tag="NNsg,def,nom"/>
    <form  suffix="nets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN236" rads=".*(karbon|magröntgen|sanskrit|renat|kontraströntgen|röntgen)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,def,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN237" rads=".*(slafs|fräs|boss|godis|kross|box)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN238" rads=".*(ruff|ring|skit|klabb|smäll|dunk)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN239" rads=".*(larv|doft|titan|grand|astrakan|neodym)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN240" rads=".*(sjustjärnorna|tropikerna|apokryferna|spenderbyxorna|balkanländerna|småtimmarna)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="NNpl,def,nom"/>
    <form  suffix="s" tag="NNpl,def,gen"/>
  </table>
  <table name="NN241" rads=".*(mp3|T9|jod-131|MP3|cesium-137|kol-14)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="-s" tag="NNsg,indef,gen"/>
    <form  suffix=":s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN242" rads=".*(löjtnantshjärt|pansarhjärt|modershjärt|fetthjärt|hjärt|tigerhjärt)" fast="-">
    <!-- 6 members -->
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="an" tag="NNpl,indef,nom"/>
    <form  suffix="ana" tag="NNpl,def,nom"/>
    <form  suffix="anas" tag="NNpl,def,gen"/>
    <form  suffix="ans" tag="NNpl,indef,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="at" tag="NNsg,def,nom"/>
    <form  suffix="ats" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN243" rads=".*(nick|ströss|raff|kis|dreg|dräg)" fast="-">
    <!-- 6 members -->
    <form  suffix="el" tag="NNci"/>
    <form  suffix="el" tag="NNcm"/>
    <form  suffix="el" tag="NNsg,indef,nom"/>
    <form  suffix="el-" tag="NNci"/>
    <form  suffix="el-" tag="NNcm"/>
    <form  suffix="el-" tag="NNsms"/>
    <form  suffix="eln" tag="NNsg,def,nom"/>
    <form  suffix="elns" tag="NNsg,def,gen"/>
    <form  suffix="els" tag="NNsg,indef,gen"/>
    <form  suffix="let" tag="NNsg,def,nom"/>
    <form  suffix="lets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN244" rads=".*(mjölkåd|pulsåd|blodåd|lungpulsåd|åd|källåd)" fast="-">
    <!-- 6 members -->
    <form  suffix="er" tag="NNci"/>
    <form  suffix="er" tag="NNcm"/>
    <form  suffix="er" tag="NNsg,indef,nom"/>
    <form  suffix="er-" tag="NNci"/>
    <form  suffix="er-" tag="NNcm"/>
    <form  suffix="er-" tag="NNsms"/>
    <form  suffix="ern" tag="NNsg,def,nom"/>
    <form  suffix="erns" tag="NNsg,def,gen"/>
    <form  suffix="ers" tag="NNsg,indef,gen"/>
    <form  suffix="ra" tag="NNsg,indef,nom"/>
    <form  suffix="ran" tag="NNsg,def,nom"/>
    <form  suffix="rans" tag="NNsg,def,gen"/>
    <form  suffix="ras" tag="NNsg,indef,gen"/>
    <form  suffix="re" tag="NNcm"/>
    <form  suffix="re-" tag="NNcm"/>
    <form  suffix="ror" tag="NNpl,indef,nom"/>
    <form  suffix="rorna" tag="NNpl,def,nom"/>
    <form  suffix="rornas" tag="NNpl,def,gen"/>
    <form  suffix="rors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN245" rads=".*(eftersom|sensom|försom|indiansom|högsom|brittsom)" fast="-">
    <!-- 6 members -->
    <form  suffix="mar" tag="NNci"/>
    <form  suffix="mar" tag="NNcm"/>
    <form  suffix="mar" tag="NNsg,indef,nom"/>
    <form  suffix="mar-" tag="NNci"/>
    <form  suffix="mar-" tag="NNcm"/>
    <form  suffix="mar-" tag="NNsms"/>
    <form  suffix="maren" tag="NNsg,def,nom"/>
    <form  suffix="marens" tag="NNsg,def,gen"/>
    <form  suffix="marn" tag="NNsg,def,nom"/>
    <form  suffix="marns" tag="NNsg,def,gen"/>
    <form  suffix="mars" tag="NNsg,indef,gen"/>
    <form  suffix="rar" tag="NNpl,indef,nom"/>
    <form  suffix="rarna" tag="NNpl,def,nom"/>
    <form  suffix="rarnas" tag="NNpl,def,gen"/>
    <form  suffix="rars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN246" rads=".*(flim|förmaksflim|skim|hjärtflim|slam|sagoskim)" fast="-">
    <!-- 6 members -->
    <form  suffix="mer" tag="NNci"/>
    <form  suffix="mer" tag="NNcm"/>
    <form  suffix="mer" tag="NNsg,indef,nom"/>
    <form  suffix="mer-" tag="NNci"/>
    <form  suffix="mer-" tag="NNcm"/>
    <form  suffix="mer-" tag="NNsms"/>
    <form  suffix="mers" tag="NNsg,indef,gen"/>
    <form  suffix="ret" tag="NNsg,def,nom"/>
    <form  suffix="rets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN247" rads=".*(styvm|svärm|farm|husm|ungm|morm)" fast="-">
    <!-- 6 members -->
    <form  suffix="or" tag="NNci"/>
    <form  suffix="oder" tag="NNci"/>
    <form  suffix="oder" tag="NNcm"/>
    <form  suffix="oder" tag="NNsg,indef,nom"/>
    <form  suffix="oder-" tag="NNci"/>
    <form  suffix="oder-" tag="NNcm"/>
    <form  suffix="oder-" tag="NNsms"/>
    <form  suffix="odern" tag="NNsg,def,nom"/>
    <form  suffix="oderns" tag="NNsg,def,gen"/>
    <form  suffix="oders" tag="NNci"/>
    <form  suffix="oders" tag="NNcm"/>
    <form  suffix="oders" tag="NNsg,indef,gen"/>
    <form  suffix="oders-" tag="NNci"/>
    <form  suffix="oders-" tag="NNcm"/>
    <form  suffix="oders-" tag="NNsms"/>
    <form  suffix="or" tag="NNcm"/>
    <form  suffix="or" tag="NNsg,indef,nom"/>
    <form  suffix="or-" tag="NNci"/>
    <form  suffix="or-" tag="NNcm"/>
    <form  suffix="or-" tag="NNsms"/>
    <form  suffix="ors" tag="NNci"/>
    <form  suffix="ors" tag="NNcm"/>
    <form  suffix="ors" tag="NNsg,indef,gen"/>
    <form  suffix="ors-" tag="NNci"/>
    <form  suffix="ors-" tag="NNcm"/>
    <form  suffix="ors-" tag="NNsms"/>
    <form  suffix="ödrar" tag="NNpl,indef,nom"/>
    <form  suffix="ödrarna" tag="NNpl,def,nom"/>
    <form  suffix="ödrarnas" tag="NNpl,def,gen"/>
    <form  suffix="ödrars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN248" rads=".*(vattenkopp|inälv|linblån|vattkopp|småhack|kokopp)" fast="-">
    <!-- 6 members -->
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN249" rads=".*(bärs|klematis|pyjamas|bellis|bagis)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,gen"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
  </table>
  <table name="NN250" rads=".*(cig|schilling|cigg|samizdat|krysantemum)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN251" rads=".*(ferrum|blodserum|gluten|ormserum|sanningsserum)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,def,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN252" rads=".*(slask|vask|visp|gräddvisp|rasp)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN253" rads=".*(galon|krasch|gabon|elektron|primat)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN254" rads=".*(sari|tsunami|snällhetsguru|guru|safari)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNpl,indef,nom"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="sarna" tag="NNpl,def,nom"/>
    <form  suffix="sarnas" tag="NNpl,def,gen"/>
  </table>
  <table name="NN255" rads=".*(madam|parallellogram|väggmadam|kraftparallellogram|roddarmadam)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="men" tag="NNsg,def,nom"/>
    <form  suffix="mens" tag="NNsg,def,gen"/>
    <form  suffix="mer" tag="NNpl,indef,nom"/>
    <form  suffix="merna" tag="NNpl,def,nom"/>
    <form  suffix="mernas" tag="NNpl,def,gen"/>
    <form  suffix="mers" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN256" rads=".*(stigma|huvudtema|sångtema|sidotema|tema)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="n" tag="NNpl,indef,nom"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
    <form  suffix="ns" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="t" tag="NNsg,def,nom"/>
    <form  suffix="ta" tag="NNpl,indef,nom"/>
    <form  suffix="tana" tag="NNpl,def,nom"/>
    <form  suffix="tanas" tag="NNpl,def,gen"/>
    <form  suffix="tas" tag="NNpl,indef,gen"/>
    <form  suffix="ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN257" rads=".*(vidkommande|samboende|förmenande|beroende|föregående)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="n" tag="NNpl,indef,nom"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
    <form  suffix="ns" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="t" tag="NNsg,def,nom"/>
    <form  suffix="ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN258" rads=".*(blomklas|bisonox|björnbärsbusk|bisamox|blankborst)" fast="-">
    <!-- 5 members -->
    <form  suffix="e" tag="NNci"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e" tag="NNsg,indef,nom"/>
    <form  suffix="e-" tag="NNci"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="e-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="es" tag="NNci"/>
    <form  suffix="es" tag="NNcm"/>
    <form  suffix="es" tag="NNsg,indef,gen"/>
    <form  suffix="es-" tag="NNci"/>
    <form  suffix="es-" tag="NNcm"/>
    <form  suffix="es-" tag="NNsms"/>
  </table>
  <table name="NN259" rads=".*(vägtun|biltun|vindtun|tun|ken)" fast="-">
    <!-- 5 members -->
    <form  suffix="nel" tag="NNci"/>
    <form  suffix="lar" tag="NNpl,indef,nom"/>
    <form  suffix="larna" tag="NNpl,def,nom"/>
    <form  suffix="larnas" tag="NNpl,def,gen"/>
    <form  suffix="lars" tag="NNpl,indef,gen"/>
    <form  suffix="nel" tag="NNcm"/>
    <form  suffix="nel" tag="NNsg,indef,nom"/>
    <form  suffix="nel-" tag="NNci"/>
    <form  suffix="nel-" tag="NNcm"/>
    <form  suffix="nel-" tag="NNsms"/>
    <form  suffix="neln" tag="NNsg,def,nom"/>
    <form  suffix="nelns" tag="NNsg,def,gen"/>
    <form  suffix="nels" tag="NNcm"/>
    <form  suffix="nels" tag="NNsg,indef,gen"/>
    <form  suffix="nels-" tag="NNcm"/>
    <form  suffix="nlar" tag="NNpl,indef,nom"/>
    <form  suffix="nlarna" tag="NNpl,def,nom"/>
    <form  suffix="nlarnas" tag="NNpl,def,gen"/>
    <form  suffix="nlars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN260" rads=".*(dubr|farbr|morbr|halvbr|br)" fast="-">
    <!-- 5 members -->
    <form  suffix="or" tag="NNci"/>
    <form  suffix="odern" tag="NNsg,def,nom"/>
    <form  suffix="oderns" tag="NNsg,def,gen"/>
    <form  suffix="or" tag="NNcm"/>
    <form  suffix="or" tag="NNsg,indef,nom"/>
    <form  suffix="or-" tag="NNci"/>
    <form  suffix="or-" tag="NNcm"/>
    <form  suffix="or-" tag="NNsms"/>
    <form  suffix="orn" tag="NNsg,def,nom"/>
    <form  suffix="orns" tag="NNsg,def,gen"/>
    <form  suffix="ors" tag="NNci"/>
    <form  suffix="ors" tag="NNcm"/>
    <form  suffix="ors" tag="NNsg,indef,gen"/>
    <form  suffix="ors-" tag="NNci"/>
    <form  suffix="ors-" tag="NNcm"/>
    <form  suffix="ors-" tag="NNsms"/>
    <form  suffix="öder" tag="NNpl,indef,nom"/>
    <form  suffix="öderna" tag="NNpl,def,nom"/>
    <form  suffix="ödernas" tag="NNpl,def,gen"/>
    <form  suffix="öders" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN261" rads=".*(bann|skrubb|palt|brill|grill)" fast="-">
    <!-- 5 members -->
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN262" rads=".*(minim|maxim|optim|kontinu|existensminim)" fast="-">
    <!-- 5 members -->
    <form  suffix="um" tag="NNci"/>
    <form  suffix="a" tag="NNpl,indef,nom"/>
    <form  suffix="ana" tag="NNpl,def,nom"/>
    <form  suffix="anas" tag="NNpl,def,gen"/>
    <form  suffix="as" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="um" tag="NNcm"/>
    <form  suffix="um" tag="NNpl,indef,nom"/>
    <form  suffix="um" tag="NNsg,def,nom"/>
    <form  suffix="um" tag="NNsg,indef,nom"/>
    <form  suffix="um-" tag="NNci"/>
    <form  suffix="um-" tag="NNcm"/>
    <form  suffix="um-" tag="NNsms"/>
    <form  suffix="umen" tag="NNpl,def,nom"/>
    <form  suffix="umens" tag="NNpl,def,gen"/>
    <form  suffix="umet" tag="NNsg,def,nom"/>
    <form  suffix="umets" tag="NNsg,def,gen"/>
    <form  suffix="ums" tag="NNpl,indef,gen"/>
    <form  suffix="ums" tag="NNsg,def,gen"/>
    <form  suffix="ums" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN263" rads=".*(gerundi|preterit|gerundiv|futur|supin)" fast="-">
    <!-- 5 members -->
    <form  suffix="um" tag="NNci"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="um" tag="NNcm"/>
    <form  suffix="um" tag="NNsg,def,nom"/>
    <form  suffix="um" tag="NNsg,indef,nom"/>
    <form  suffix="um-" tag="NNci"/>
    <form  suffix="um-" tag="NNcm"/>
    <form  suffix="um-" tag="NNsms"/>
    <form  suffix="umet" tag="NNsg,def,nom"/>
    <form  suffix="umets" tag="NNsg,def,gen"/>
    <form  suffix="ums" tag="NNsg,def,gen"/>
    <form  suffix="ums" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN264" rads=".*(klimakteri|pandemoni|purgatori|deliri|periheli)" fast="-">
    <!-- 5 members -->
    <form  suffix="um" tag="NNci"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="um" tag="NNcm"/>
    <form  suffix="um" tag="NNsg,indef,nom"/>
    <form  suffix="um-" tag="NNci"/>
    <form  suffix="um-" tag="NNcm"/>
    <form  suffix="um-" tag="NNsms"/>
    <form  suffix="ums" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN265" rads=".*(surkart|äppelkart|äpplekart|kart)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN266" rads=".*(mark|mikron|förgätmigej|rad)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN267" rads=".*(biceps|penis|focus|fokus)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,def,gen"/>
    <form  suffix="" tag="NNsg,def,nom"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
  </table>
  <table name="NN268" rads=".*(skank|svan|våg|centerhalv)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN269" rads=".*(rapsfrö|frö|etui|dådrafrö)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="n" tag="NNpl,indef,nom"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
    <form  suffix="ns" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="t" tag="NNsg,def,nom"/>
    <form  suffix="ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN270" rads=".*(romkom|sitkom|romcom|sitcom)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="mar" tag="NNpl,indef,nom"/>
    <form  suffix="marna" tag="NNpl,def,nom"/>
    <form  suffix="marnas" tag="NNpl,def,gen"/>
    <form  suffix="mars" tag="NNpl,indef,gen"/>
    <form  suffix="men" tag="NNsg,def,nom"/>
    <form  suffix="mens" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNpl,indef,nom"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="sen" tag="NNpl,def,nom"/>
    <form  suffix="sens" tag="NNpl,def,gen"/>
  </table>
  <table name="NN271" rads=".*(glasiglo|iglo|igloo|tango)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="r" tag="NNpl,indef,nom"/>
    <form  suffix="rna" tag="NNpl,def,nom"/>
    <form  suffix="rnas" tag="NNpl,def,gen"/>
    <form  suffix="rs" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNpl,indef,nom"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="sar" tag="NNpl,indef,nom"/>
    <form  suffix="sarna" tag="NNpl,def,nom"/>
    <form  suffix="sarnas" tag="NNpl,def,gen"/>
    <form  suffix="sars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN272" rads=".*(lasso|baldersbrå|alltiallo|libretto)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="n" tag="NNpl,indef,nom"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
    <form  suffix="ns" tag="NNpl,indef,gen"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="r" tag="NNpl,indef,nom"/>
    <form  suffix="rna" tag="NNpl,def,nom"/>
    <form  suffix="rnas" tag="NNpl,def,gen"/>
    <form  suffix="rs" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="t" tag="NNsg,def,nom"/>
    <form  suffix="ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN273" rads=".*(långfransk|dat|formfransk|småfransk)" fast="-">
    <!-- 4 members -->
    <form  suffix="a" tag="NNci"/>
    <form  suffix="a" tag="NNcm"/>
    <form  suffix="a" tag="NNpl,indef,nom"/>
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="a-" tag="NNci"/>
    <form  suffix="a-" tag="NNcm"/>
    <form  suffix="a-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ana" tag="NNpl,def,nom"/>
    <form  suffix="anas" tag="NNpl,def,gen"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNpl,indef,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="at" tag="NNsg,def,nom"/>
    <form  suffix="ats" tag="NNsg,def,gen"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN274" rads=".*(penultim|vulv|ultim|antepenultim)" fast="-">
    <!-- 4 members -->
    <form  suffix="a" tag="NNci"/>
    <form  suffix="a" tag="NNcm"/>
    <form  suffix="a" tag="NNsg,def,nom"/>
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="a-" tag="NNci"/>
    <form  suffix="a-" tag="NNcm"/>
    <form  suffix="a-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN275" rads=".*(ink|gurkh|pari|sherp)" fast="-">
    <!-- 4 members -->
    <form  suffix="a" tag="NNci"/>
    <form  suffix="a" tag="NNcm"/>
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="a-" tag="NNci"/>
    <form  suffix="a-" tag="NNcm"/>
    <form  suffix="a-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNpl,indef,gen"/>
    <form  suffix="as" tag="NNpl,indef,nom"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN276" rads=".*(herd|kyrkoherd|kamelherd|fåraherd)" fast="-">
    <!-- 4 members -->
    <form  suffix="e" tag="NNci"/>
    <form  suffix="a" tag="NNci"/>
    <form  suffix="a" tag="NNcm"/>
    <form  suffix="a-" tag="NNci"/>
    <form  suffix="a-" tag="NNcm"/>
    <form  suffix="a-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e" tag="NNsg,indef,nom"/>
    <form  suffix="e-" tag="NNci"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="e-" tag="NNsms"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="es" tag="NNcm"/>
    <form  suffix="es" tag="NNsg,indef,gen"/>
    <form  suffix="es-" tag="NNcm"/>
  </table>
  <table name="NN277" rads=".*(kas|vårdkas|drös|slas)" fast="-">
    <!-- 4 members -->
    <form  suffix="e" tag="NNsg,indef,nom"/>
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="es" tag="NNcm"/>
    <form  suffix="es" tag="NNsg,indef,gen"/>
    <form  suffix="es-" tag="NNcm"/>
  </table>
  <table name="NN278" rads=".*(mordäng|skyddsäng|ärkeäng|äng)" fast="-">
    <!-- 4 members -->
    <form  suffix="el" tag="NNci"/>
    <form  suffix="el" tag="NNcm"/>
    <form  suffix="el" tag="NNsg,indef,nom"/>
    <form  suffix="el-" tag="NNci"/>
    <form  suffix="el-" tag="NNcm"/>
    <form  suffix="el-" tag="NNsms"/>
    <form  suffix="eln" tag="NNsg,def,nom"/>
    <form  suffix="elns" tag="NNsg,def,gen"/>
    <form  suffix="els" tag="NNcm"/>
    <form  suffix="els" tag="NNsg,indef,gen"/>
    <form  suffix="els-" tag="NNcm"/>
    <form  suffix="la" tag="NNci"/>
    <form  suffix="la" tag="NNcm"/>
    <form  suffix="la-" tag="NNci"/>
    <form  suffix="la-" tag="NNcm"/>
    <form  suffix="la-" tag="NNsms"/>
    <form  suffix="lar" tag="NNpl,indef,nom"/>
    <form  suffix="larna" tag="NNpl,def,nom"/>
    <form  suffix="larnas" tag="NNpl,def,gen"/>
    <form  suffix="lars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN279" rads=".*(toff|filttoff|badtoff|trätoff)" fast="-">
    <!-- 4 members -->
    <form  suffix="el" tag="NNci"/>
    <form  suffix="el" tag="NNcm"/>
    <form  suffix="el" tag="NNsg,indef,nom"/>
    <form  suffix="el-" tag="NNci"/>
    <form  suffix="el-" tag="NNcm"/>
    <form  suffix="el-" tag="NNsms"/>
    <form  suffix="eln" tag="NNsg,def,nom"/>
    <form  suffix="elns" tag="NNsg,def,gen"/>
    <form  suffix="els" tag="NNsg,indef,gen"/>
    <form  suffix="la" tag="NNsg,indef,nom"/>
    <form  suffix="lan" tag="NNsg,def,nom"/>
    <form  suffix="lans" tag="NNsg,def,gen"/>
    <form  suffix="las" tag="NNsg,indef,gen"/>
    <form  suffix="le" tag="NNcm"/>
    <form  suffix="le-" tag="NNcm"/>
    <form  suffix="lor" tag="NNpl,indef,nom"/>
    <form  suffix="lorna" tag="NNpl,def,nom"/>
    <form  suffix="lornas" tag="NNpl,def,gen"/>
    <form  suffix="lors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN280" rads=".*(nom|relativpronom|pronom|specim)" fast="-">
    <!-- 4 members -->
    <form  suffix="en" tag="NNci"/>
    <form  suffix="en" tag="NNcm"/>
    <form  suffix="en" tag="NNpl,indef,nom"/>
    <form  suffix="en" tag="NNsg,indef,nom"/>
    <form  suffix="en-" tag="NNci"/>
    <form  suffix="en-" tag="NNcm"/>
    <form  suffix="en-" tag="NNsms"/>
    <form  suffix="enen" tag="NNpl,def,nom"/>
    <form  suffix="enens" tag="NNpl,def,gen"/>
    <form  suffix="enet" tag="NNsg,def,nom"/>
    <form  suffix="enets" tag="NNsg,def,gen"/>
    <form  suffix="ens" tag="NNcm"/>
    <form  suffix="ens" tag="NNpl,indef,gen"/>
    <form  suffix="ens" tag="NNsg,indef,gen"/>
    <form  suffix="ens-" tag="NNcm"/>
    <form  suffix="ina" tag="NNpl,def,nom"/>
    <form  suffix="ina" tag="NNpl,indef,nom"/>
    <form  suffix="inas" tag="NNpl,def,gen"/>
    <form  suffix="inas" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN281" rads=".*(fing|långfing|lillfing|pekfing)" fast="-">
    <!-- 4 members -->
    <form  suffix="er" tag="NNci"/>
    <form  suffix="er" tag="NNcm"/>
    <form  suffix="er" tag="NNsg,indef,nom"/>
    <form  suffix="er-" tag="NNci"/>
    <form  suffix="er-" tag="NNcm"/>
    <form  suffix="er-" tag="NNsms"/>
    <form  suffix="ern" tag="NNsg,def,nom"/>
    <form  suffix="erns" tag="NNsg,def,gen"/>
    <form  suffix="ers" tag="NNsg,indef,gen"/>
    <form  suffix="rar" tag="NNpl,indef,nom"/>
    <form  suffix="rarna" tag="NNpl,def,nom"/>
    <form  suffix="rarnas" tag="NNpl,def,gen"/>
    <form  suffix="rars" tag="NNpl,indef,gen"/>
    <form  suffix="ret" tag="NNsg,def,nom"/>
    <form  suffix="rets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN282" rads=".*(storstöv|johannesnyck|göknyck|sjumilastöv)" fast="-">
    <!-- 4 members -->
    <form  suffix="lar" tag="NNpl,indef,nom"/>
    <form  suffix="el" tag="NNci"/>
    <form  suffix="el" tag="NNcm"/>
    <form  suffix="el-" tag="NNci"/>
    <form  suffix="el-" tag="NNcm"/>
    <form  suffix="el-" tag="NNsms"/>
    <form  suffix="els" tag="NNci"/>
    <form  suffix="els" tag="NNcm"/>
    <form  suffix="els-" tag="NNci"/>
    <form  suffix="els-" tag="NNcm"/>
    <form  suffix="els-" tag="NNsms"/>
    <form  suffix="larna" tag="NNpl,def,nom"/>
    <form  suffix="larnas" tag="NNpl,def,gen"/>
    <form  suffix="lars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN283" rads=".*(backham|snuskhum|sum|hum)" fast="-">
    <!-- 4 members -->
    <form  suffix="mer" tag="NNci"/>
    <form  suffix="mer" tag="NNcm"/>
    <form  suffix="mer" tag="NNsg,indef,nom"/>
    <form  suffix="mer-" tag="NNci"/>
    <form  suffix="mer-" tag="NNcm"/>
    <form  suffix="mer-" tag="NNsms"/>
    <form  suffix="mern" tag="NNsg,def,nom"/>
    <form  suffix="merns" tag="NNsg,def,gen"/>
    <form  suffix="mers" tag="NNcm"/>
    <form  suffix="mers" tag="NNsg,indef,gen"/>
    <form  suffix="mers-" tag="NNcm"/>
    <form  suffix="mrar" tag="NNpl,indef,nom"/>
    <form  suffix="mrarna" tag="NNpl,def,nom"/>
    <form  suffix="mrarnas" tag="NNpl,def,gen"/>
    <form  suffix="mrars" tag="NNpl,indef,gen"/>
    <form  suffix="rar" tag="NNpl,indef,nom"/>
    <form  suffix="rarna" tag="NNpl,def,nom"/>
    <form  suffix="rarnas" tag="NNpl,def,gen"/>
    <form  suffix="rars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN284" rads=".*(anm|m|kycklingm|stamm)" fast="-">
    <!-- 4 members -->
    <form  suffix="oder" tag="NNci"/>
    <form  suffix="oder" tag="NNcm"/>
    <form  suffix="oder" tag="NNsg,indef,nom"/>
    <form  suffix="oder-" tag="NNci"/>
    <form  suffix="oder-" tag="NNcm"/>
    <form  suffix="oder-" tag="NNsms"/>
    <form  suffix="odern" tag="NNsg,def,nom"/>
    <form  suffix="oderns" tag="NNsg,def,gen"/>
    <form  suffix="oders" tag="NNcm"/>
    <form  suffix="oders" tag="NNsg,indef,gen"/>
    <form  suffix="oders-" tag="NNcm"/>
    <form  suffix="ödrar" tag="NNpl,indef,nom"/>
    <form  suffix="ödrarna" tag="NNpl,def,nom"/>
    <form  suffix="ödrarnas" tag="NNpl,def,gen"/>
    <form  suffix="ödrars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN285" rads=".*(etermedi|fluid|massmedi|residu)" fast="-">
    <!-- 4 members -->
    <form  suffix="um" tag="NNci"/>
    <form  suffix="a" tag="NNpl,indef,nom"/>
    <form  suffix="ana" tag="NNpl,def,nom"/>
    <form  suffix="anas" tag="NNpl,def,gen"/>
    <form  suffix="as" tag="NNpl,indef,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="um" tag="NNcm"/>
    <form  suffix="um" tag="NNsg,indef,nom"/>
    <form  suffix="um-" tag="NNci"/>
    <form  suffix="um-" tag="NNcm"/>
    <form  suffix="um-" tag="NNsms"/>
    <form  suffix="ums" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN286" rads=".*(misstroendevot|unik|skript|vot)" fast="-">
    <!-- 4 members -->
    <form  suffix="um" tag="NNci"/>
    <form  suffix="a" tag="NNpl,indef,nom"/>
    <form  suffix="as" tag="NNpl,indef,gen"/>
    <form  suffix="um" tag="NNcm"/>
    <form  suffix="um" tag="NNpl,indef,nom"/>
    <form  suffix="um" tag="NNsg,indef,nom"/>
    <form  suffix="um-" tag="NNci"/>
    <form  suffix="um-" tag="NNcm"/>
    <form  suffix="um-" tag="NNsms"/>
    <form  suffix="umen" tag="NNpl,def,nom"/>
    <form  suffix="umens" tag="NNpl,def,gen"/>
    <form  suffix="umet" tag="NNsg,def,nom"/>
    <form  suffix="umets" tag="NNsg,def,gen"/>
    <form  suffix="ums" tag="NNpl,indef,gen"/>
    <form  suffix="ums" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN287" rads=".*(ammoni|alumini|duralumini|opi)" fast="-">
    <!-- 4 members -->
    <form  suffix="um" tag="NNci"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="um" tag="NNcm"/>
    <form  suffix="um" tag="NNsg,indef,nom"/>
    <form  suffix="um-" tag="NNci"/>
    <form  suffix="um-" tag="NNcm"/>
    <form  suffix="um-" tag="NNsms"/>
    <form  suffix="umet" tag="NNsg,def,nom"/>
    <form  suffix="umets" tag="NNsg,def,gen"/>
    <form  suffix="ums" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN288" rads=".*(lad|royalt|tor|shant)" fast="-">
    <!-- 4 members -->
    <form  suffix="y" tag="NNci"/>
    <form  suffix="ies" tag="NNpl,def,gen"/>
    <form  suffix="ies" tag="NNpl,def,nom"/>
    <form  suffix="ies" tag="NNpl,indef,gen"/>
    <form  suffix="ies" tag="NNpl,indef,nom"/>
    <form  suffix="iesarna" tag="NNpl,def,nom"/>
    <form  suffix="iesarnas" tag="NNpl,def,gen"/>
    <form  suffix="y" tag="NNcm"/>
    <form  suffix="y" tag="NNsg,indef,nom"/>
    <form  suffix="y-" tag="NNci"/>
    <form  suffix="y-" tag="NNcm"/>
    <form  suffix="y-" tag="NNsms"/>
    <form  suffix="yn" tag="NNsg,def,nom"/>
    <form  suffix="yns" tag="NNsg,def,gen"/>
    <form  suffix="ys" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN289" rads=".*(reagens|ras|brons)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,gen"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,gen"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN290" rads=".*(grågyllen|kvitten|teach-in)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,def,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="a" tag="NNpl,def,nom"/>
    <form  suffix="as" tag="NNpl,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN291" rads=".*(sagoväsen|andeväsen|naturväsen)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="a" tag="NNpl,def,nom"/>
    <form  suffix="as" tag="NNpl,def,gen"/>
    <form  suffix="det" tag="NNsg,def,nom"/>
    <form  suffix="dets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN292" rads=".*(stift|ting|brott)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="en" tag="NNpl,indef,nom"/>
    <form  suffix="ena" tag="NNpl,def,nom"/>
    <form  suffix="enas" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN293" rads=".*(bar|volt|personal)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNci"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNci"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="s-" tag="NNsms"/>
  </table>
  <table name="NN294" rads=".*(borst|mejl|agnborst)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN295" rads=".*(dvd-rom|hästskosöm|cd-rom)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="men" tag="NNpl,def,nom"/>
    <form  suffix="men" tag="NNsg,def,nom"/>
    <form  suffix="mens" tag="NNpl,def,gen"/>
    <form  suffix="mens" tag="NNsg,def,gen"/>
    <form  suffix="met" tag="NNsg,def,nom"/>
    <form  suffix="mets" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN296" rads=".*(mikrofiche|etage|triage)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="n" tag="NNpl,def,nom"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="ns" tag="NNpl,def,gen"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="r" tag="NNpl,indef,nom"/>
    <form  suffix="rna" tag="NNpl,def,nom"/>
    <form  suffix="rnas" tag="NNpl,def,gen"/>
    <form  suffix="rs" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
    <form  suffix="t" tag="NNsg,def,nom"/>
    <form  suffix="ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN297" rads=".*(donjuan|bibelkanon|shogun)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,def,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN298" rads=".*(jockey|discjockey|diskjockey)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNpl,indef,nom"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="sar" tag="NNpl,indef,nom"/>
    <form  suffix="sarna" tag="NNpl,def,nom"/>
    <form  suffix="sarnas" tag="NNpl,def,gen"/>
    <form  suffix="sars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN299" rads=".*(frisbee|spaniel|cockerspaniel)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="NNci"/>
    <form  suffix="" tag="NNcm"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="n" tag="NNsg,def,nom"/>
    <form  suffix="ns" tag="NNsg,def,gen"/>
    <form  suffix="s" tag="NNcm"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNpl,indef,nom"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="s-" tag="NNcm"/>
  </table>
  <table name="NN300" rads=".*(betjänte|consortes|honoratiores)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="NNpl,indef,nom"/>
  </table>
  <table name="NN301" rads=".*(viby-i|i|o)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="NNpl,indef,nom"/>
    <form  suffix="" tag="NNsg,indef,nom"/>
    <form  suffix="-" tag="NNci"/>
    <form  suffix="-" tag="NNcm"/>
    <form  suffix="-" tag="NNsms"/>
    <form  suffix="-en" tag="NNpl,def,nom"/>
    <form  suffix="-ens" tag="NNpl,def,gen"/>
    <form  suffix="-et" tag="NNsg,def,nom"/>
    <form  suffix="-ets" tag="NNsg,def,gen"/>
    <form  suffix="-n" tag="NNpl,indef,nom"/>
    <form  suffix="-na" tag="NNpl,def,nom"/>
    <form  suffix="-nas" tag="NNpl,def,gen"/>
    <form  suffix="-ns" tag="NNpl,indef,gen"/>
    <form  suffix="-s" tag="NNpl,indef,gen"/>
    <form  suffix="-s" tag="NNsg,indef,gen"/>
    <form  suffix="-t" tag="NNsg,def,nom"/>
    <form  suffix="-ts" tag="NNsg,def,gen"/>
    <form  suffix=":en" tag="NNpl,def,nom"/>
    <form  suffix=":ens" tag="NNpl,def,gen"/>
    <form  suffix=":et" tag="NNsg,def,nom"/>
    <form  suffix=":ets" tag="NNsg,def,gen"/>
    <form  suffix=":n" tag="NNpl,indef,nom"/>
    <form  suffix=":na" tag="NNpl,def,nom"/>
    <form  suffix=":nas" tag="NNpl,def,gen"/>
    <form  suffix=":ns" tag="NNpl,indef,gen"/>
    <form  suffix=":s" tag="NNpl,indef,gen"/>
    <form  suffix=":s" tag="NNsg,indef,gen"/>
    <form  suffix=":t" tag="NNsg,def,nom"/>
    <form  suffix=":ts" tag="NNsg,def,gen"/>
    <form  suffix="en" tag="NNpl,def,nom"/>
    <form  suffix="ens" tag="NNpl,def,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="n" tag="NNpl,indef,nom"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
    <form  suffix="ns" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNpl,indef,gen"/>
    <form  suffix="s" tag="NNsg,indef,gen"/>
    <form  suffix="t" tag="NNsg,def,nom"/>
    <form  suffix="ts" tag="NNsg,def,gen"/>
  </table>
  <table name="NN302" rads=".*(prism|glasprism|diafragm)" fast="-">
    <!-- 3 members -->
    <form  suffix="a" tag="NNci"/>
    <form  suffix="a" tag="NNcm"/>
    <form  suffix="a" tag="NNsg,indef,nom"/>
    <form  suffix="a-" tag="NNci"/>
    <form  suffix="a-" tag="NNcm"/>
    <form  suffix="a-" tag="NNsms"/>
    <form  suffix="an" tag="NNsg,def,nom"/>
    <form  suffix="ans" tag="NNsg,def,gen"/>
    <form  suffix="as" tag="NNsg,indef,gen"/>
    <form  suffix="at" tag="NNsg,def,nom"/>
    <form  suffix="ats" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="or" tag="NNpl,indef,nom"/>
    <form  suffix="orna" tag="NNpl,def,nom"/>
    <form  suffix="ornas" tag="NNpl,def,gen"/>
    <form  suffix="ors" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN303" rads=".*(jordand|luftand|skönand)" fast="-">
    <!-- 3 members -->
    <form  suffix="e" tag="NNci"/>
    <form  suffix="ar" tag="NNpl,indef,nom"/>
    <form  suffix="arna" tag="NNpl,def,nom"/>
    <form  suffix="arnas" tag="NNpl,def,gen"/>
    <form  suffix="ars" tag="NNpl,indef,gen"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e" tag="NNsg,indef,nom"/>
    <form  suffix="e-" tag="NNci"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="e-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,indef,nom"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ena" tag="NNpl,def,nom"/>
    <form  suffix="enas" tag="NNpl,def,gen"/>
    <form  suffix="ens" tag="NNpl,indef,gen"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="es" tag="NNcm"/>
    <form  suffix="es" tag="NNsg,indef,gen"/>
    <form  suffix="es-" tag="NNcm"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN304" rads=".*(sidoaltar|altar|högaltar)" fast="-">
    <!-- 3 members -->
    <form  suffix="e" tag="NNci"/>
    <form  suffix="e" tag="NNcm"/>
    <form  suffix="e" tag="NNsg,indef,nom"/>
    <form  suffix="e-" tag="NNci"/>
    <form  suffix="e-" tag="NNcm"/>
    <form  suffix="e-" tag="NNsms"/>
    <form  suffix="en" tag="NNpl,indef,nom"/>
    <form  suffix="ens" tag="NNpl,indef,gen"/>
    <form  suffix="es" tag="NNsg,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="na" tag="NNpl,def,nom"/>
    <form  suffix="nas" tag="NNpl,def,gen"/>
  </table>
  <table name="NN305" rads=".*(puck|sing|spack)" fast="-">
    <!-- 3 members -->
    <form  suffix="el" tag="NNci"/>
    <form  suffix="el" tag="NNcm"/>
    <form  suffix="el" tag="NNsg,indef,nom"/>
    <form  suffix="el-" tag="NNci"/>
    <form  suffix="el-" tag="NNcm"/>
    <form  suffix="el-" tag="NNsms"/>
    <form  suffix="eln" tag="NNsg,def,nom"/>
    <form  suffix="elns" tag="NNsg,def,gen"/>
    <form  suffix="els" tag="NNcm"/>
    <form  suffix="els" tag="NNsg,indef,gen"/>
    <form  suffix="els-" tag="NNcm"/>
    <form  suffix="lar" tag="NNpl,indef,nom"/>
    <form  suffix="larna" tag="NNpl,def,nom"/>
    <form  suffix="larnas" tag="NNpl,def,gen"/>
    <form  suffix="lars" tag="NNpl,indef,gen"/>
    <form  suffix="let" tag="NNsg,def,nom"/>
    <form  suffix="lets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN306" rads=".*(folksäg|hörsäg|säg)" fast="-">
    <!-- 3 members -->
    <form  suffix="en" tag="NNci"/>
    <form  suffix="en" tag="NNcm"/>
    <form  suffix="en" tag="NNsg,indef,nom"/>
    <form  suffix="en-" tag="NNci"/>
    <form  suffix="en-" tag="NNcm"/>
    <form  suffix="en-" tag="NNsms"/>
    <form  suffix="enn" tag="NNsg,def,nom"/>
    <form  suffix="enns" tag="NNsg,def,gen"/>
    <form  suffix="ens" tag="NNcm"/>
    <form  suffix="ens" tag="NNsg,indef,gen"/>
    <form  suffix="ens-" tag="NNcm"/>
    <form  suffix="ner" tag="NNpl,indef,nom"/>
    <form  suffix="nerna" tag="NNpl,def,nom"/>
    <form  suffix="nernas" tag="NNpl,def,gen"/>
    <form  suffix="ners" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN307" rads=".*(ab|förhind|paternost)" fast="-">
    <!-- 3 members -->
    <form  suffix="er" tag="NNci"/>
    <form  suffix="er" tag="NNcm"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="er" tag="NNsg,def,nom"/>
    <form  suffix="er" tag="NNsg,indef,nom"/>
    <form  suffix="er-" tag="NNci"/>
    <form  suffix="er-" tag="NNcm"/>
    <form  suffix="er-" tag="NNsms"/>
    <form  suffix="eren" tag="NNpl,def,nom"/>
    <form  suffix="erens" tag="NNpl,def,gen"/>
    <form  suffix="eret" tag="NNsg,def,nom"/>
    <form  suffix="erets" tag="NNsg,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="ers" tag="NNsg,def,gen"/>
    <form  suffix="ers" tag="NNsg,indef,gen"/>
    <form  suffix="ren" tag="NNpl,def,nom"/>
    <form  suffix="rens" tag="NNpl,def,gen"/>
    <form  suffix="ret" tag="NNsg,def,nom"/>
    <form  suffix="rets" tag="NNsg,def,gen"/>
  </table>
  <table name="NN308" rads=".*(met|lit|mikromet)" fast="-">
    <!-- 3 members -->
    <form  suffix="er" tag="NNci"/>
    <form  suffix="er" tag="NNcm"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="er" tag="NNsg,indef,nom"/>
    <form  suffix="er-" tag="NNci"/>
    <form  suffix="er-" tag="NNcm"/>
    <form  suffix="er-" tag="NNsms"/>
    <form  suffix="ern" tag="NNsg,def,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="erns" tag="NNsg,def,gen"/>
    <form  suffix="ers" tag="NNcm"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="ers" tag="NNsg,indef,gen"/>
    <form  suffix="ers-" tag="NNcm"/>
    <form  suffix="rar" tag="NNpl,indef,nom"/>
    <form  suffix="rarna" tag="NNpl,def,nom"/>
    <form  suffix="rarnas" tag="NNpl,def,gen"/>
    <form  suffix="rars" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN309" rads=".*(stenf|getf|kof)" fast="-">
    <!-- 3 members -->
    <form  suffix="ot" tag="NNci"/>
    <form  suffix="ot" tag="NNcm"/>
    <form  suffix="ot" tag="NNsg,indef,nom"/>
    <form  suffix="ot-" tag="NNci"/>
    <form  suffix="ot-" tag="NNcm"/>
    <form  suffix="ot-" tag="NNsms"/>
    <form  suffix="otar" tag="NNpl,indef,nom"/>
    <form  suffix="otarna" tag="NNpl,def,nom"/>
    <form  suffix="otarnas" tag="NNpl,def,gen"/>
    <form  suffix="otars" tag="NNpl,indef,gen"/>
    <form  suffix="oten" tag="NNsg,def,nom"/>
    <form  suffix="otens" tag="NNsg,def,gen"/>
    <form  suffix="ots" tag="NNsg,indef,gen"/>
    <form  suffix="ötter" tag="NNpl,indef,nom"/>
    <form  suffix="ötterna" tag="NNpl,def,nom"/>
    <form  suffix="ötternas" tag="NNpl,def,gen"/>
    <form  suffix="ötters" tag="NNpl,indef,gen"/>
  </table>
  <table name="NN310" rads=".*(neri|gerani|kaprifoli)" fast="-">
    <!-- 3 members -->
    <form  suffix="um" tag="NNci"/>
    <form  suffix="en" tag="NNsg,def,nom"/>
    <form  suffix="ens" tag="NNsg,def,gen"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="um" tag="NNcm"/>
    <form  suffix="um" tag="NNsg,indef,nom"/>
    <form  suffix="um-" tag="NNci"/>
    <form  suffix="um-" tag="NNcm"/>
    <form  suffix="um-" tag="NNsms"/>
    <form  suffix="ums" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN311" rads=".*(examinatori|alluvi|seminari)" fast="-">
    <!-- 3 members -->
    <form  suffix="um" tag="NNci"/>
    <form  suffix="er" tag="NNpl,indef,nom"/>
    <form  suffix="erna" tag="NNpl,def,nom"/>
    <form  suffix="ernas" tag="NNpl,def,gen"/>
    <form  suffix="ers" tag="NNpl,indef,gen"/>
    <form  suffix="et" tag="NNsg,def,nom"/>
    <form  suffix="ets" tag="NNsg,def,gen"/>
    <form  suffix="um" tag="NNcm"/>
    <form  suffix="um" tag="NNsg,indef,nom"/>
    <form  suffix="um-" tag="NNci"/>
    <form  suffix="um-" tag="NNcm"/>
    <form  suffix="um-" tag="NNsms"/>
    <form  suffix="umet" tag="NNsg,def,nom"/>
    <form  suffix="umets" tag="NNsg,def,gen"/>
    <form  suffix="ums" tag="NNsg,indef,gen"/>
  </table>
  <table name="NN312" rads=".*(larvf|lerf|kråkf)" fast="-">
    <!-- 3 members -->
    <form  suffix="ötter" tag="NNpl,indef,nom"/>
    <form  suffix="ots" tag="NNci"/>
    <form  suffix="ots" tag="NNcm"/>
    <form  suffix="ots-" tag="NNci"/>
    <form  suffix="ots-" tag="NNcm"/>
    <form  suffix="ots-" tag="NNsms"/>
    <form  suffix="ötterna" tag="NNpl,def,nom"/>
    <form  suffix="ötternas" tag="NNpl,def,gen"/>
    <form  suffix="ötters" tag="NNpl,indef,gen"/>
  </table>
  <table name="NNA1" rads=".*">
    <!-- 127 members -->
    <form  suffix="" tag="NNAsg,indef,nom"/>
    <form  suffix="-" tag="NNAci"/>
    <form  suffix="-" tag="NNAcm"/>
    <form  suffix="-" tag="NNAsms"/>
    <form  suffix="-en" tag="NNAsg,def,nom"/>
    <form  suffix="-ens" tag="NNAsg,def,gen"/>
    <form  suffix="-et" tag="NNAsg,def,nom"/>
    <form  suffix="-ets" tag="NNAsg,def,gen"/>
    <form  suffix="-n" tag="NNAsg,def,nom"/>
    <form  suffix="-ns" tag="NNAsg,def,gen"/>
    <form  suffix="-s" tag="NNAsg,indef,gen"/>
    <form  suffix="-t" tag="NNAsg,def,nom"/>
    <form  suffix="-ts" tag="NNAsg,def,gen"/>
    <form  suffix=":en" tag="NNAsg,def,nom"/>
    <form  suffix=":ens" tag="NNAsg,def,gen"/>
    <form  suffix=":et" tag="NNAsg,def,nom"/>
    <form  suffix=":ets" tag="NNAsg,def,gen"/>
    <form  suffix=":n" tag="NNAsg,def,nom"/>
    <form  suffix=":ns" tag="NNAsg,def,gen"/>
    <form  suffix=":s" tag="NNAsg,indef,gen"/>
    <form  suffix=":t" tag="NNAsg,def,nom"/>
    <form  suffix=":ts" tag="NNAsg,def,gen"/>
    <form  suffix="en" tag="NNAsg,def,nom"/>
    <form  suffix="ens" tag="NNAsg,def,gen"/>
    <form  suffix="et" tag="NNAsg,def,nom"/>
    <form  suffix="ets" tag="NNAsg,def,gen"/>
    <form  suffix="n" tag="NNAsg,def,nom"/>
    <form  suffix="ns" tag="NNAsg,def,gen"/>
    <form  suffix="s" tag="NNAsg,indef,gen"/>
    <form  suffix="t" tag="NNAsg,def,nom"/>
    <form  suffix="ts" tag="NNAsg,def,gen"/>
  </table>
  <table name="NNA2" rads=".*">
    <!-- 51 members -->
    <form  suffix="" tag="NNAsg,indef,nom"/>
    <form  suffix="-" tag="NNAci"/>
    <form  suffix="-" tag="NNAcm"/>
    <form  suffix="-" tag="NNAsms"/>
  </table>
  <table name="NNA3" rads=".*">
    <!-- 33 members -->
    <form  suffix="" tag="NNApl,indef,nom"/>
    <form  suffix="" tag="NNAsg,indef,nom"/>
    <form  suffix="-" tag="NNAci"/>
    <form  suffix="-" tag="NNAcm"/>
    <form  suffix="-" tag="NNAsms"/>
    <form  suffix="-en" tag="NNApl,def,nom"/>
    <form  suffix="-en" tag="NNAsg,def,nom"/>
    <form  suffix="-ens" tag="NNApl,def,gen"/>
    <form  suffix="-ens" tag="NNAsg,def,gen"/>
    <form  suffix="-s" tag="NNApl,indef,gen"/>
    <form  suffix="-s" tag="NNAsg,indef,gen"/>
    <form  suffix=":en" tag="NNApl,def,nom"/>
    <form  suffix=":en" tag="NNAsg,def,nom"/>
    <form  suffix=":ens" tag="NNApl,def,gen"/>
    <form  suffix=":ens" tag="NNAsg,def,gen"/>
    <form  suffix=":s" tag="NNApl,indef,gen"/>
    <form  suffix=":s" tag="NNAsg,indef,gen"/>
    <form  suffix="en" tag="NNApl,def,nom"/>
    <form  suffix="en" tag="NNAsg,def,nom"/>
    <form  suffix="ens" tag="NNApl,def,gen"/>
    <form  suffix="ens" tag="NNAsg,def,gen"/>
    <form  suffix="s" tag="NNApl,indef,gen"/>
    <form  suffix="s" tag="NNAsg,indef,gen"/>
  </table>
  <table name="NNA4" rads=".*" fast="-">
    <!-- 26 members -->
    <form  suffix="" tag="NNAsg,indef,nom"/>
    <form  suffix="-" tag="NNAci"/>
    <form  suffix="-" tag="NNAcm"/>
    <form  suffix="-" tag="NNAsms"/>
    <form  suffix="-s" tag="NNAsg,indef,gen"/>
    <form  suffix=":s" tag="NNAsg,indef,gen"/>
    <form  suffix="s" tag="NNAsg,indef,gen"/>
  </table>
  <table name="NNA5" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="" tag="NNApl,indef,nom"/>
    <form  suffix="" tag="NNAsg,indef,nom"/>
    <form  suffix="-" tag="NNAci"/>
    <form  suffix="-" tag="NNAcm"/>
    <form  suffix="-" tag="NNAsms"/>
    <form  suffix="-n" tag="NNAsg,def,nom"/>
    <form  suffix="-na" tag="NNApl,def,nom"/>
    <form  suffix="-nas" tag="NNApl,def,gen"/>
    <form  suffix="-ns" tag="NNAsg,def,gen"/>
    <form  suffix="-s" tag="NNApl,indef,gen"/>
    <form  suffix="-s" tag="NNAsg,indef,gen"/>
    <form  suffix=":n" tag="NNAsg,def,nom"/>
    <form  suffix=":na" tag="NNApl,def,nom"/>
    <form  suffix=":nas" tag="NNApl,def,gen"/>
    <form  suffix=":ns" tag="NNAsg,def,gen"/>
    <form  suffix=":s" tag="NNApl,indef,gen"/>
    <form  suffix=":s" tag="NNAsg,indef,gen"/>
    <form  suffix="n" tag="NNAsg,def,nom"/>
    <form  suffix="na" tag="NNApl,def,nom"/>
    <form  suffix="nas" tag="NNApl,def,gen"/>
    <form  suffix="ns" tag="NNAsg,def,gen"/>
    <form  suffix="s" tag="NNApl,indef,gen"/>
    <form  suffix="s" tag="NNAsg,indef,gen"/>
  </table>
  <table name="NNA6" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="NNAsg,indef,nom"/>
    <form  suffix="-" tag="NNAci"/>
    <form  suffix="-" tag="NNAcm"/>
    <form  suffix="-" tag="NNAsms"/>
    <form  suffix=":ar" tag="NNApl,indef,nom"/>
    <form  suffix=":arna" tag="NNApl,def,nom"/>
    <form  suffix=":arnas" tag="NNApl,def,gen"/>
    <form  suffix=":ars" tag="NNApl,indef,gen"/>
    <form  suffix=":er" tag="NNApl,indef,nom"/>
    <form  suffix=":erna" tag="NNApl,def,nom"/>
    <form  suffix=":ernas" tag="NNApl,def,gen"/>
    <form  suffix=":ers" tag="NNApl,indef,gen"/>
    <form  suffix=":n" tag="NNAsg,def,nom"/>
    <form  suffix=":ns" tag="NNAsg,def,gen"/>
    <form  suffix=":s" tag="NNAsg,indef,gen"/>
    <form  suffix="ar" tag="NNApl,indef,nom"/>
    <form  suffix="arna" tag="NNApl,def,nom"/>
    <form  suffix="arnas" tag="NNApl,def,gen"/>
    <form  suffix="ars" tag="NNApl,indef,gen"/>
    <form  suffix="er" tag="NNApl,indef,nom"/>
    <form  suffix="erna" tag="NNApl,def,nom"/>
    <form  suffix="ernas" tag="NNApl,def,gen"/>
    <form  suffix="ers" tag="NNApl,indef,gen"/>
    <form  suffix="n" tag="NNAsg,def,nom"/>
    <form  suffix="ns" tag="NNAsg,def,gen"/>
    <form  suffix="s" tag="NNAsg,indef,gen"/>
  </table>
  <table name="NNA7" rads=".*(dna|pH|mitokondrie-DNA|mRNA|DNA|tRNA)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="NNApl,indef,nom"/>
    <form  suffix="" tag="NNAsg,indef,nom"/>
    <form  suffix="-" tag="NNAci"/>
    <form  suffix="-" tag="NNAcm"/>
    <form  suffix="-" tag="NNAsms"/>
    <form  suffix="-er" tag="NNApl,indef,nom"/>
    <form  suffix="-erna" tag="NNApl,def,nom"/>
    <form  suffix="-ernas" tag="NNApl,def,gen"/>
    <form  suffix="-ers" tag="NNApl,indef,gen"/>
    <form  suffix="-n" tag="NNAsg,def,nom"/>
    <form  suffix="-na" tag="NNApl,def,nom"/>
    <form  suffix="-nas" tag="NNApl,def,gen"/>
    <form  suffix="-ns" tag="NNAsg,def,gen"/>
    <form  suffix="-s" tag="NNApl,indef,gen"/>
    <form  suffix="-s" tag="NNAsg,indef,gen"/>
    <form  suffix="-t" tag="NNAsg,def,nom"/>
    <form  suffix="-ts" tag="NNAsg,def,gen"/>
    <form  suffix=":er" tag="NNApl,indef,nom"/>
    <form  suffix=":erna" tag="NNApl,def,nom"/>
    <form  suffix=":ernas" tag="NNApl,def,gen"/>
    <form  suffix=":ers" tag="NNApl,indef,gen"/>
    <form  suffix=":n" tag="NNAsg,def,nom"/>
    <form  suffix=":na" tag="NNApl,def,nom"/>
    <form  suffix=":nas" tag="NNApl,def,gen"/>
    <form  suffix=":ns" tag="NNAsg,def,gen"/>
    <form  suffix=":s" tag="NNApl,indef,gen"/>
    <form  suffix=":s" tag="NNAsg,indef,gen"/>
    <form  suffix=":t" tag="NNAsg,def,nom"/>
    <form  suffix=":ts" tag="NNAsg,def,gen"/>
    <form  suffix="er" tag="NNApl,indef,nom"/>
    <form  suffix="erna" tag="NNApl,def,nom"/>
    <form  suffix="ernas" tag="NNApl,def,gen"/>
    <form  suffix="ers" tag="NNApl,indef,gen"/>
    <form  suffix="n" tag="NNAsg,def,nom"/>
    <form  suffix="na" tag="NNApl,def,nom"/>
    <form  suffix="nas" tag="NNApl,def,gen"/>
    <form  suffix="ns" tag="NNAsg,def,gen"/>
    <form  suffix="s" tag="NNApl,indef,gen"/>
    <form  suffix="s" tag="NNAsg,indef,gen"/>
    <form  suffix="t" tag="NNAsg,def,nom"/>
    <form  suffix="ts" tag="NNAsg,def,gen"/>
  </table>
  <table name="NNA8" rads=".*(sms|VM|EEG|EKG)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="NNApl,indef,nom"/>
    <form  suffix="" tag="NNAsg,indef,nom"/>
    <form  suffix="-" tag="NNAci"/>
    <form  suffix="-" tag="NNAcm"/>
    <form  suffix="-" tag="NNAsms"/>
    <form  suffix="-n" tag="NNApl,indef,nom"/>
    <form  suffix="-na" tag="NNApl,def,nom"/>
    <form  suffix="-nas" tag="NNApl,def,gen"/>
    <form  suffix="-ns" tag="NNApl,indef,gen"/>
    <form  suffix="-s" tag="NNApl,indef,gen"/>
    <form  suffix="-s" tag="NNAsg,indef,gen"/>
    <form  suffix="-t" tag="NNAsg,def,nom"/>
    <form  suffix="-ts" tag="NNAsg,def,gen"/>
    <form  suffix=":n" tag="NNApl,indef,nom"/>
    <form  suffix=":na" tag="NNApl,def,nom"/>
    <form  suffix=":nas" tag="NNApl,def,gen"/>
    <form  suffix=":ns" tag="NNApl,indef,gen"/>
    <form  suffix=":s" tag="NNApl,indef,gen"/>
    <form  suffix=":s" tag="NNAsg,indef,gen"/>
    <form  suffix=":t" tag="NNAsg,def,nom"/>
    <form  suffix=":ts" tag="NNAsg,def,gen"/>
    <form  suffix="n" tag="NNApl,indef,nom"/>
    <form  suffix="na" tag="NNApl,def,nom"/>
    <form  suffix="nas" tag="NNApl,def,gen"/>
    <form  suffix="ns" tag="NNApl,indef,gen"/>
    <form  suffix="s" tag="NNApl,indef,gen"/>
    <form  suffix="s" tag="NNAsg,indef,gen"/>
    <form  suffix="t" tag="NNAsg,def,nom"/>
    <form  suffix="ts" tag="NNAsg,def,gen"/>
  </table>
  <table name="NNA9" rads=".*(har|kg|ha|mg)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="NNApl,indef,nom"/>
    <form  suffix="" tag="NNAsg,indef,nom"/>
    <form  suffix="-" tag="NNAci"/>
    <form  suffix="-" tag="NNAcm"/>
    <form  suffix="-" tag="NNAsms"/>
    <form  suffix="-s" tag="NNApl,indef,gen"/>
    <form  suffix="-s" tag="NNAsg,indef,gen"/>
    <form  suffix=":s" tag="NNApl,indef,gen"/>
    <form  suffix=":s" tag="NNAsg,indef,gen"/>
    <form  suffix="s" tag="NNApl,indef,gen"/>
    <form  suffix="s" tag="NNAsg,indef,gen"/>
  </table>
  <table name="NNA10" rads=".*(C|Kr|m)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="NNApl,indef,nom"/>
    <form  suffix="" tag="NNAsg,indef,nom"/>
    <form  suffix="-" tag="NNAci"/>
    <form  suffix="-" tag="NNAcm"/>
    <form  suffix="-" tag="NNAsms"/>
    <form  suffix="-en" tag="NNApl,def,nom"/>
    <form  suffix="-en" tag="NNAsg,def,nom"/>
    <form  suffix="-ens" tag="NNApl,def,gen"/>
    <form  suffix="-ens" tag="NNAsg,def,gen"/>
    <form  suffix="-et" tag="NNAsg,def,nom"/>
    <form  suffix="-ets" tag="NNAsg,def,gen"/>
    <form  suffix="-n" tag="NNAsg,def,nom"/>
    <form  suffix="-ns" tag="NNAsg,def,gen"/>
    <form  suffix="-s" tag="NNApl,indef,gen"/>
    <form  suffix="-s" tag="NNAsg,indef,gen"/>
    <form  suffix="-t" tag="NNAsg,def,nom"/>
    <form  suffix="-ts" tag="NNAsg,def,gen"/>
    <form  suffix=":en" tag="NNApl,def,nom"/>
    <form  suffix=":en" tag="NNAsg,def,nom"/>
    <form  suffix=":ens" tag="NNApl,def,gen"/>
    <form  suffix=":ens" tag="NNAsg,def,gen"/>
    <form  suffix=":et" tag="NNAsg,def,nom"/>
    <form  suffix=":ets" tag="NNAsg,def,gen"/>
    <form  suffix=":n" tag="NNAsg,def,nom"/>
    <form  suffix=":ns" tag="NNAsg,def,gen"/>
    <form  suffix=":s" tag="NNApl,indef,gen"/>
    <form  suffix=":s" tag="NNAsg,indef,gen"/>
    <form  suffix=":t" tag="NNAsg,def,nom"/>
    <form  suffix=":ts" tag="NNAsg,def,gen"/>
    <form  suffix="en" tag="NNApl,def,nom"/>
    <form  suffix="en" tag="NNAsg,def,nom"/>
    <form  suffix="ens" tag="NNApl,def,gen"/>
    <form  suffix="ens" tag="NNAsg,def,gen"/>
    <form  suffix="et" tag="NNAsg,def,nom"/>
    <form  suffix="ets" tag="NNAsg,def,gen"/>
    <form  suffix="n" tag="NNAsg,def,nom"/>
    <form  suffix="ns" tag="NNAsg,def,gen"/>
    <form  suffix="s" tag="NNApl,indef,gen"/>
    <form  suffix="s" tag="NNAsg,indef,gen"/>
    <form  suffix="t" tag="NNAsg,def,nom"/>
    <form  suffix="ts" tag="NNAsg,def,gen"/>
  </table>
  <table name="NNA11" rads=".*(DVD-ROM|CD-ROM|PM)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="NNApl,indef,nom"/>
    <form  suffix="" tag="NNAsg,indef,nom"/>
    <form  suffix="-" tag="NNAci"/>
    <form  suffix="-" tag="NNAcm"/>
    <form  suffix="-" tag="NNAsms"/>
    <form  suffix="-en" tag="NNAsg,def,nom"/>
    <form  suffix="-ens" tag="NNAsg,def,gen"/>
    <form  suffix="-et" tag="NNAsg,def,nom"/>
    <form  suffix="-ets" tag="NNAsg,def,gen"/>
    <form  suffix="-na" tag="NNApl,def,nom"/>
    <form  suffix="-nas" tag="NNApl,def,gen"/>
    <form  suffix="-s" tag="NNApl,indef,gen"/>
    <form  suffix="-s" tag="NNAsg,indef,gen"/>
    <form  suffix=":en" tag="NNAsg,def,nom"/>
    <form  suffix=":ens" tag="NNAsg,def,gen"/>
    <form  suffix=":et" tag="NNAsg,def,nom"/>
    <form  suffix=":ets" tag="NNAsg,def,gen"/>
    <form  suffix=":na" tag="NNApl,def,nom"/>
    <form  suffix=":nas" tag="NNApl,def,gen"/>
    <form  suffix=":s" tag="NNApl,indef,gen"/>
    <form  suffix=":s" tag="NNAsg,indef,gen"/>
    <form  suffix="en" tag="NNAsg,def,nom"/>
    <form  suffix="ens" tag="NNAsg,def,gen"/>
    <form  suffix="et" tag="NNAsg,def,nom"/>
    <form  suffix="ets" tag="NNAsg,def,gen"/>
    <form  suffix="na" tag="NNApl,def,nom"/>
    <form  suffix="nas" tag="NNApl,def,gen"/>
    <form  suffix="s" tag="NNApl,indef,gen"/>
    <form  suffix="s" tag="NNAsg,indef,gen"/>
  </table>
  <table name="NNH1" rads=".*(åring|siding|hörning)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="NNHsg,indef,nom"/>
    <form  suffix="ar" tag="NNHpl,indef,nom"/>
    <form  suffix="arna" tag="NNHpl,def,nom"/>
    <form  suffix="arnas" tag="NNHpl,def,gen"/>
    <form  suffix="ars" tag="NNHpl,indef,gen"/>
    <form  suffix="en" tag="NNHsg,def,nom"/>
    <form  suffix="ens" tag="NNHsg,def,gen"/>
    <form  suffix="s" tag="NNHci"/>
    <form  suffix="s" tag="NNHcm"/>
    <form  suffix="s" tag="NNHsg,indef,gen"/>
    <form  suffix="s-" tag="NNHci"/>
    <form  suffix="s-" tag="NNHcm"/>
    <form  suffix="s-" tag="NNHsms"/>
  </table>
  <table name="PM1" rads=".*">
    <!-- 2067 members -->
    <form  suffix="" tag="PMnom"/>
    <form  suffix="s" tag="PMgen"/>
  </table>
  <table name="PM2" rads=".*">
    <!-- 236 members -->
    <form  suffix="" tag="PMgen"/>
    <form  suffix="" tag="PMnom"/>
  </table>
  <table name="PM3" rads=".*">
    <!-- 71 members -->
    <form  suffix="" tag="PMci"/>
    <form  suffix="" tag="PMcm"/>
    <form  suffix="" tag="PMsg,def,nom"/>
    <form  suffix="" tag="PMsg,indef,nom"/>
    <form  suffix="-" tag="PMci"/>
    <form  suffix="-" tag="PMcm"/>
    <form  suffix="-" tag="PMsms"/>
    <form  suffix="ar" tag="PMpl,indef,nom"/>
    <form  suffix="arna" tag="PMpl,def,nom"/>
    <form  suffix="arnas" tag="PMpl,def,gen"/>
    <form  suffix="ars" tag="PMpl,indef,gen"/>
    <form  suffix="s" tag="PMsg,def,gen"/>
    <form  suffix="s" tag="PMsg,indef,gen"/>
  </table>
  <table name="PM4" rads=".*">
    <!-- 49 members -->
    <form  suffix="on" tag="PMci"/>
    <form  suffix="on" tag="PMcm"/>
    <form  suffix="on" tag="PMsg,indef,nom"/>
    <form  suffix="on-" tag="PMci"/>
    <form  suffix="on-" tag="PMcm"/>
    <form  suffix="on-" tag="PMsms"/>
    <form  suffix="onen" tag="PMsg,def,nom"/>
    <form  suffix="onens" tag="PMsg,def,gen"/>
    <form  suffix="ons" tag="PMsg,indef,gen"/>
    <form  suffix="öner" tag="PMpl,indef,nom"/>
    <form  suffix="önerna" tag="PMpl,def,nom"/>
    <form  suffix="önernas" tag="PMpl,def,gen"/>
    <form  suffix="öners" tag="PMpl,indef,gen"/>
  </table>
  <table name="PM5" rads=".*">
    <!-- 47 members -->
    <form  suffix="a" tag="PMci"/>
    <form  suffix="a" tag="PMcm"/>
    <form  suffix="a" tag="PMsg,indef,nom"/>
    <form  suffix="a-" tag="PMci"/>
    <form  suffix="a-" tag="PMcm"/>
    <form  suffix="a-" tag="PMsms"/>
    <form  suffix="an" tag="PMsg,def,nom"/>
    <form  suffix="ans" tag="PMsg,def,gen"/>
    <form  suffix="as" tag="PMsg,indef,gen"/>
    <form  suffix="or" tag="PMpl,indef,nom"/>
    <form  suffix="orna" tag="PMpl,def,nom"/>
    <form  suffix="ornas" tag="PMpl,def,gen"/>
    <form  suffix="ors" tag="PMpl,indef,gen"/>
  </table>
  <table name="PM6" rads=".*">
    <!-- 32 members -->
    <form  suffix="e" tag="PMci"/>
    <form  suffix="ar" tag="PMpl,indef,nom"/>
    <form  suffix="arna" tag="PMpl,def,nom"/>
    <form  suffix="arnas" tag="PMpl,def,gen"/>
    <form  suffix="ars" tag="PMpl,indef,gen"/>
    <form  suffix="e" tag="PMcm"/>
    <form  suffix="e" tag="PMsg,def,nom"/>
    <form  suffix="e" tag="PMsg,indef,nom"/>
    <form  suffix="e-" tag="PMci"/>
    <form  suffix="e-" tag="PMcm"/>
    <form  suffix="e-" tag="PMsms"/>
    <form  suffix="es" tag="PMsg,def,gen"/>
    <form  suffix="es" tag="PMsg,indef,gen"/>
  </table>
  <table name="PM7" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="" tag="PMgen"/>
    <form  suffix="" tag="PMnom"/>
    <form  suffix="s" tag="PMgen"/>
  </table>
  <table name="PM8" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="" tag="PMci"/>
    <form  suffix="" tag="PMcm"/>
    <form  suffix="" tag="PMsg,def,gen"/>
    <form  suffix="" tag="PMsg,def,nom"/>
    <form  suffix="" tag="PMsg,indef,gen"/>
    <form  suffix="" tag="PMsg,indef,nom"/>
    <form  suffix="-" tag="PMci"/>
    <form  suffix="-" tag="PMcm"/>
    <form  suffix="-" tag="PMsms"/>
    <form  suffix="ar" tag="PMpl,indef,nom"/>
    <form  suffix="arna" tag="PMpl,def,nom"/>
    <form  suffix="arnas" tag="PMpl,def,gen"/>
    <form  suffix="ars" tag="PMpl,indef,gen"/>
  </table>
  <table name="PM9" rads=".*(Matteus|Max|Lukas|Markus)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="PMci"/>
    <form  suffix="" tag="PMcm"/>
    <form  suffix="" tag="PMgen"/>
    <form  suffix="" tag="PMnom"/>
    <form  suffix="" tag="PMsg,def,gen"/>
    <form  suffix="" tag="PMsg,def,nom"/>
    <form  suffix="" tag="PMsg,indef,gen"/>
    <form  suffix="" tag="PMsg,indef,nom"/>
    <form  suffix="-" tag="PMci"/>
    <form  suffix="-" tag="PMcm"/>
    <form  suffix="-" tag="PMsms"/>
    <form  suffix="ar" tag="PMpl,indef,nom"/>
    <form  suffix="arna" tag="PMpl,def,nom"/>
    <form  suffix="arnas" tag="PMpl,def,gen"/>
    <form  suffix="ars" tag="PMpl,indef,gen"/>
  </table>
  <table name="PM10" rads=".*(Birgitt|Len|Sofi)" fast="-">
    <!-- 3 members -->
    <form  suffix="a" tag="PMci"/>
    <form  suffix="a" tag="PMcm"/>
    <form  suffix="a" tag="PMnom"/>
    <form  suffix="a" tag="PMsg,indef,nom"/>
    <form  suffix="a-" tag="PMci"/>
    <form  suffix="a-" tag="PMcm"/>
    <form  suffix="a-" tag="PMsms"/>
    <form  suffix="an" tag="PMsg,def,nom"/>
    <form  suffix="ans" tag="PMsg,def,gen"/>
    <form  suffix="as" tag="PMgen"/>
    <form  suffix="as" tag="PMsg,indef,gen"/>
    <form  suffix="or" tag="PMpl,indef,nom"/>
    <form  suffix="orna" tag="PMpl,def,nom"/>
    <form  suffix="ornas" tag="PMpl,def,gen"/>
    <form  suffix="ors" tag="PMpl,indef,gen"/>
  </table>
  <table name="PMA1" rads=".*">
    <!-- 83 members -->
    <form  suffix="" tag="PMAnom"/>
    <form  suffix="-s" tag="PMAgen"/>
    <form  suffix=":s" tag="PMAgen"/>
    <form  suffix="s" tag="PMAgen"/>
  </table>
  <table name="PMA2" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="" tag="PMAgen"/>
    <form  suffix="" tag="PMAnom"/>
  </table>
  <table name="PN1" rads=".*" fast="-">
    <!-- 21 members -->
    <form  suffix="" tag="PNack"/>
    <form  suffix="" tag="PNnom"/>
    <form  suffix="s" tag="PNposs,pl"/>
    <form  suffix="s" tag="PNposs,sg,n"/>
    <form  suffix="s" tag="PNposs,sg,u"/>
  </table>
  <table name="PN2" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="" tag="PNinvar"/>
  </table>
  <table name="PN3" rads=".*(övrig|själv|åtskillig|all|samtlig)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="PNsg,u,nom"/>
    <form  suffix="a" tag="PNpl,nom"/>
    <form  suffix="as" tag="PNpl,gen"/>
    <form  suffix="s" tag="PNsg,u,gen"/>
    <form  suffix="t" tag="PNsg,n,nom"/>
    <form  suffix="ts" tag="PNsg,n,gen"/>
  </table>
  <table name="PN4" rads=".*(så|ehuruda|hurda|huruda)" fast="-">
    <!-- 4 members -->
    <form  suffix="n" tag="PNsg,u,nom"/>
    <form  suffix="dan" tag="PNsg,u,nom"/>
    <form  suffix="dana" tag="PNpl,nom"/>
    <form  suffix="danas" tag="PNpl,gen"/>
    <form  suffix="dans" tag="PNsg,u,gen"/>
    <form  suffix="dant" tag="PNsg,n,nom"/>
    <form  suffix="dants" tag="PNsg,n,gen"/>
    <form  suffix="na" tag="PNpl,nom"/>
    <form  suffix="nas" tag="PNpl,gen"/>
    <form  suffix="ns" tag="PNsg,u,gen"/>
    <form  suffix="nt" tag="PNsg,n,nom"/>
    <form  suffix="nts" tag="PNsg,n,gen"/>
  </table>
  <table name="PN5" rads=".*(själv|en|samm)" fast="-">
    <!-- 3 members -->
    <form  suffix="a" tag="PNpl,nom"/>
    <form  suffix="a" tag="PNsg,n,nom"/>
    <form  suffix="a" tag="PNsg,u,nom"/>
    <form  suffix="as" tag="PNpl,gen"/>
    <form  suffix="as" tag="PNsg,n,gen"/>
    <form  suffix="as" tag="PNsg,u,gen"/>
    <form  suffix="e" tag="PNsg,u,nom"/>
    <form  suffix="es" tag="PNsg,u,gen"/>
  </table>
  <table name="PP1" rads=".*">
    <!-- 167 members -->
    <form  suffix="" tag="PPinvar"/>
  </table>
  <table name="PPA1" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="" tag="PPAinvar"/>
    <form  suffix="-" tag="PPAc"/>
    <form  suffix="-" tag="PPAsms"/>
  </table>
  <table name="SN1" rads=".*">
    <!-- 43 members -->
    <form  suffix="" tag="SNinvar"/>
  </table>
  <table name="SXC1" rads=".*">
    <!-- 134 members -->
    <form  suffix="" tag="SXCc"/>
    <form  suffix="-" tag="SXCc"/>
    <form  suffix="-" tag="SXCsms"/>
  </table>
  <table name="VB1" rads=".*">
    <!-- 2654 members -->
    <form  suffix="a" tag="VBimper"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="ad" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ade" tag="VBpret,ind,aktiv"/>
    <form  suffix="ade" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ade" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ade" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ade" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ades" tag="VBpret,ind,s-form"/>
    <form  suffix="ades" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ades" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ades" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ades" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ads" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ande" tag="VBpres_part,nom"/>
    <form  suffix="andes" tag="VBpres_part,gen"/>
    <form  suffix="ar" tag="VBpres,ind,aktiv"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="as" tag="VBpres,ind,s-form"/>
    <form  suffix="at" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="at" tag="VBsup,aktiv"/>
    <form  suffix="ats" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ats" tag="VBsup,s-form"/>
  </table>
  <table name="VB2" rads=".*">
    <!-- 2020 members -->
    <form  suffix="" tag="VBimper"/>
    <form  suffix="" tag="VBinf,aktiv"/>
    <form  suffix="d" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="de" tag="VBpret,ind,aktiv"/>
    <form  suffix="de" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="de" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="de" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="de" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="des" tag="VBpret,ind,s-form"/>
    <form  suffix="des" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="des" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="des" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="des" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="nde" tag="VBpres_part,nom"/>
    <form  suffix="ndes" tag="VBpres_part,gen"/>
    <form  suffix="r" tag="VBpres,ind,aktiv"/>
    <form  suffix="s" tag="VBinf,s-form"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="t" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="t" tag="VBsup,aktiv"/>
    <form  suffix="ts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB3" rads=".*">
    <!-- 206 members -->
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="" tag="VBimper"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="ande" tag="VBpres_part,nom"/>
    <form  suffix="andes" tag="VBpres_part,gen"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="d" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="da" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="da" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="da" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="das" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="das" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="das" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="de" tag="VBpret,ind,aktiv"/>
    <form  suffix="de" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="des" tag="VBpret,ind,s-form"/>
    <form  suffix="des" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="er" tag="VBpres,ind,aktiv"/>
    <form  suffix="es" tag="VBpres,ind,s-form"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="t" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="t" tag="VBsup,aktiv"/>
    <form  suffix="ts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB4" rads=".*">
    <!-- 197 members -->
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="" tag="VBimper"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="ande" tag="VBpres_part,nom"/>
    <form  suffix="andes" tag="VBpres_part,gen"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="er" tag="VBpres,ind,aktiv"/>
    <form  suffix="es" tag="VBpres,ind,s-form"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="t" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="t" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="t" tag="VBsup,aktiv"/>
    <form  suffix="ta" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ta" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ta" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="tas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="tas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="tas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="te" tag="VBpret,ind,aktiv"/>
    <form  suffix="te" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="tes" tag="VBpret,ind,s-form"/>
    <form  suffix="tes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ts" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB5" rads=".*">
    <!-- 134 members -->
    <form  suffix="s" tag="VBimper"/>
    <form  suffix="des" tag="VBpret,ind,s-form"/>
    <form  suffix="s" tag="VBinf,s-form"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB6" rads=".*">
    <!-- 116 members -->
    <form  suffix="la" tag="VBimper"/>
    <form  suffix="el" tag="VBc"/>
    <form  suffix="el-" tag="VBc"/>
    <form  suffix="el-" tag="VBsms"/>
    <form  suffix="la" tag="VBinf,aktiv"/>
    <form  suffix="lad" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="lade" tag="VBpret,ind,aktiv"/>
    <form  suffix="lade" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="lade" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="lade" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="lade" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="lades" tag="VBpret,ind,s-form"/>
    <form  suffix="lades" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="lades" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="lades" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="lades" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="lads" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="lande" tag="VBpres_part,nom"/>
    <form  suffix="landes" tag="VBpres_part,gen"/>
    <form  suffix="lar" tag="VBpres,ind,aktiv"/>
    <form  suffix="las" tag="VBinf,s-form"/>
    <form  suffix="las" tag="VBpres,ind,s-form"/>
    <form  suffix="lat" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="lat" tag="VBsup,aktiv"/>
    <form  suffix="lats" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="lats" tag="VBsup,s-form"/>
  </table>
  <table name="VB7" rads=".*">
    <!-- 81 members -->
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="" tag="VBimper"/>
    <form  suffix="" tag="VBpres,ind,aktiv"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="ande" tag="VBpres_part,nom"/>
    <form  suffix="andes" tag="VBpres_part,gen"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="d" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="da" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="da" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="da" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="das" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="das" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="das" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="de" tag="VBpret,ind,aktiv"/>
    <form  suffix="de" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="des" tag="VBpret,ind,s-form"/>
    <form  suffix="des" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="es" tag="VBpres,ind,s-form"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="t" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="t" tag="VBsup,aktiv"/>
    <form  suffix="ts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB8" rads=".*">
    <!-- 67 members -->
    <form  suffix="da" tag="VBinf,aktiv"/>
    <form  suffix="d" tag="VBc"/>
    <form  suffix="d" tag="VBimper"/>
    <form  suffix="d-" tag="VBc"/>
    <form  suffix="d-" tag="VBsms"/>
    <form  suffix="dande" tag="VBpres_part,nom"/>
    <form  suffix="dandes" tag="VBpres_part,gen"/>
    <form  suffix="das" tag="VBinf,s-form"/>
    <form  suffix="dd" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="dda" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="dda" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="dda" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ddas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ddas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ddas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="dde" tag="VBpret,ind,aktiv"/>
    <form  suffix="dde" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ddes" tag="VBpret,ind,s-form"/>
    <form  suffix="ddes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="dds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="der" tag="VBpres,ind,aktiv"/>
    <form  suffix="des" tag="VBpres,ind,s-form"/>
    <form  suffix="ds" tag="VBpres,ind,s-form"/>
    <form  suffix="tt" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="tt" tag="VBsup,aktiv"/>
    <form  suffix="tts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="tts" tag="VBsup,s-form"/>
  </table>
  <table name="VB9" rads=".*">
    <!-- 62 members -->
    <form  suffix="a" tag="VBimper"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="ade" tag="VBpret,ind,aktiv"/>
    <form  suffix="ades" tag="VBpret,ind,s-form"/>
    <form  suffix="ande" tag="VBpres_part,nom"/>
    <form  suffix="andes" tag="VBpres_part,gen"/>
    <form  suffix="ar" tag="VBpres,ind,aktiv"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="as" tag="VBpres,ind,s-form"/>
    <form  suffix="at" tag="VBsup,aktiv"/>
    <form  suffix="ats" tag="VBsup,s-form"/>
  </table>
  <table name="VB10" rads=".*">
    <!-- 62 members -->
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="" tag="VBimper"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="ande" tag="VBpres_part,nom"/>
    <form  suffix="andes" tag="VBpres_part,gen"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="er" tag="VBpres,ind,aktiv"/>
    <form  suffix="es" tag="VBpres,ind,s-form"/>
    <form  suffix="t" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="t" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="t" tag="VBsup,aktiv"/>
    <form  suffix="ta" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ta" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ta" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="tas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="tas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="tas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="te" tag="VBpret,ind,aktiv"/>
    <form  suffix="te" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="tes" tag="VBpret,ind,s-form"/>
    <form  suffix="tes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ts" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB11" rads=".*">
    <!-- 55 members -->
    <form  suffix="" tag="VBc"/>
    <form  suffix="" tag="VBimper"/>
    <form  suffix="" tag="VBinf,aktiv"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="dd" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="dda" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="dda" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="dda" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ddas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ddas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ddas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="dde" tag="VBpret,ind,aktiv"/>
    <form  suffix="dde" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ddes" tag="VBpret,ind,s-form"/>
    <form  suffix="ddes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="dds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ende" tag="VBpres_part,nom"/>
    <form  suffix="endes" tag="VBpres_part,gen"/>
    <form  suffix="r" tag="VBpres,ind,aktiv"/>
    <form  suffix="s" tag="VBinf,s-form"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="tt" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="tt" tag="VBsup,aktiv"/>
    <form  suffix="tts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="tts" tag="VBsup,s-form"/>
  </table>
  <table name="VB12" rads=".*">
    <!-- 53 members -->
    <form  suffix="ma" tag="VBimper"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="ma" tag="VBinf,aktiv"/>
    <form  suffix="mad" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="made" tag="VBpret,ind,aktiv"/>
    <form  suffix="made" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="made" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="made" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="made" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="mades" tag="VBpret,ind,s-form"/>
    <form  suffix="mades" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="mades" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="mades" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="mades" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="mads" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="mande" tag="VBpres_part,nom"/>
    <form  suffix="mandes" tag="VBpres_part,gen"/>
    <form  suffix="mar" tag="VBpres,ind,aktiv"/>
    <form  suffix="mas" tag="VBinf,s-form"/>
    <form  suffix="mas" tag="VBpres,ind,s-form"/>
    <form  suffix="mat" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="mat" tag="VBsup,aktiv"/>
    <form  suffix="mats" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="mats" tag="VBsup,s-form"/>
  </table>
  <table name="VB13" rads=".*">
    <!-- 51 members -->
    <form  suffix="ägga" tag="VBinf,aktiv"/>
    <form  suffix="a" tag="VBpret,ind,aktiv"/>
    <form  suffix="ade" tag="VBpret,ind,aktiv"/>
    <form  suffix="ades" tag="VBpret,ind,s-form"/>
    <form  suffix="agd" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="agda" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="agda" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="agda" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="agdas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="agdas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="agdas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="agde" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="agdes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="agds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="agt" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="agt" tag="VBsup,aktiv"/>
    <form  suffix="agts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="agts" tag="VBsup,s-form"/>
    <form  suffix="as" tag="VBpret,ind,s-form"/>
    <form  suffix="ägg" tag="VBc"/>
    <form  suffix="ägg" tag="VBimper"/>
    <form  suffix="ägg-" tag="VBc"/>
    <form  suffix="ägg-" tag="VBsms"/>
    <form  suffix="äggande" tag="VBpres_part,nom"/>
    <form  suffix="äggandes" tag="VBpres_part,gen"/>
    <form  suffix="äggas" tag="VBinf,s-form"/>
    <form  suffix="ägger" tag="VBpres,ind,aktiv"/>
    <form  suffix="ägges" tag="VBpres,ind,s-form"/>
    <form  suffix="äggs" tag="VBpres,ind,s-form"/>
  </table>
  <table name="VB14" rads=".*">
    <!-- 50 members -->
    <form  suffix="ätta" tag="VBinf,aktiv"/>
    <form  suffix="att" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="att" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="att" tag="VBsup,aktiv"/>
    <form  suffix="atta" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="atta" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="atta" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="attas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="attas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="attas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="atte" tag="VBpret,ind,aktiv"/>
    <form  suffix="atte" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="attes" tag="VBpret,ind,s-form"/>
    <form  suffix="attes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="atts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="atts" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="atts" tag="VBsup,s-form"/>
    <form  suffix="ätt" tag="VBc"/>
    <form  suffix="ätt" tag="VBimper"/>
    <form  suffix="ätt-" tag="VBc"/>
    <form  suffix="ätt-" tag="VBsms"/>
    <form  suffix="ättande" tag="VBpres_part,nom"/>
    <form  suffix="ättandes" tag="VBpres_part,gen"/>
    <form  suffix="ättas" tag="VBinf,s-form"/>
    <form  suffix="ätter" tag="VBpres,ind,aktiv"/>
    <form  suffix="ättes" tag="VBpres,ind,s-form"/>
    <form  suffix="ätts" tag="VBpres,ind,s-form"/>
  </table>
  <table name="VB15" rads=".*">
    <!-- 47 members -->
    <form  suffix="a" tag="VBimper"/>
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="ag" tag="VBc"/>
    <form  suffix="ag" tag="VBimper"/>
    <form  suffix="ag-" tag="VBc"/>
    <form  suffix="ag-" tag="VBsms"/>
    <form  suffix="aga" tag="VBinf,aktiv"/>
    <form  suffix="agande" tag="VBpres_part,nom"/>
    <form  suffix="agandes" tag="VBpres_part,gen"/>
    <form  suffix="agas" tag="VBinf,s-form"/>
    <form  suffix="age" tag="VBpres,konj,aktiv"/>
    <form  suffix="agen" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="agens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ager" tag="VBpres,ind,aktiv"/>
    <form  suffix="ages" tag="VBpres,ind,s-form"/>
    <form  suffix="ages" tag="VBpres,konj,s-form"/>
    <form  suffix="aget" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="agets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="agit" tag="VBsup,aktiv"/>
    <form  suffix="agits" tag="VBsup,s-form"/>
    <form  suffix="agna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="agna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="agna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="agnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="agnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="agnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="agne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="agnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ags" tag="VBpres,ind,s-form"/>
    <form  suffix="ar" tag="VBpres,ind,aktiv"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="as" tag="VBpres,ind,s-form"/>
    <form  suffix="og" tag="VBpret,ind,aktiv"/>
    <form  suffix="oge" tag="VBpret,konj,aktiv"/>
    <form  suffix="oges" tag="VBpret,konj,s-form"/>
    <form  suffix="ogs" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB16" rads=".*">
    <!-- 47 members -->
    <form  suffix="la" tag="VBimper"/>
    <form  suffix="el" tag="VBc"/>
    <form  suffix="el-" tag="VBc"/>
    <form  suffix="el-" tag="VBsms"/>
    <form  suffix="la" tag="VBinf,aktiv"/>
    <form  suffix="lade" tag="VBpret,ind,aktiv"/>
    <form  suffix="lades" tag="VBpret,ind,s-form"/>
    <form  suffix="lande" tag="VBpres_part,nom"/>
    <form  suffix="landes" tag="VBpres_part,gen"/>
    <form  suffix="lar" tag="VBpres,ind,aktiv"/>
    <form  suffix="las" tag="VBinf,s-form"/>
    <form  suffix="las" tag="VBpres,ind,s-form"/>
    <form  suffix="lat" tag="VBsup,aktiv"/>
    <form  suffix="lats" tag="VBsup,s-form"/>
  </table>
  <table name="VB17" rads=".*">
    <!-- 46 members -->
    <form  suffix="ra" tag="VBimper"/>
    <form  suffix="er" tag="VBc"/>
    <form  suffix="er-" tag="VBc"/>
    <form  suffix="er-" tag="VBsms"/>
    <form  suffix="ra" tag="VBinf,aktiv"/>
    <form  suffix="rad" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="rade" tag="VBpret,ind,aktiv"/>
    <form  suffix="rade" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="rade" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="rade" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="rade" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="rades" tag="VBpret,ind,s-form"/>
    <form  suffix="rades" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="rades" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="rades" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="rades" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="rads" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="rande" tag="VBpres_part,nom"/>
    <form  suffix="randes" tag="VBpres_part,gen"/>
    <form  suffix="rar" tag="VBpres,ind,aktiv"/>
    <form  suffix="ras" tag="VBinf,s-form"/>
    <form  suffix="ras" tag="VBpres,ind,s-form"/>
    <form  suffix="rat" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="rat" tag="VBsup,aktiv"/>
    <form  suffix="rats" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="rats" tag="VBsup,s-form"/>
  </table>
  <table name="VB18" rads=".*">
    <!-- 38 members -->
    <form  suffix="iva" tag="VBinf,aktiv"/>
    <form  suffix="ev" tag="VBpret,ind,aktiv"/>
    <form  suffix="evs" tag="VBpret,ind,s-form"/>
    <form  suffix="iv" tag="VBc"/>
    <form  suffix="iv" tag="VBimper"/>
    <form  suffix="iv-" tag="VBc"/>
    <form  suffix="iv-" tag="VBsms"/>
    <form  suffix="ivande" tag="VBpres_part,nom"/>
    <form  suffix="ivandes" tag="VBpres_part,gen"/>
    <form  suffix="ivas" tag="VBinf,s-form"/>
    <form  suffix="ive" tag="VBpres,konj,aktiv"/>
    <form  suffix="ive" tag="VBpret,konj,aktiv"/>
    <form  suffix="iven" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ivens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="iver" tag="VBpres,ind,aktiv"/>
    <form  suffix="ives" tag="VBpres,ind,s-form"/>
    <form  suffix="ives" tag="VBpres,konj,s-form"/>
    <form  suffix="ives" tag="VBpret,konj,s-form"/>
    <form  suffix="ivet" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="ivets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ivit" tag="VBsup,aktiv"/>
    <form  suffix="ivits" tag="VBsup,s-form"/>
    <form  suffix="ivna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ivna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ivna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ivnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ivnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ivnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ivne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ivnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ivs" tag="VBpres,ind,s-form"/>
  </table>
  <table name="VB19" rads=".*">
    <!-- 37 members -->
    <form  suffix="öra" tag="VBinf,aktiv"/>
    <form  suffix="jord" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="jorda" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="jorda" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="jorda" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="jordas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="jordas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="jordas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="jorde" tag="VBpret,ind,aktiv"/>
    <form  suffix="jorde" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="jordes" tag="VBpret,ind,s-form"/>
    <form  suffix="jordes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="jords" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="jort" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="jort" tag="VBsup,aktiv"/>
    <form  suffix="jorts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="jorts" tag="VBsup,s-form"/>
    <form  suffix="ör" tag="VBc"/>
    <form  suffix="ör" tag="VBimper"/>
    <form  suffix="ör" tag="VBpres,ind,aktiv"/>
    <form  suffix="ör-" tag="VBc"/>
    <form  suffix="ör-" tag="VBsms"/>
    <form  suffix="örande" tag="VBpres_part,nom"/>
    <form  suffix="örandes" tag="VBpres_part,gen"/>
    <form  suffix="öras" tag="VBinf,s-form"/>
    <form  suffix="öres" tag="VBpres,ind,s-form"/>
    <form  suffix="örs" tag="VBpres,ind,s-form"/>
  </table>
  <table name="VB20" rads=".*">
    <!-- 36 members -->
    <form  suffix="ra" tag="VBimper"/>
    <form  suffix="er" tag="VBc"/>
    <form  suffix="er-" tag="VBc"/>
    <form  suffix="er-" tag="VBsms"/>
    <form  suffix="ra" tag="VBinf,aktiv"/>
    <form  suffix="rade" tag="VBpret,ind,aktiv"/>
    <form  suffix="rades" tag="VBpret,ind,s-form"/>
    <form  suffix="rande" tag="VBpres_part,nom"/>
    <form  suffix="randes" tag="VBpres_part,gen"/>
    <form  suffix="rar" tag="VBpres,ind,aktiv"/>
    <form  suffix="ras" tag="VBinf,s-form"/>
    <form  suffix="ras" tag="VBpres,ind,s-form"/>
    <form  suffix="rat" tag="VBsup,aktiv"/>
    <form  suffix="rats" tag="VBsup,s-form"/>
  </table>
  <table name="VB21" rads=".*">
    <!-- 34 members -->
    <form  suffix="ma" tag="VBinf,aktiv"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="" tag="VBimper"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="d" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="da" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="da" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="da" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="das" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="das" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="das" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="de" tag="VBpret,ind,aktiv"/>
    <form  suffix="de" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="des" tag="VBpret,ind,s-form"/>
    <form  suffix="des" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="mande" tag="VBpres_part,nom"/>
    <form  suffix="mandes" tag="VBpres_part,gen"/>
    <form  suffix="mas" tag="VBinf,s-form"/>
    <form  suffix="mer" tag="VBpres,ind,aktiv"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="t" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="t" tag="VBsup,aktiv"/>
    <form  suffix="ts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB22" rads=".*">
    <!-- 32 members -->
    <form  suffix="da" tag="VBinf,aktiv"/>
    <form  suffix="d" tag="VBc"/>
    <form  suffix="d" tag="VBimper"/>
    <form  suffix="d" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="d-" tag="VBc"/>
    <form  suffix="d-" tag="VBsms"/>
    <form  suffix="da" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="da" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="da" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="dande" tag="VBpres_part,nom"/>
    <form  suffix="dandes" tag="VBpres_part,gen"/>
    <form  suffix="das" tag="VBinf,s-form"/>
    <form  suffix="das" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="das" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="das" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="de" tag="VBpret,ind,aktiv"/>
    <form  suffix="de" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="der" tag="VBpres,ind,aktiv"/>
    <form  suffix="des" tag="VBpres,ind,s-form"/>
    <form  suffix="des" tag="VBpret,ind,s-form"/>
    <form  suffix="des" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ds" tag="VBpres,ind,s-form"/>
    <form  suffix="ds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="t" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="t" tag="VBsup,aktiv"/>
    <form  suffix="ts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB23" rads=".*">
    <!-- 31 members -->
    <form  suffix="na" tag="VBimper"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="na" tag="VBinf,aktiv"/>
    <form  suffix="nad" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="nade" tag="VBpret,ind,aktiv"/>
    <form  suffix="nade" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="nade" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="nade" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="nade" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="nades" tag="VBpret,ind,s-form"/>
    <form  suffix="nades" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="nades" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="nades" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="nades" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="nads" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="nande" tag="VBpres_part,nom"/>
    <form  suffix="nandes" tag="VBpres_part,gen"/>
    <form  suffix="nar" tag="VBpres,ind,aktiv"/>
    <form  suffix="nas" tag="VBinf,s-form"/>
    <form  suffix="nas" tag="VBpres,ind,s-form"/>
    <form  suffix="nat" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="nat" tag="VBsup,aktiv"/>
    <form  suffix="nats" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="nats" tag="VBsup,s-form"/>
  </table>
  <table name="VB24" rads=".*" fast="-">
    <!-- 26 members -->
    <form  suffix="ålla" tag="VBinf,aktiv"/>
    <form  suffix="åll" tag="VBc"/>
    <form  suffix="åll" tag="VBimper"/>
    <form  suffix="åll-" tag="VBc"/>
    <form  suffix="åll-" tag="VBsms"/>
    <form  suffix="ållande" tag="VBpres_part,nom"/>
    <form  suffix="ållandes" tag="VBpres_part,gen"/>
    <form  suffix="ållas" tag="VBinf,s-form"/>
    <form  suffix="ålle" tag="VBpres,konj,aktiv"/>
    <form  suffix="ållen" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ållens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="åller" tag="VBpres,ind,aktiv"/>
    <form  suffix="ålles" tag="VBpres,ind,s-form"/>
    <form  suffix="ålles" tag="VBpres,konj,s-form"/>
    <form  suffix="ållet" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="ållets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ållit" tag="VBsup,aktiv"/>
    <form  suffix="ållits" tag="VBsup,s-form"/>
    <form  suffix="ållna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ållna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ållna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ållnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ållnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ållnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ållne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ållnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ålls" tag="VBpres,ind,s-form"/>
    <form  suffix="öll" tag="VBpret,ind,aktiv"/>
    <form  suffix="ölle" tag="VBpret,konj,aktiv"/>
    <form  suffix="ölles" tag="VBpret,konj,s-form"/>
    <form  suffix="ölls" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB25" rads=".*" fast="-">
    <!-- 21 members -->
    <form  suffix="e" tag="VBc"/>
    <form  suffix="av" tag="VBpret,ind,aktiv"/>
    <form  suffix="avs" tag="VBpret,ind,s-form"/>
    <form  suffix="e" tag="VBimper"/>
    <form  suffix="e" tag="VBinf,aktiv"/>
    <form  suffix="e-" tag="VBc"/>
    <form  suffix="e-" tag="VBsms"/>
    <form  suffix="eende" tag="VBpres_part,nom"/>
    <form  suffix="eendes" tag="VBpres_part,gen"/>
    <form  suffix="er" tag="VBpres,ind,aktiv"/>
    <form  suffix="es" tag="VBinf,s-form"/>
    <form  suffix="es" tag="VBpres,ind,s-form"/>
    <form  suffix="ett" tag="VBsup,aktiv"/>
    <form  suffix="etts" tag="VBsup,s-form"/>
    <form  suffix="iv" tag="VBc"/>
    <form  suffix="iv" tag="VBimper"/>
    <form  suffix="iv-" tag="VBc"/>
    <form  suffix="iv-" tag="VBsms"/>
    <form  suffix="iva" tag="VBinf,aktiv"/>
    <form  suffix="ivande" tag="VBpres_part,nom"/>
    <form  suffix="ivandes" tag="VBpres_part,gen"/>
    <form  suffix="ivas" tag="VBinf,s-form"/>
    <form  suffix="ive" tag="VBpres,konj,aktiv"/>
    <form  suffix="iven" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ivens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="iver" tag="VBpres,ind,aktiv"/>
    <form  suffix="ives" tag="VBpres,ind,s-form"/>
    <form  suffix="ives" tag="VBpres,konj,s-form"/>
    <form  suffix="ivet" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="ivets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ivit" tag="VBsup,aktiv"/>
    <form  suffix="ivits" tag="VBsup,s-form"/>
    <form  suffix="ivna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ivna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ivna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ivnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ivnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ivnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ivne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ivnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ivs" tag="VBpres,ind,s-form"/>
    <form  suffix="åve" tag="VBpret,konj,aktiv"/>
    <form  suffix="åves" tag="VBpret,konj,s-form"/>
  </table>
  <table name="VB26" rads=".*" fast="-">
    <!-- 21 members -->
    <form  suffix="na" tag="VBinf,aktiv"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="d" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="da" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="da" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="da" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="das" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="das" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="das" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="de" tag="VBpret,ind,aktiv"/>
    <form  suffix="de" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="des" tag="VBpret,ind,s-form"/>
    <form  suffix="des" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="n" tag="VBimper"/>
    <form  suffix="nande" tag="VBpres_part,nom"/>
    <form  suffix="nandes" tag="VBpres_part,gen"/>
    <form  suffix="nas" tag="VBinf,s-form"/>
    <form  suffix="ner" tag="VBpres,ind,aktiv"/>
    <form  suffix="nes" tag="VBpres,ind,s-form"/>
    <form  suffix="ns" tag="VBpres,ind,s-form"/>
    <form  suffix="t" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="t" tag="VBsup,aktiv"/>
    <form  suffix="ts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB27" rads=".*" fast="-">
    <!-- 20 members -->
    <form  suffix="inna" tag="VBinf,aktiv"/>
    <form  suffix="ann" tag="VBpret,ind,aktiv"/>
    <form  suffix="anns" tag="VBpret,ind,s-form"/>
    <form  suffix="in" tag="VBc"/>
    <form  suffix="in-" tag="VBc"/>
    <form  suffix="in-" tag="VBsms"/>
    <form  suffix="inn" tag="VBimper"/>
    <form  suffix="innande" tag="VBpres_part,nom"/>
    <form  suffix="innandes" tag="VBpres_part,gen"/>
    <form  suffix="innas" tag="VBinf,s-form"/>
    <form  suffix="inne" tag="VBpres,konj,aktiv"/>
    <form  suffix="inner" tag="VBpres,ind,aktiv"/>
    <form  suffix="innes" tag="VBpres,ind,s-form"/>
    <form  suffix="innes" tag="VBpres,konj,s-form"/>
    <form  suffix="inns" tag="VBpres,ind,s-form"/>
    <form  suffix="unna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="unna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="unna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="unnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="unnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="unnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="unne" tag="VBpret,konj,aktiv"/>
    <form  suffix="unne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="unnen" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="unnens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="unnes" tag="VBpret,konj,s-form"/>
    <form  suffix="unnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="unnet" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="unnets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="unnit" tag="VBsup,aktiv"/>
    <form  suffix="unnits" tag="VBsup,s-form"/>
  </table>
  <table name="VB28" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="e" tag="VBc"/>
    <form  suffix="e" tag="VBimper"/>
    <form  suffix="e" tag="VBinf,aktiv"/>
    <form  suffix="e-" tag="VBc"/>
    <form  suffix="e-" tag="VBsms"/>
    <form  suffix="edd" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="edda" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="edda" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="edda" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="eddas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="eddas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="eddas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="edde" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="eddes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="edds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="eende" tag="VBpres_part,nom"/>
    <form  suffix="eendes" tag="VBpres_part,gen"/>
    <form  suffix="er" tag="VBpres,ind,aktiv"/>
    <form  suffix="es" tag="VBinf,s-form"/>
    <form  suffix="es" tag="VBpres,ind,s-form"/>
    <form  suffix="ett" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="ett" tag="VBsup,aktiv"/>
    <form  suffix="etts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="etts" tag="VBsup,s-form"/>
    <form  suffix="åg" tag="VBpret,ind,aktiv"/>
    <form  suffix="åge" tag="VBpret,konj,aktiv"/>
    <form  suffix="åges" tag="VBpret,konj,s-form"/>
    <form  suffix="ågs" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB29" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="juta" tag="VBinf,aktiv"/>
    <form  suffix="jut" tag="VBc"/>
    <form  suffix="jut" tag="VBimper"/>
    <form  suffix="jut-" tag="VBc"/>
    <form  suffix="jut-" tag="VBsms"/>
    <form  suffix="jutande" tag="VBpres_part,nom"/>
    <form  suffix="jutandes" tag="VBpres_part,gen"/>
    <form  suffix="jutas" tag="VBinf,s-form"/>
    <form  suffix="jute" tag="VBpres,konj,aktiv"/>
    <form  suffix="juten" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="jutens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="juter" tag="VBpres,ind,aktiv"/>
    <form  suffix="jutes" tag="VBpres,ind,s-form"/>
    <form  suffix="jutes" tag="VBpres,konj,s-form"/>
    <form  suffix="jutet" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="jutets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="jutit" tag="VBsup,aktiv"/>
    <form  suffix="jutits" tag="VBsup,s-form"/>
    <form  suffix="jutna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="jutna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="jutna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="jutnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="jutnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="jutnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="jutne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="jutnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="juts" tag="VBpres,ind,s-form"/>
    <form  suffix="öt" tag="VBpret,ind,aktiv"/>
    <form  suffix="öte" tag="VBpret,konj,aktiv"/>
    <form  suffix="ötes" tag="VBpret,konj,s-form"/>
    <form  suffix="öts" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB30" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="ma" tag="VBinf,aktiv"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="" tag="VBimper"/>
    <form  suffix="" tag="VBpret,ind,aktiv"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="mande" tag="VBpres_part,nom"/>
    <form  suffix="mandes" tag="VBpres_part,gen"/>
    <form  suffix="mas" tag="VBinf,s-form"/>
    <form  suffix="me" tag="VBpres,konj,aktiv"/>
    <form  suffix="me" tag="VBpret,konj,aktiv"/>
    <form  suffix="men" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="mens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="mer" tag="VBpres,ind,aktiv"/>
    <form  suffix="mes" tag="VBpres,ind,s-form"/>
    <form  suffix="mes" tag="VBpres,konj,s-form"/>
    <form  suffix="mes" tag="VBpret,konj,s-form"/>
    <form  suffix="met" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="mets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="mit" tag="VBsup,aktiv"/>
    <form  suffix="mits" tag="VBsup,s-form"/>
    <form  suffix="na" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="na" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="na" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="nas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="nas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="nas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="nes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="s" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB31" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="å" tag="VBc"/>
    <form  suffix="agen" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="agens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="aget" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="agets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="agit" tag="VBsup,aktiv"/>
    <form  suffix="agits" tag="VBsup,s-form"/>
    <form  suffix="agna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="agna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="agna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="agnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="agnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="agnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="agne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="agnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="og" tag="VBpret,ind,aktiv"/>
    <form  suffix="oge" tag="VBpret,konj,aktiv"/>
    <form  suffix="oges" tag="VBpret,konj,s-form"/>
    <form  suffix="ogs" tag="VBpret,ind,s-form"/>
    <form  suffix="å" tag="VBimper"/>
    <form  suffix="å" tag="VBinf,aktiv"/>
    <form  suffix="å-" tag="VBc"/>
    <form  suffix="å-" tag="VBsms"/>
    <form  suffix="ående" tag="VBpres_part,nom"/>
    <form  suffix="åendes" tag="VBpres_part,gen"/>
    <form  suffix="år" tag="VBpres,ind,aktiv"/>
    <form  suffix="ås" tag="VBinf,s-form"/>
    <form  suffix="ås" tag="VBpres,ind,s-form"/>
  </table>
  <table name="VB32" rads=".*" fast="-">
    <!-- 18 members -->
    <form  suffix="" tag="VBimper"/>
    <form  suffix="" tag="VBinf,aktiv"/>
    <form  suffix="de" tag="VBpret,ind,aktiv"/>
    <form  suffix="des" tag="VBpret,ind,s-form"/>
    <form  suffix="nde" tag="VBpres_part,nom"/>
    <form  suffix="ndes" tag="VBpres_part,gen"/>
    <form  suffix="r" tag="VBpres,ind,aktiv"/>
    <form  suffix="s" tag="VBinf,s-form"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="t" tag="VBsup,aktiv"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB33" rads=".*" fast="-">
    <!-- 18 members -->
    <form  suffix="å" tag="VBc"/>
    <form  suffix="ick" tag="VBpret,ind,aktiv"/>
    <form  suffix="icks" tag="VBpret,ind,s-form"/>
    <form  suffix="inge" tag="VBpret,konj,aktiv"/>
    <form  suffix="inges" tag="VBpret,konj,s-form"/>
    <form  suffix="å" tag="VBimper"/>
    <form  suffix="å" tag="VBinf,aktiv"/>
    <form  suffix="å-" tag="VBc"/>
    <form  suffix="å-" tag="VBsms"/>
    <form  suffix="ående" tag="VBpres_part,nom"/>
    <form  suffix="åendes" tag="VBpres_part,gen"/>
    <form  suffix="ånge" tag="VBpres,konj,aktiv"/>
    <form  suffix="ången" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ångens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ånges" tag="VBpres,konj,s-form"/>
    <form  suffix="ånget" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="ångets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ångna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ångna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ångna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ångnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ångnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ångnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ångne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ångnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="år" tag="VBpres,ind,aktiv"/>
    <form  suffix="ås" tag="VBinf,s-form"/>
    <form  suffix="ås" tag="VBpres,ind,s-form"/>
    <form  suffix="ått" tag="VBsup,aktiv"/>
    <form  suffix="åtts" tag="VBsup,s-form"/>
  </table>
  <table name="VB34" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="ida" tag="VBinf,aktiv"/>
    <form  suffix="ed" tag="VBpret,ind,aktiv"/>
    <form  suffix="eds" tag="VBpret,ind,s-form"/>
    <form  suffix="id" tag="VBc"/>
    <form  suffix="id" tag="VBimper"/>
    <form  suffix="id-" tag="VBc"/>
    <form  suffix="id-" tag="VBsms"/>
    <form  suffix="idande" tag="VBpres_part,nom"/>
    <form  suffix="idandes" tag="VBpres_part,gen"/>
    <form  suffix="idas" tag="VBinf,s-form"/>
    <form  suffix="ide" tag="VBpres,konj,aktiv"/>
    <form  suffix="ide" tag="VBpret,konj,aktiv"/>
    <form  suffix="iden" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="idens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ider" tag="VBpres,ind,aktiv"/>
    <form  suffix="ides" tag="VBpres,ind,s-form"/>
    <form  suffix="ides" tag="VBpres,konj,s-form"/>
    <form  suffix="ides" tag="VBpret,konj,s-form"/>
    <form  suffix="idet" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="idets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="idit" tag="VBsup,aktiv"/>
    <form  suffix="idits" tag="VBsup,s-form"/>
    <form  suffix="idna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="idna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="idna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="idnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="idnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="idnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="idne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="idnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ids" tag="VBpres,ind,s-form"/>
  </table>
  <table name="VB35" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="yta" tag="VBinf,aktiv"/>
    <form  suffix="uten" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="utens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="utet" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="utets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="utit" tag="VBsup,aktiv"/>
    <form  suffix="utits" tag="VBsup,s-form"/>
    <form  suffix="utna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="utna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="utna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="utnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="utnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="utnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="utne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="utnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="yt" tag="VBc"/>
    <form  suffix="yt" tag="VBimper"/>
    <form  suffix="yt-" tag="VBc"/>
    <form  suffix="yt-" tag="VBsms"/>
    <form  suffix="ytande" tag="VBpres_part,nom"/>
    <form  suffix="ytandes" tag="VBpres_part,gen"/>
    <form  suffix="ytas" tag="VBinf,s-form"/>
    <form  suffix="yte" tag="VBpres,konj,aktiv"/>
    <form  suffix="yter" tag="VBpres,ind,aktiv"/>
    <form  suffix="ytes" tag="VBpres,ind,s-form"/>
    <form  suffix="ytes" tag="VBpres,konj,s-form"/>
    <form  suffix="yts" tag="VBpres,ind,s-form"/>
    <form  suffix="öt" tag="VBpret,ind,aktiv"/>
    <form  suffix="öte" tag="VBpret,konj,aktiv"/>
    <form  suffix="ötes" tag="VBpret,konj,s-form"/>
    <form  suffix="öts" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB36" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="alla" tag="VBinf,aktiv"/>
    <form  suffix="all" tag="VBc"/>
    <form  suffix="all" tag="VBimper"/>
    <form  suffix="all-" tag="VBc"/>
    <form  suffix="all-" tag="VBsms"/>
    <form  suffix="allande" tag="VBpres_part,nom"/>
    <form  suffix="allandes" tag="VBpres_part,gen"/>
    <form  suffix="allas" tag="VBinf,s-form"/>
    <form  suffix="alle" tag="VBpres,konj,aktiv"/>
    <form  suffix="allen" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="allens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="aller" tag="VBpres,ind,aktiv"/>
    <form  suffix="alles" tag="VBpres,ind,s-form"/>
    <form  suffix="alles" tag="VBpres,konj,s-form"/>
    <form  suffix="allet" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="allets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="allit" tag="VBsup,aktiv"/>
    <form  suffix="allits" tag="VBsup,s-form"/>
    <form  suffix="allna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="allna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="allna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="allnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="allnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="allnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="allne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="allnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="alls" tag="VBpres,ind,s-form"/>
    <form  suffix="öll" tag="VBpret,ind,aktiv"/>
    <form  suffix="ölle" tag="VBpret,konj,aktiv"/>
    <form  suffix="ölles" tag="VBpret,konj,s-form"/>
    <form  suffix="ölls" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB37" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="" tag="VBimper"/>
    <form  suffix="" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="" tag="VBsup,aktiv"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="a" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="a" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="a" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ande" tag="VBpres_part,nom"/>
    <form  suffix="andes" tag="VBpres_part,gen"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="as" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="as" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="as" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="e" tag="VBpret,ind,aktiv"/>
    <form  suffix="e" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="er" tag="VBpres,ind,aktiv"/>
    <form  suffix="es" tag="VBpres,ind,s-form"/>
    <form  suffix="es" tag="VBpret,ind,s-form"/>
    <form  suffix="es" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="s" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="s" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="s" tag="VBsup,s-form"/>
  </table>
  <table name="VB38" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="å" tag="VBc"/>
    <form  suffix="od" tag="VBpret,ind,aktiv"/>
    <form  suffix="ode" tag="VBpret,konj,aktiv"/>
    <form  suffix="odes" tag="VBpret,konj,s-form"/>
    <form  suffix="ods" tag="VBpret,ind,s-form"/>
    <form  suffix="å" tag="VBimper"/>
    <form  suffix="å" tag="VBinf,aktiv"/>
    <form  suffix="å-" tag="VBc"/>
    <form  suffix="å-" tag="VBsms"/>
    <form  suffix="ående" tag="VBpres_part,nom"/>
    <form  suffix="åendes" tag="VBpres_part,gen"/>
    <form  suffix="år" tag="VBpres,ind,aktiv"/>
    <form  suffix="ås" tag="VBinf,s-form"/>
    <form  suffix="ås" tag="VBpres,ind,s-form"/>
    <form  suffix="ått" tag="VBsup,aktiv"/>
    <form  suffix="åtts" tag="VBsup,s-form"/>
  </table>
  <table name="VB39" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="" tag="VBc"/>
    <form  suffix="" tag="VBimper"/>
    <form  suffix="" tag="VBinf,aktiv"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="d" tag="VBc"/>
    <form  suffix="d" tag="VBimper"/>
    <form  suffix="d-" tag="VBc"/>
    <form  suffix="d-" tag="VBsms"/>
    <form  suffix="da" tag="VBinf,aktiv"/>
    <form  suffix="dande" tag="VBpres_part,nom"/>
    <form  suffix="dandes" tag="VBpres_part,gen"/>
    <form  suffix="das" tag="VBinf,s-form"/>
    <form  suffix="dd" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="dda" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="dda" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="dda" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ddas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ddas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ddas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="dde" tag="VBpret,ind,aktiv"/>
    <form  suffix="dde" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ddes" tag="VBpret,ind,s-form"/>
    <form  suffix="ddes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="dds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="der" tag="VBpres,ind,aktiv"/>
    <form  suffix="ds" tag="VBpres,ind,s-form"/>
    <form  suffix="ende" tag="VBpres_part,nom"/>
    <form  suffix="endes" tag="VBpres_part,gen"/>
    <form  suffix="r" tag="VBpres,ind,aktiv"/>
    <form  suffix="s" tag="VBinf,s-form"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="tt" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="tt" tag="VBsup,aktiv"/>
    <form  suffix="tts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="tts" tag="VBsup,s-form"/>
  </table>
  <table name="VB40" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="" tag="VBimper"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="ande" tag="VBpres_part,nom"/>
    <form  suffix="andes" tag="VBpres_part,gen"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="d" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="da" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="da" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="da" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="das" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="das" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="das" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="de" tag="VBpret,ind,aktiv"/>
    <form  suffix="de" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="des" tag="VBpret,ind,s-form"/>
    <form  suffix="des" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="er" tag="VBpres,ind,aktiv"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="t" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="t" tag="VBsup,aktiv"/>
    <form  suffix="ts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB41" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="inga" tag="VBimper"/>
    <form  suffix="agt" tag="VBsup,aktiv"/>
    <form  suffix="agte" tag="VBpret,ind,aktiv"/>
    <form  suffix="agtes" tag="VBpret,ind,s-form"/>
    <form  suffix="agts" tag="VBsup,s-form"/>
    <form  suffix="ing" tag="VBc"/>
    <form  suffix="ing-" tag="VBc"/>
    <form  suffix="ing-" tag="VBsms"/>
    <form  suffix="inga" tag="VBinf,aktiv"/>
    <form  suffix="ingad" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ingade" tag="VBpret,ind,aktiv"/>
    <form  suffix="ingade" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ingade" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ingade" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ingade" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ingades" tag="VBpret,ind,s-form"/>
    <form  suffix="ingades" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ingades" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ingades" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ingades" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ingads" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ingande" tag="VBpres_part,nom"/>
    <form  suffix="ingandes" tag="VBpres_part,gen"/>
    <form  suffix="ingar" tag="VBpres,ind,aktiv"/>
    <form  suffix="ingas" tag="VBinf,s-form"/>
    <form  suffix="ingas" tag="VBpres,ind,s-form"/>
    <form  suffix="ingat" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="ingat" tag="VBsup,aktiv"/>
    <form  suffix="ingats" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ingats" tag="VBsup,s-form"/>
  </table>
  <table name="VB42" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="å" tag="VBc"/>
    <form  suffix="ick" tag="VBpret,ind,aktiv"/>
    <form  suffix="icks" tag="VBpret,ind,s-form"/>
    <form  suffix="inge" tag="VBpret,konj,aktiv"/>
    <form  suffix="inges" tag="VBpret,konj,s-form"/>
    <form  suffix="å" tag="VBimper"/>
    <form  suffix="å" tag="VBinf,aktiv"/>
    <form  suffix="å-" tag="VBc"/>
    <form  suffix="å-" tag="VBsms"/>
    <form  suffix="ående" tag="VBpres_part,nom"/>
    <form  suffix="åendes" tag="VBpres_part,gen"/>
    <form  suffix="år" tag="VBpres,ind,aktiv"/>
    <form  suffix="ås" tag="VBinf,s-form"/>
    <form  suffix="ås" tag="VBpres,ind,s-form"/>
    <form  suffix="ått" tag="VBsup,aktiv"/>
    <form  suffix="åtts" tag="VBsup,s-form"/>
  </table>
  <table name="VB43" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="uda" tag="VBinf,aktiv"/>
    <form  suffix="ud" tag="VBc"/>
    <form  suffix="ud" tag="VBimper"/>
    <form  suffix="ud-" tag="VBc"/>
    <form  suffix="ud-" tag="VBsms"/>
    <form  suffix="udande" tag="VBpres_part,nom"/>
    <form  suffix="udandes" tag="VBpres_part,gen"/>
    <form  suffix="udas" tag="VBinf,s-form"/>
    <form  suffix="ude" tag="VBpres,konj,aktiv"/>
    <form  suffix="uden" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="udens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="uder" tag="VBpres,ind,aktiv"/>
    <form  suffix="udes" tag="VBpres,ind,s-form"/>
    <form  suffix="udes" tag="VBpres,konj,s-form"/>
    <form  suffix="udet" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="udets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="udit" tag="VBsup,aktiv"/>
    <form  suffix="udits" tag="VBsup,s-form"/>
    <form  suffix="udna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="udna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="udna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="udnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="udnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="udnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="udne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="udnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="uds" tag="VBpres,ind,s-form"/>
    <form  suffix="öd" tag="VBpret,ind,aktiv"/>
    <form  suffix="öde" tag="VBpret,konj,aktiv"/>
    <form  suffix="ödes" tag="VBpret,konj,s-form"/>
    <form  suffix="öds" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB44" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="uta" tag="VBinf,aktiv"/>
    <form  suffix="ut" tag="VBc"/>
    <form  suffix="ut" tag="VBimper"/>
    <form  suffix="ut-" tag="VBc"/>
    <form  suffix="ut-" tag="VBsms"/>
    <form  suffix="utande" tag="VBpres_part,nom"/>
    <form  suffix="utandes" tag="VBpres_part,gen"/>
    <form  suffix="utas" tag="VBinf,s-form"/>
    <form  suffix="ute" tag="VBpres,konj,aktiv"/>
    <form  suffix="uten" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="utens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="uter" tag="VBpres,ind,aktiv"/>
    <form  suffix="utes" tag="VBpres,ind,s-form"/>
    <form  suffix="utes" tag="VBpres,konj,s-form"/>
    <form  suffix="utet" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="utets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="utit" tag="VBsup,aktiv"/>
    <form  suffix="utits" tag="VBsup,s-form"/>
    <form  suffix="utna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="utna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="utna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="utnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="utnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="utnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="utne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="utnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="uts" tag="VBpres,ind,s-form"/>
    <form  suffix="öt" tag="VBpret,ind,aktiv"/>
    <form  suffix="öte" tag="VBpret,konj,aktiv"/>
    <form  suffix="ötes" tag="VBpret,konj,s-form"/>
    <form  suffix="öts" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB45" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="as" tag="VBimper"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="as" tag="VBpres,ind,s-form"/>
    <form  suffix="ats" tag="VBsup,s-form"/>
    <form  suffix="tes" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB46" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="ära" tag="VBinf,aktiv"/>
    <form  suffix="ar" tag="VBpret,ind,aktiv"/>
    <form  suffix="ars" tag="VBpret,ind,s-form"/>
    <form  suffix="ure" tag="VBpret,konj,aktiv"/>
    <form  suffix="uren" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="urens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ures" tag="VBpret,konj,s-form"/>
    <form  suffix="uret" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="urets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="urit" tag="VBsup,aktiv"/>
    <form  suffix="urits" tag="VBsup,s-form"/>
    <form  suffix="urna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="urna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="urna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="urnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="urnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="urnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="urne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="urnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="är" tag="VBc"/>
    <form  suffix="är" tag="VBimper"/>
    <form  suffix="är" tag="VBpres,ind,aktiv"/>
    <form  suffix="är-" tag="VBc"/>
    <form  suffix="är-" tag="VBsms"/>
    <form  suffix="ärande" tag="VBpres_part,nom"/>
    <form  suffix="ärandes" tag="VBpres_part,gen"/>
    <form  suffix="äras" tag="VBinf,s-form"/>
    <form  suffix="äre" tag="VBpres,konj,aktiv"/>
    <form  suffix="äres" tag="VBpres,ind,s-form"/>
    <form  suffix="äres" tag="VBpres,konj,s-form"/>
    <form  suffix="ärs" tag="VBpres,ind,s-form"/>
  </table>
  <table name="VB47" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="a" tag="VBimper"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="ad" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ade" tag="VBpret,ind,aktiv"/>
    <form  suffix="ade" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ade" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ade" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ade" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ades" tag="VBpret,ind,s-form"/>
    <form  suffix="ades" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ades" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ades" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ades" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ads" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ande" tag="VBpres_part,nom"/>
    <form  suffix="andes" tag="VBpres_part,gen"/>
    <form  suffix="ar" tag="VBpres,ind,aktiv"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="as" tag="VBpres,ind,s-form"/>
    <form  suffix="at" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="at" tag="VBsup,aktiv"/>
    <form  suffix="ats" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ats" tag="VBsup,s-form"/>
    <form  suffix="d" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="da" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="da" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="da" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="das" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="das" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="das" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="de" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="des" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="t" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="t" tag="VBsup,aktiv"/>
    <form  suffix="te" tag="VBpret,ind,aktiv"/>
    <form  suffix="tes" tag="VBpret,ind,s-form"/>
    <form  suffix="ts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB48" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="la" tag="VBimper"/>
    <form  suffix="la" tag="VBinf,aktiv"/>
    <form  suffix="lad" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="lade" tag="VBpret,ind,aktiv"/>
    <form  suffix="lade" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="lade" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="lade" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="lade" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="lades" tag="VBpret,ind,s-form"/>
    <form  suffix="lades" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="lades" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="lades" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="lades" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="lads" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="lande" tag="VBpres_part,nom"/>
    <form  suffix="landes" tag="VBpres_part,gen"/>
    <form  suffix="lar" tag="VBpres,ind,aktiv"/>
    <form  suffix="las" tag="VBinf,s-form"/>
    <form  suffix="las" tag="VBpres,ind,s-form"/>
    <form  suffix="lat" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="lat" tag="VBsup,aktiv"/>
    <form  suffix="lats" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="lats" tag="VBsup,s-form"/>
    <form  suffix="mel" tag="VBc"/>
    <form  suffix="mel-" tag="VBc"/>
    <form  suffix="mel-" tag="VBsms"/>
  </table>
  <table name="VB49" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="åta" tag="VBinf,aktiv"/>
    <form  suffix="ät" tag="VBpret,ind,aktiv"/>
    <form  suffix="äts" tag="VBpret,ind,s-form"/>
    <form  suffix="åt" tag="VBc"/>
    <form  suffix="åt" tag="VBimper"/>
    <form  suffix="åt-" tag="VBc"/>
    <form  suffix="åt-" tag="VBsms"/>
    <form  suffix="åtande" tag="VBpres_part,nom"/>
    <form  suffix="åtandes" tag="VBpres_part,gen"/>
    <form  suffix="åtas" tag="VBinf,s-form"/>
    <form  suffix="åte" tag="VBpres,konj,aktiv"/>
    <form  suffix="åte" tag="VBpret,konj,aktiv"/>
    <form  suffix="åten" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="åtens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="åter" tag="VBpres,ind,aktiv"/>
    <form  suffix="åtes" tag="VBpres,ind,s-form"/>
    <form  suffix="åtes" tag="VBpres,konj,s-form"/>
    <form  suffix="åtes" tag="VBpret,konj,s-form"/>
    <form  suffix="åtet" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="åtets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="åtit" tag="VBsup,aktiv"/>
    <form  suffix="åtits" tag="VBsup,s-form"/>
    <form  suffix="åtna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="åtna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="åtna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="åtnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="åtnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="åtnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="åtne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="åtnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="åts" tag="VBpres,ind,s-form"/>
  </table>
  <table name="VB50" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="a" tag="VBimper"/>
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="ad" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ade" tag="VBpret,ind,aktiv"/>
    <form  suffix="ade" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ade" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ade" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ade" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ades" tag="VBpret,ind,s-form"/>
    <form  suffix="ades" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ades" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ades" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ades" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ads" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ande" tag="VBpres_part,nom"/>
    <form  suffix="andes" tag="VBpres_part,gen"/>
    <form  suffix="ar" tag="VBpres,ind,aktiv"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="as" tag="VBpres,ind,s-form"/>
    <form  suffix="at" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="at" tag="VBsup,aktiv"/>
    <form  suffix="ats" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ats" tag="VBsup,s-form"/>
    <form  suffix="e" tag="VBc"/>
    <form  suffix="e-" tag="VBc"/>
    <form  suffix="e-" tag="VBsms"/>
  </table>
  <table name="VB51" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="la" tag="VBimper"/>
    <form  suffix="la" tag="VBinf,aktiv"/>
    <form  suffix="lade" tag="VBpret,ind,aktiv"/>
    <form  suffix="lades" tag="VBpret,ind,s-form"/>
    <form  suffix="lande" tag="VBpres_part,nom"/>
    <form  suffix="landes" tag="VBpres_part,gen"/>
    <form  suffix="lar" tag="VBpres,ind,aktiv"/>
    <form  suffix="las" tag="VBinf,s-form"/>
    <form  suffix="las" tag="VBpres,ind,s-form"/>
    <form  suffix="lat" tag="VBsup,aktiv"/>
    <form  suffix="lats" tag="VBsup,s-form"/>
    <form  suffix="mel" tag="VBc"/>
    <form  suffix="mel-" tag="VBc"/>
    <form  suffix="mel-" tag="VBsms"/>
  </table>
  <table name="VB52" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="" tag="VBimper"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="ande" tag="VBpres_part,nom"/>
    <form  suffix="andes" tag="VBpres_part,gen"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="at" tag="VBsup,aktiv"/>
    <form  suffix="ats" tag="VBsup,s-form"/>
    <form  suffix="d" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="da" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="da" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="da" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="das" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="das" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="das" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="de" tag="VBpret,ind,aktiv"/>
    <form  suffix="de" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="des" tag="VBpret,ind,s-form"/>
    <form  suffix="des" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="er" tag="VBpres,ind,aktiv"/>
    <form  suffix="es" tag="VBpres,ind,s-form"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="t" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="t" tag="VBsup,aktiv"/>
    <form  suffix="ts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB53" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="des" tag="VBpret,ind,s-form"/>
    <form  suffix="es" tag="VBimper"/>
    <form  suffix="es" tag="VBpres,ind,s-form"/>
    <form  suffix="s" tag="VBimper"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB54" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="inda" tag="VBinf,aktiv"/>
    <form  suffix="and" tag="VBpret,ind,aktiv"/>
    <form  suffix="ands" tag="VBpret,ind,s-form"/>
    <form  suffix="ind" tag="VBc"/>
    <form  suffix="ind" tag="VBimper"/>
    <form  suffix="ind-" tag="VBc"/>
    <form  suffix="ind-" tag="VBsms"/>
    <form  suffix="indande" tag="VBpres_part,nom"/>
    <form  suffix="indandes" tag="VBpres_part,gen"/>
    <form  suffix="indas" tag="VBinf,s-form"/>
    <form  suffix="inde" tag="VBpres,konj,aktiv"/>
    <form  suffix="inder" tag="VBpres,ind,aktiv"/>
    <form  suffix="indes" tag="VBpres,ind,s-form"/>
    <form  suffix="indes" tag="VBpres,konj,s-form"/>
    <form  suffix="inds" tag="VBpres,ind,s-form"/>
    <form  suffix="unde" tag="VBpret,konj,aktiv"/>
    <form  suffix="unden" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="undens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="undes" tag="VBpret,konj,s-form"/>
    <form  suffix="undet" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="undets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="undit" tag="VBsup,aktiv"/>
    <form  suffix="undits" tag="VBsup,s-form"/>
    <form  suffix="undna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="undna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="undna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="undnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="undnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="undnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="undne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="undnes" tag="VBpret_part,def,sg,masc,gen"/>
  </table>
  <table name="VB55" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="ipa" tag="VBinf,aktiv"/>
    <form  suffix="ep" tag="VBpret,ind,aktiv"/>
    <form  suffix="eps" tag="VBpret,ind,s-form"/>
    <form  suffix="ip" tag="VBc"/>
    <form  suffix="ip" tag="VBimper"/>
    <form  suffix="ip-" tag="VBc"/>
    <form  suffix="ip-" tag="VBsms"/>
    <form  suffix="ipande" tag="VBpres_part,nom"/>
    <form  suffix="ipandes" tag="VBpres_part,gen"/>
    <form  suffix="ipas" tag="VBinf,s-form"/>
    <form  suffix="ipe" tag="VBpres,konj,aktiv"/>
    <form  suffix="ipe" tag="VBpret,konj,aktiv"/>
    <form  suffix="ipen" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ipens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="iper" tag="VBpres,ind,aktiv"/>
    <form  suffix="ipes" tag="VBpres,ind,s-form"/>
    <form  suffix="ipes" tag="VBpres,konj,s-form"/>
    <form  suffix="ipes" tag="VBpret,konj,s-form"/>
    <form  suffix="ipet" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="ipets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ipit" tag="VBsup,aktiv"/>
    <form  suffix="ipits" tag="VBsup,s-form"/>
    <form  suffix="ipna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ipna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ipna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ipnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ipnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ipnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ipne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ipnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ips" tag="VBpres,ind,s-form"/>
  </table>
  <table name="VB56" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="a" tag="VBimper"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="" tag="VBimper"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="ad" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ade" tag="VBpret,ind,aktiv"/>
    <form  suffix="ade" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ade" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ade" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ade" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ades" tag="VBpret,ind,s-form"/>
    <form  suffix="ades" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ades" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ades" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ades" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ads" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ande" tag="VBpres_part,nom"/>
    <form  suffix="andes" tag="VBpres_part,gen"/>
    <form  suffix="ar" tag="VBpres,ind,aktiv"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="as" tag="VBpres,ind,s-form"/>
    <form  suffix="at" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="at" tag="VBsup,aktiv"/>
    <form  suffix="ats" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ats" tag="VBsup,s-form"/>
    <form  suffix="er" tag="VBpres,ind,aktiv"/>
    <form  suffix="es" tag="VBpres,ind,s-form"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="t" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="t" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="t" tag="VBsup,aktiv"/>
    <form  suffix="ta" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ta" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ta" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="tas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="tas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="tas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="te" tag="VBpret,ind,aktiv"/>
    <form  suffix="te" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="tes" tag="VBpret,ind,s-form"/>
    <form  suffix="tes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ts" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB57" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="uga" tag="VBinf,aktiv"/>
    <form  suffix="ug" tag="VBc"/>
    <form  suffix="ug" tag="VBimper"/>
    <form  suffix="ug-" tag="VBc"/>
    <form  suffix="ug-" tag="VBsms"/>
    <form  suffix="ugande" tag="VBpres_part,nom"/>
    <form  suffix="ugandes" tag="VBpres_part,gen"/>
    <form  suffix="ugas" tag="VBinf,s-form"/>
    <form  suffix="uge" tag="VBpres,konj,aktiv"/>
    <form  suffix="ugen" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ugens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="uger" tag="VBpres,ind,aktiv"/>
    <form  suffix="uges" tag="VBpres,ind,s-form"/>
    <form  suffix="uges" tag="VBpres,konj,s-form"/>
    <form  suffix="uget" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="ugets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ugit" tag="VBsup,aktiv"/>
    <form  suffix="ugits" tag="VBsup,s-form"/>
    <form  suffix="ugna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ugna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ugna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ugnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ugnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ugnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ugne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ugnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ugs" tag="VBpres,ind,s-form"/>
    <form  suffix="ög" tag="VBpret,ind,aktiv"/>
    <form  suffix="öge" tag="VBpret,konj,aktiv"/>
    <form  suffix="öges" tag="VBpret,konj,s-form"/>
    <form  suffix="ögs" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB58" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="ugga" tag="VBinf,aktiv"/>
    <form  suffix="ugg" tag="VBc"/>
    <form  suffix="ugg" tag="VBimper"/>
    <form  suffix="ugg-" tag="VBc"/>
    <form  suffix="ugg-" tag="VBsms"/>
    <form  suffix="uggande" tag="VBpres_part,nom"/>
    <form  suffix="uggandes" tag="VBpres_part,gen"/>
    <form  suffix="uggas" tag="VBinf,s-form"/>
    <form  suffix="ugge" tag="VBpres,konj,aktiv"/>
    <form  suffix="uggen" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="uggens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ugger" tag="VBpres,ind,aktiv"/>
    <form  suffix="ugges" tag="VBpres,ind,s-form"/>
    <form  suffix="ugges" tag="VBpres,konj,s-form"/>
    <form  suffix="ugget" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="uggets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="uggit" tag="VBsup,aktiv"/>
    <form  suffix="uggits" tag="VBsup,s-form"/>
    <form  suffix="uggna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="uggna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="uggna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="uggnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="uggnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="uggnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="uggne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="uggnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="uggs" tag="VBpres,ind,s-form"/>
    <form  suffix="ögg" tag="VBpret,ind,aktiv"/>
    <form  suffix="ögge" tag="VBpret,konj,aktiv"/>
    <form  suffix="ögges" tag="VBpret,konj,s-form"/>
    <form  suffix="öggs" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB59" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="älja" tag="VBinf,aktiv"/>
    <form  suffix="ald" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="alda" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="alda" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="alda" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="aldas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="aldas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="aldas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="alde" tag="VBpret,ind,aktiv"/>
    <form  suffix="alde" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="aldes" tag="VBpret,ind,s-form"/>
    <form  suffix="aldes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="alds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="alt" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="alt" tag="VBsup,aktiv"/>
    <form  suffix="alts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="alts" tag="VBsup,s-form"/>
    <form  suffix="älj" tag="VBc"/>
    <form  suffix="älj" tag="VBimper"/>
    <form  suffix="älj-" tag="VBc"/>
    <form  suffix="älj-" tag="VBsms"/>
    <form  suffix="äljande" tag="VBpres_part,nom"/>
    <form  suffix="äljandes" tag="VBpres_part,gen"/>
    <form  suffix="äljas" tag="VBinf,s-form"/>
    <form  suffix="äljer" tag="VBpres,ind,aktiv"/>
    <form  suffix="äljes" tag="VBpres,ind,s-form"/>
    <form  suffix="äljs" tag="VBpres,ind,s-form"/>
  </table>
  <table name="VB60" rads=".*(klag|svar|vädj|pröv|plåg|predik)" fast="-">
    <!-- 6 members -->
    <form  suffix="a" tag="VBimper"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="ad" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ade" tag="VBpret,ind,aktiv"/>
    <form  suffix="ade" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ade" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ade" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ade" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ades" tag="VBpret,ind,s-form"/>
    <form  suffix="ades" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ades" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ades" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ades" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ads" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ande" tag="VBpres_part,nom"/>
    <form  suffix="andes" tag="VBpres_part,gen"/>
    <form  suffix="ar" tag="VBpres,ind,aktiv"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="as" tag="VBpres,ind,s-form"/>
    <form  suffix="at" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="at" tag="VBsup,aktiv"/>
    <form  suffix="ats" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ats" tag="VBsup,s-form"/>
    <form  suffix="o" tag="VBc"/>
    <form  suffix="o-" tag="VBc"/>
    <form  suffix="o-" tag="VBsms"/>
  </table>
  <table name="VB61" rads=".*(nagelf|villf|genomf|framf|lagf|f)" fast="-">
    <!-- 6 members -->
    <form  suffix="ara" tag="VBinf,aktiv"/>
    <form  suffix="ar" tag="VBc"/>
    <form  suffix="ar" tag="VBimper"/>
    <form  suffix="ar" tag="VBpres,ind,aktiv"/>
    <form  suffix="ar-" tag="VBc"/>
    <form  suffix="ar-" tag="VBsms"/>
    <form  suffix="arande" tag="VBpres_part,nom"/>
    <form  suffix="arandes" tag="VBpres_part,gen"/>
    <form  suffix="aras" tag="VBinf,s-form"/>
    <form  suffix="are" tag="VBpres,konj,aktiv"/>
    <form  suffix="aren" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="arens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ares" tag="VBpres,ind,s-form"/>
    <form  suffix="ares" tag="VBpres,konj,s-form"/>
    <form  suffix="aret" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="arets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="arit" tag="VBsup,aktiv"/>
    <form  suffix="arits" tag="VBsup,s-form"/>
    <form  suffix="arna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="arna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="arna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="arnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="arnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="arnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="arne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="arnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ars" tag="VBpres,ind,s-form"/>
    <form  suffix="or" tag="VBpret,ind,aktiv"/>
    <form  suffix="ore" tag="VBpret,konj,aktiv"/>
    <form  suffix="ores" tag="VBpret,konj,s-form"/>
    <form  suffix="ors" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB62" rads=".*(best|dr|uppspr|underst|spr|undanst)" fast="-">
    <!-- 6 members -->
    <form  suffix="icka" tag="VBinf,aktiv"/>
    <form  suffix="ack" tag="VBpret,ind,aktiv"/>
    <form  suffix="acks" tag="VBpret,ind,s-form"/>
    <form  suffix="ick" tag="VBc"/>
    <form  suffix="ick" tag="VBimper"/>
    <form  suffix="ick-" tag="VBc"/>
    <form  suffix="ick-" tag="VBsms"/>
    <form  suffix="ickande" tag="VBpres_part,nom"/>
    <form  suffix="ickandes" tag="VBpres_part,gen"/>
    <form  suffix="ickas" tag="VBinf,s-form"/>
    <form  suffix="icke" tag="VBpres,konj,aktiv"/>
    <form  suffix="icker" tag="VBpres,ind,aktiv"/>
    <form  suffix="ickes" tag="VBpres,ind,s-form"/>
    <form  suffix="ickes" tag="VBpres,konj,s-form"/>
    <form  suffix="icks" tag="VBpres,ind,s-form"/>
    <form  suffix="ucke" tag="VBpret,konj,aktiv"/>
    <form  suffix="ucken" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="uckens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="uckes" tag="VBpret,konj,s-form"/>
    <form  suffix="ucket" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="uckets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="uckit" tag="VBsup,aktiv"/>
    <form  suffix="uckits" tag="VBsup,s-form"/>
    <form  suffix="uckna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="uckna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="uckna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ucknas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ucknas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ucknas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="uckne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ucknes" tag="VBpret_part,def,sg,masc,gen"/>
  </table>
  <table name="VB63" rads=".*(underst|uppst|överst|landst|best|st)" fast="-">
    <!-- 6 members -->
    <form  suffix="iga" tag="VBinf,aktiv"/>
    <form  suffix="eg" tag="VBpret,ind,aktiv"/>
    <form  suffix="egs" tag="VBpret,ind,s-form"/>
    <form  suffix="ig" tag="VBc"/>
    <form  suffix="ig" tag="VBimper"/>
    <form  suffix="ig-" tag="VBc"/>
    <form  suffix="ig-" tag="VBsms"/>
    <form  suffix="igande" tag="VBpres_part,nom"/>
    <form  suffix="igandes" tag="VBpres_part,gen"/>
    <form  suffix="igas" tag="VBinf,s-form"/>
    <form  suffix="ige" tag="VBpres,konj,aktiv"/>
    <form  suffix="ige" tag="VBpret,konj,aktiv"/>
    <form  suffix="igen" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="igens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="iger" tag="VBpres,ind,aktiv"/>
    <form  suffix="iges" tag="VBpres,ind,s-form"/>
    <form  suffix="iges" tag="VBpres,konj,s-form"/>
    <form  suffix="iges" tag="VBpret,konj,s-form"/>
    <form  suffix="iget" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="igets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="igit" tag="VBsup,aktiv"/>
    <form  suffix="igits" tag="VBsup,s-form"/>
    <form  suffix="igna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="igna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="igna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ignas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ignas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ignas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="igne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ignes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="igs" tag="VBpres,ind,s-form"/>
  </table>
  <table name="VB64" rads=".*(åtskil|avskil|utskil|särskil|skil|urskil)" fast="-">
    <!-- 6 members -->
    <form  suffix="ja" tag="VBinf,aktiv"/>
    <form  suffix="d" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="da" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="da" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="da" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="das" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="das" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="das" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="de" tag="VBpret,ind,aktiv"/>
    <form  suffix="de" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="des" tag="VBpret,ind,s-form"/>
    <form  suffix="des" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="j" tag="VBc"/>
    <form  suffix="j" tag="VBimper"/>
    <form  suffix="j-" tag="VBc"/>
    <form  suffix="j-" tag="VBsms"/>
    <form  suffix="jande" tag="VBpres_part,nom"/>
    <form  suffix="jandes" tag="VBpres_part,gen"/>
    <form  suffix="jas" tag="VBinf,s-form"/>
    <form  suffix="jer" tag="VBpres,ind,aktiv"/>
    <form  suffix="jes" tag="VBpres,ind,s-form"/>
    <form  suffix="js" tag="VBpres,ind,s-form"/>
    <form  suffix="t" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="t" tag="VBsup,aktiv"/>
    <form  suffix="ts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB65" rads=".*(handha|ha|omhänderha|inneha|föreha)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="VBc"/>
    <form  suffix="" tag="VBimper"/>
    <form  suffix="" tag="VBinf,aktiv"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="de" tag="VBpret,ind,aktiv"/>
    <form  suffix="des" tag="VBpret,ind,s-form"/>
    <form  suffix="ft" tag="VBsup,aktiv"/>
    <form  suffix="fts" tag="VBsup,s-form"/>
    <form  suffix="nde" tag="VBpres_part,nom"/>
    <form  suffix="ndes" tag="VBpres_part,gen"/>
    <form  suffix="r" tag="VBpres,ind,aktiv"/>
    <form  suffix="s" tag="VBinf,s-form"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="va" tag="VBinf,aktiv"/>
    <form  suffix="vande" tag="VBpres_part,nom"/>
    <form  suffix="vandes" tag="VBpres_part,gen"/>
    <form  suffix="vas" tag="VBinf,s-form"/>
    <form  suffix="ver" tag="VBpres,ind,aktiv"/>
  </table>
  <table name="VB66" rads=".*(kvarbl|utebl|varsebl|förbl|bl)" fast="-">
    <!-- 5 members -->
    <form  suffix="i" tag="VBimper"/>
    <form  suffix="ev" tag="VBpret,ind,aktiv"/>
    <form  suffix="eve" tag="VBpret,konj,aktiv"/>
    <form  suffix="i" tag="VBinf,aktiv"/>
    <form  suffix="ir" tag="VBpres,ind,aktiv"/>
    <form  suffix="iv" tag="VBc"/>
    <form  suffix="iv" tag="VBimper"/>
    <form  suffix="iv-" tag="VBc"/>
    <form  suffix="iv-" tag="VBsms"/>
    <form  suffix="iva" tag="VBinf,aktiv"/>
    <form  suffix="ivande" tag="VBpres_part,nom"/>
    <form  suffix="ivandes" tag="VBpres_part,gen"/>
    <form  suffix="ive" tag="VBpres,konj,aktiv"/>
    <form  suffix="iven" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ivens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="iver" tag="VBpres,ind,aktiv"/>
    <form  suffix="ivet" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="ivets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ivit" tag="VBsup,aktiv"/>
    <form  suffix="ivna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ivna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ivna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ivnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ivnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ivnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ivne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ivnes" tag="VBpret_part,def,sg,masc,gen"/>
  </table>
  <table name="VB67" rads=".*(anstr|fogstr|bestr|str|understr)" fast="-">
    <!-- 5 members -->
    <form  suffix="yka" tag="VBinf,aktiv"/>
    <form  suffix="uken" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ukens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="uket" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="ukets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ukit" tag="VBsup,aktiv"/>
    <form  suffix="ukits" tag="VBsup,s-form"/>
    <form  suffix="ukna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ukna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ukna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="uknas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="uknas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="uknas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ukne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="uknes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="yk" tag="VBc"/>
    <form  suffix="yk" tag="VBimper"/>
    <form  suffix="yk-" tag="VBc"/>
    <form  suffix="yk-" tag="VBsms"/>
    <form  suffix="ykande" tag="VBpres_part,nom"/>
    <form  suffix="ykandes" tag="VBpres_part,gen"/>
    <form  suffix="ykas" tag="VBinf,s-form"/>
    <form  suffix="yke" tag="VBpres,konj,aktiv"/>
    <form  suffix="yker" tag="VBpres,ind,aktiv"/>
    <form  suffix="ykes" tag="VBpres,ind,s-form"/>
    <form  suffix="ykes" tag="VBpres,konj,s-form"/>
    <form  suffix="yks" tag="VBpres,ind,s-form"/>
    <form  suffix="ök" tag="VBpret,ind,aktiv"/>
    <form  suffix="öke" tag="VBpret,konj,aktiv"/>
    <form  suffix="ökes" tag="VBpret,konj,s-form"/>
    <form  suffix="öks" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB68" rads=".*(uts|tills|föruts|mots|frams)" fast="-">
    <!-- 5 members -->
    <form  suffix="äga" tag="VBinf,aktiv"/>
    <form  suffix="ade" tag="VBpret,ind,aktiv"/>
    <form  suffix="ades" tag="VBpret,ind,s-form"/>
    <form  suffix="agd" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="agda" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="agda" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="agda" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="agdas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="agdas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="agdas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="agde" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="agdes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="agds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="agt" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="agt" tag="VBsup,aktiv"/>
    <form  suffix="agts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="agts" tag="VBsup,s-form"/>
    <form  suffix="äg" tag="VBc"/>
    <form  suffix="äg" tag="VBimper"/>
    <form  suffix="äg-" tag="VBc"/>
    <form  suffix="äg-" tag="VBsms"/>
    <form  suffix="ägande" tag="VBpres_part,nom"/>
    <form  suffix="ägandes" tag="VBpres_part,gen"/>
    <form  suffix="ägas" tag="VBinf,s-form"/>
    <form  suffix="äger" tag="VBpres,ind,aktiv"/>
    <form  suffix="äges" tag="VBpres,ind,s-form"/>
    <form  suffix="ägs" tag="VBpres,ind,s-form"/>
  </table>
  <table name="VB69" rads=".*(unders|återförs|utförs|s|förs)" fast="-">
    <!-- 5 members -->
    <form  suffix="älja" tag="VBinf,aktiv"/>
    <form  suffix="älj" tag="VBc"/>
    <form  suffix="älj" tag="VBimper"/>
    <form  suffix="älj-" tag="VBc"/>
    <form  suffix="älj-" tag="VBsms"/>
    <form  suffix="äljande" tag="VBpres_part,nom"/>
    <form  suffix="äljandes" tag="VBpres_part,gen"/>
    <form  suffix="äljas" tag="VBinf,s-form"/>
    <form  suffix="äljer" tag="VBpres,ind,aktiv"/>
    <form  suffix="äljes" tag="VBpres,ind,s-form"/>
    <form  suffix="äljs" tag="VBpres,ind,s-form"/>
    <form  suffix="åld" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ålda" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ålda" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ålda" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="åldas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="åldas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="åldas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ålde" tag="VBpret,ind,aktiv"/>
    <form  suffix="ålde" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="åldes" tag="VBpret,ind,s-form"/>
    <form  suffix="åldes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ålds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ålt" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="ålt" tag="VBsup,aktiv"/>
    <form  suffix="ålts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ålts" tag="VBsup,s-form"/>
  </table>
  <table name="VB70" rads=".*(kal|av|opp|smyg|upp)" fast="-">
    <!-- 5 members -->
    <form  suffix="äta" tag="VBinf,aktiv"/>
    <form  suffix="ät" tag="VBc"/>
    <form  suffix="ät" tag="VBimper"/>
    <form  suffix="ät-" tag="VBc"/>
    <form  suffix="ät-" tag="VBsms"/>
    <form  suffix="ätande" tag="VBpres_part,nom"/>
    <form  suffix="ätandes" tag="VBpres_part,gen"/>
    <form  suffix="ätas" tag="VBinf,s-form"/>
    <form  suffix="äte" tag="VBpres,konj,aktiv"/>
    <form  suffix="äten" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ätens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="äter" tag="VBpres,ind,aktiv"/>
    <form  suffix="ätes" tag="VBpres,ind,s-form"/>
    <form  suffix="ätes" tag="VBpres,konj,s-form"/>
    <form  suffix="ätet" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="ätets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ätit" tag="VBsup,aktiv"/>
    <form  suffix="ätits" tag="VBsup,s-form"/>
    <form  suffix="ätna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ätna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ätna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ätnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ätnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ätnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ätne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ätnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="äts" tag="VBpres,ind,s-form"/>
    <form  suffix="åt" tag="VBpret,ind,aktiv"/>
    <form  suffix="åte" tag="VBpret,konj,aktiv"/>
    <form  suffix="åtes" tag="VBpret,konj,s-form"/>
    <form  suffix="åts" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB71" rads=".*(framv|v|tillv|oppv|uppv)" fast="-">
    <!-- 5 members -->
    <form  suffix="äxa" tag="VBimper"/>
    <form  suffix="uxe" tag="VBpret,konj,aktiv"/>
    <form  suffix="uxen" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="uxens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="uxet" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="uxets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="uxit" tag="VBsup,aktiv"/>
    <form  suffix="uxna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="uxna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="uxna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="uxnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="uxnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="uxnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="uxne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="uxnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="äx" tag="VBc"/>
    <form  suffix="äx-" tag="VBc"/>
    <form  suffix="äx-" tag="VBsms"/>
    <form  suffix="äxa" tag="VBinf,aktiv"/>
    <form  suffix="äxande" tag="VBpres_part,nom"/>
    <form  suffix="äxandes" tag="VBpres_part,gen"/>
    <form  suffix="äxe" tag="VBpres,konj,aktiv"/>
    <form  suffix="äxer" tag="VBpres,ind,aktiv"/>
    <form  suffix="äxt" tag="VBsup,aktiv"/>
    <form  suffix="äxte" tag="VBpret,ind,aktiv"/>
  </table>
  <table name="VB72" rads=".*(försp|besm|sm|sp|rundsm)" fast="-">
    <!-- 5 members -->
    <form  suffix="örja" tag="VBinf,aktiv"/>
    <form  suffix="ord" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="orda" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="orda" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="orda" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ordas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ordas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ordas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="orde" tag="VBpret,ind,aktiv"/>
    <form  suffix="orde" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ordes" tag="VBpret,ind,s-form"/>
    <form  suffix="ordes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ords" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ort" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="ort" tag="VBsup,aktiv"/>
    <form  suffix="orts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="orts" tag="VBsup,s-form"/>
    <form  suffix="örj" tag="VBc"/>
    <form  suffix="örj" tag="VBimper"/>
    <form  suffix="örj-" tag="VBc"/>
    <form  suffix="örj-" tag="VBsms"/>
    <form  suffix="örjande" tag="VBpres_part,nom"/>
    <form  suffix="örjandes" tag="VBpres_part,gen"/>
    <form  suffix="örjas" tag="VBinf,s-form"/>
    <form  suffix="örjer" tag="VBpres,ind,aktiv"/>
    <form  suffix="örjes" tag="VBpres,ind,s-form"/>
    <form  suffix="örjs" tag="VBpres,ind,s-form"/>
  </table>
  <table name="VB73" rads=".*(ugnsbak|fören|skap|prisbelön)" fast="-">
    <!-- 4 members -->
    <form  suffix="a" tag="VBimper"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="ad" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ade" tag="VBpret,ind,aktiv"/>
    <form  suffix="ade" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ade" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ade" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ade" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ades" tag="VBpret,ind,s-form"/>
    <form  suffix="ades" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ades" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ades" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ades" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ads" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ande" tag="VBpres_part,nom"/>
    <form  suffix="andes" tag="VBpres_part,gen"/>
    <form  suffix="ar" tag="VBpres,ind,aktiv"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="as" tag="VBpres,ind,s-form"/>
    <form  suffix="at" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="at" tag="VBsup,aktiv"/>
    <form  suffix="ats" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ats" tag="VBsup,s-form"/>
    <form  suffix="t" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="t" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ta" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ta" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ta" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="tas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="tas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="tas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="te" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="tes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ts" tag="VBpret_part,indef,sg,u,gen"/>
  </table>
  <table name="VB74" rads=".*(övervar|närvar|avvar|undvar)" fast="-">
    <!-- 4 members -->
    <form  suffix="a" tag="VBimper"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="ade" tag="VBpret,ind,aktiv"/>
    <form  suffix="ades" tag="VBpret,ind,s-form"/>
    <form  suffix="ande" tag="VBpres_part,nom"/>
    <form  suffix="andes" tag="VBpres_part,gen"/>
    <form  suffix="ar" tag="VBpres,ind,aktiv"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="as" tag="VBpres,ind,s-form"/>
    <form  suffix="at" tag="VBsup,aktiv"/>
    <form  suffix="ats" tag="VBsup,s-form"/>
    <form  suffix="it" tag="VBsup,aktiv"/>
    <form  suffix="its" tag="VBsup,s-form"/>
  </table>
  <table name="VB75" rads=".*(förmäl|omförmäl|genmäl|mäl)" fast="-">
    <!-- 4 members -->
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="" tag="VBimper"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="ande" tag="VBpres_part,nom"/>
    <form  suffix="andes" tag="VBpres_part,gen"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="d" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="da" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="da" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="da" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="das" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="das" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="das" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="de" tag="VBpret,ind,aktiv"/>
    <form  suffix="de" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="des" tag="VBpret,ind,s-form"/>
    <form  suffix="des" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="er" tag="VBpres,ind,aktiv"/>
    <form  suffix="es" tag="VBpres,ind,s-form"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="t" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="t" tag="VBsup,aktiv"/>
    <form  suffix="te" tag="VBpret,ind,aktiv"/>
    <form  suffix="tes" tag="VBpret,ind,s-form"/>
    <form  suffix="ts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB76" rads=".*(triv|stormtriv|vantriv|träng)" fast="-">
    <!-- 4 members -->
    <form  suffix="as" tag="VBimper"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="as" tag="VBpres,ind,s-form"/>
    <form  suffix="des" tag="VBpret,ind,s-form"/>
    <form  suffix="s" tag="VBimper"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB77" rads=".*(åtnöj|vidkänn|nöj|tykänn)" fast="-">
    <!-- 4 members -->
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="des" tag="VBpret,ind,s-form"/>
    <form  suffix="s" tag="VBimper"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB78" rads=".*(långle|i|återfö|le)" fast="-">
    <!-- 4 members -->
    <form  suffix="das" tag="VBinf,s-form"/>
    <form  suffix="dats" tag="VBsup,s-form"/>
    <form  suffix="ddes" tag="VBpret,ind,s-form"/>
    <form  suffix="des" tag="VBimper"/>
    <form  suffix="des" tag="VBpres,ind,s-form"/>
    <form  suffix="ds" tag="VBimper"/>
    <form  suffix="ds" tag="VBpres,ind,s-form"/>
    <form  suffix="tts" tag="VBsup,s-form"/>
  </table>
  <table name="VB79" rads=".*(bel|smål|l|hånl)" fast="-">
    <!-- 4 members -->
    <form  suffix="e" tag="VBc"/>
    <form  suffix="e" tag="VBimper"/>
    <form  suffix="e" tag="VBinf,aktiv"/>
    <form  suffix="e-" tag="VBc"/>
    <form  suffix="e-" tag="VBsms"/>
    <form  suffix="eende" tag="VBpres_part,nom"/>
    <form  suffix="eendes" tag="VBpres_part,gen"/>
    <form  suffix="er" tag="VBpres,ind,aktiv"/>
    <form  suffix="es" tag="VBinf,s-form"/>
    <form  suffix="es" tag="VBpres,ind,s-form"/>
    <form  suffix="ett" tag="VBsup,aktiv"/>
    <form  suffix="etts" tag="VBsup,s-form"/>
    <form  suffix="og" tag="VBpret,ind,aktiv"/>
    <form  suffix="oge" tag="VBpret,konj,aktiv"/>
    <form  suffix="oges" tag="VBpret,konj,s-form"/>
    <form  suffix="ogs" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB80" rads=".*(n|fört|molt|t)" fast="-">
    <!-- 4 members -->
    <form  suffix="iga" tag="VBinf,aktiv"/>
    <form  suffix="eg" tag="VBpret,ind,aktiv"/>
    <form  suffix="egs" tag="VBpret,ind,s-form"/>
    <form  suffix="ig" tag="VBc"/>
    <form  suffix="ig" tag="VBimper"/>
    <form  suffix="ig-" tag="VBc"/>
    <form  suffix="ig-" tag="VBsms"/>
    <form  suffix="igande" tag="VBpres_part,nom"/>
    <form  suffix="igandes" tag="VBpres_part,gen"/>
    <form  suffix="igas" tag="VBinf,s-form"/>
    <form  suffix="ige" tag="VBpres,konj,aktiv"/>
    <form  suffix="ige" tag="VBpret,konj,aktiv"/>
    <form  suffix="iger" tag="VBpres,ind,aktiv"/>
    <form  suffix="iges" tag="VBpres,ind,s-form"/>
    <form  suffix="iges" tag="VBpres,konj,s-form"/>
    <form  suffix="iges" tag="VBpret,konj,s-form"/>
    <form  suffix="igit" tag="VBsup,aktiv"/>
    <form  suffix="igits" tag="VBsup,s-form"/>
    <form  suffix="igs" tag="VBpres,ind,s-form"/>
  </table>
  <table name="VB81" rads=".*(l|ål|anl|förel)" fast="-">
    <!-- 4 members -->
    <form  suffix="igga" tag="VBinf,aktiv"/>
    <form  suffix="egat" tag="VBsup,aktiv"/>
    <form  suffix="egats" tag="VBsup,s-form"/>
    <form  suffix="igg" tag="VBc"/>
    <form  suffix="igg" tag="VBimper"/>
    <form  suffix="igg-" tag="VBc"/>
    <form  suffix="igg-" tag="VBsms"/>
    <form  suffix="iggande" tag="VBpres_part,nom"/>
    <form  suffix="iggandes" tag="VBpres_part,gen"/>
    <form  suffix="iggas" tag="VBinf,s-form"/>
    <form  suffix="igge" tag="VBpres,konj,aktiv"/>
    <form  suffix="igger" tag="VBpres,ind,aktiv"/>
    <form  suffix="igges" tag="VBpres,ind,s-form"/>
    <form  suffix="igges" tag="VBpres,konj,s-form"/>
    <form  suffix="iggs" tag="VBpres,ind,s-form"/>
    <form  suffix="åg" tag="VBpret,ind,aktiv"/>
    <form  suffix="ågge" tag="VBpret,konj,aktiv"/>
    <form  suffix="ågges" tag="VBpret,konj,s-form"/>
    <form  suffix="ågs" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB82" rads=".*(tv|betv|avtv|påtv)" fast="-">
    <!-- 4 members -->
    <form  suffix="inga" tag="VBimper"/>
    <form  suffix="ang" tag="VBpret,ind,aktiv"/>
    <form  suffix="angs" tag="VBpret,ind,s-form"/>
    <form  suffix="ing" tag="VBc"/>
    <form  suffix="ing-" tag="VBc"/>
    <form  suffix="ing-" tag="VBsms"/>
    <form  suffix="inga" tag="VBinf,aktiv"/>
    <form  suffix="ingad" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ingade" tag="VBpret,ind,aktiv"/>
    <form  suffix="ingade" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ingade" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ingade" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ingade" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ingades" tag="VBpret,ind,s-form"/>
    <form  suffix="ingades" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ingades" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ingades" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ingades" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ingads" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ingande" tag="VBpres_part,nom"/>
    <form  suffix="ingandes" tag="VBpres_part,gen"/>
    <form  suffix="ingar" tag="VBpres,ind,aktiv"/>
    <form  suffix="ingas" tag="VBinf,s-form"/>
    <form  suffix="ingas" tag="VBpres,ind,s-form"/>
    <form  suffix="ingat" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="ingat" tag="VBsup,aktiv"/>
    <form  suffix="ingats" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ingats" tag="VBsup,s-form"/>
    <form  suffix="inge" tag="VBpres,konj,aktiv"/>
    <form  suffix="inges" tag="VBpres,konj,s-form"/>
    <form  suffix="unge" tag="VBpret,konj,aktiv"/>
    <form  suffix="ungen" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ungens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="unges" tag="VBpret,konj,s-form"/>
    <form  suffix="unget" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="ungets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ungit" tag="VBsup,aktiv"/>
    <form  suffix="ungits" tag="VBsup,s-form"/>
    <form  suffix="ungna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ungna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ungna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ungnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ungnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ungnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ungne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ungnes" tag="VBpret_part,def,sg,masc,gen"/>
  </table>
  <table name="VB83" rads=".*(kän|min|nän|brän)" fast="-">
    <!-- 4 members -->
    <form  suffix="nas" tag="VBinf,s-form"/>
    <form  suffix="des" tag="VBpret,ind,s-form"/>
    <form  suffix="ns" tag="VBimper"/>
    <form  suffix="ns" tag="VBpres,ind,s-form"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB84" rads=".*(sj|lovsj|avsj|besj)" fast="-">
    <!-- 4 members -->
    <form  suffix="unga" tag="VBinf,aktiv"/>
    <form  suffix="ung" tag="VBc"/>
    <form  suffix="ung" tag="VBimper"/>
    <form  suffix="ung-" tag="VBc"/>
    <form  suffix="ung-" tag="VBsms"/>
    <form  suffix="ungande" tag="VBpres_part,nom"/>
    <form  suffix="ungandes" tag="VBpres_part,gen"/>
    <form  suffix="ungas" tag="VBinf,s-form"/>
    <form  suffix="unge" tag="VBpres,konj,aktiv"/>
    <form  suffix="ungen" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ungens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="unger" tag="VBpres,ind,aktiv"/>
    <form  suffix="unges" tag="VBpres,ind,s-form"/>
    <form  suffix="unges" tag="VBpres,konj,s-form"/>
    <form  suffix="unget" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="ungets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ungit" tag="VBsup,aktiv"/>
    <form  suffix="ungits" tag="VBsup,s-form"/>
    <form  suffix="ungna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ungna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ungna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ungnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ungnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ungnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ungne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ungnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ungs" tag="VBpres,ind,s-form"/>
    <form  suffix="öng" tag="VBpret,ind,aktiv"/>
    <form  suffix="önge" tag="VBpret,konj,aktiv"/>
    <form  suffix="önges" tag="VBpret,konj,s-form"/>
    <form  suffix="öngs" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB85" rads=".*(snabbfr|förfr|nedfr|nerfr)" fast="-">
    <!-- 4 members -->
    <form  suffix="ysa" tag="VBinf,aktiv"/>
    <form  suffix="use" tag="VBpret,konj,aktiv"/>
    <form  suffix="usen" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="usens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="uses" tag="VBpret,konj,s-form"/>
    <form  suffix="uset" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="usets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="usit" tag="VBsup,aktiv"/>
    <form  suffix="usits" tag="VBsup,s-form"/>
    <form  suffix="usna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="usna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="usna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="usnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="usnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="usnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="usne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="usnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ys" tag="VBc"/>
    <form  suffix="ys" tag="VBimper"/>
    <form  suffix="ys" tag="VBpres,ind,s-form"/>
    <form  suffix="ys-" tag="VBc"/>
    <form  suffix="ys-" tag="VBsms"/>
    <form  suffix="ysande" tag="VBpres_part,nom"/>
    <form  suffix="ysandes" tag="VBpres_part,gen"/>
    <form  suffix="ysas" tag="VBinf,s-form"/>
    <form  suffix="yse" tag="VBpres,konj,aktiv"/>
    <form  suffix="yser" tag="VBpres,ind,aktiv"/>
    <form  suffix="yses" tag="VBpres,konj,s-form"/>
    <form  suffix="yst" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="yst" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="yst" tag="VBsup,aktiv"/>
    <form  suffix="ysta" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ysta" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ysta" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ystas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ystas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ystas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="yste" tag="VBpret,ind,aktiv"/>
    <form  suffix="yste" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ystes" tag="VBpret,ind,s-form"/>
    <form  suffix="ystes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ysts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ysts" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ysts" tag="VBsup,s-form"/>
    <form  suffix="ös" tag="VBpret,ind,aktiv"/>
    <form  suffix="ös" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB86" rads=".*(förtr|r|tr|skr)" fast="-">
    <!-- 4 members -->
    <form  suffix="yta" tag="VBinf,aktiv"/>
    <form  suffix="ute" tag="VBpret,konj,aktiv"/>
    <form  suffix="utes" tag="VBpret,konj,s-form"/>
    <form  suffix="utit" tag="VBsup,aktiv"/>
    <form  suffix="utits" tag="VBsup,s-form"/>
    <form  suffix="yt" tag="VBc"/>
    <form  suffix="yt" tag="VBimper"/>
    <form  suffix="yt-" tag="VBc"/>
    <form  suffix="yt-" tag="VBsms"/>
    <form  suffix="ytande" tag="VBpres_part,nom"/>
    <form  suffix="ytandes" tag="VBpres_part,gen"/>
    <form  suffix="ytas" tag="VBinf,s-form"/>
    <form  suffix="yte" tag="VBpres,konj,aktiv"/>
    <form  suffix="yter" tag="VBpres,ind,aktiv"/>
    <form  suffix="ytes" tag="VBpres,ind,s-form"/>
    <form  suffix="ytes" tag="VBpres,konj,s-form"/>
    <form  suffix="yts" tag="VBpres,ind,s-form"/>
    <form  suffix="öt" tag="VBpret,ind,aktiv"/>
    <form  suffix="öts" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB87" rads=".*(försv|avsv|besv|frambesv)" fast="-">
    <!-- 4 members -->
    <form  suffix="ärja" tag="VBinf,aktiv"/>
    <form  suffix="or" tag="VBpret,ind,aktiv"/>
    <form  suffix="ors" tag="VBpret,ind,s-form"/>
    <form  suffix="ure" tag="VBpret,konj,aktiv"/>
    <form  suffix="uren" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="urens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ures" tag="VBpret,konj,s-form"/>
    <form  suffix="uret" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="urets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="urit" tag="VBsup,aktiv"/>
    <form  suffix="urits" tag="VBsup,s-form"/>
    <form  suffix="urna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="urna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="urna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="urnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="urnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="urnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="urne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="urnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="är" tag="VBc"/>
    <form  suffix="är" tag="VBpres,ind,aktiv"/>
    <form  suffix="är-" tag="VBc"/>
    <form  suffix="är-" tag="VBsms"/>
    <form  suffix="ära" tag="VBinf,aktiv"/>
    <form  suffix="ärande" tag="VBpres_part,nom"/>
    <form  suffix="ärandes" tag="VBpres_part,gen"/>
    <form  suffix="äras" tag="VBinf,s-form"/>
    <form  suffix="ärj" tag="VBc"/>
    <form  suffix="ärj" tag="VBimper"/>
    <form  suffix="ärj-" tag="VBc"/>
    <form  suffix="ärj-" tag="VBsms"/>
    <form  suffix="ärjande" tag="VBpres_part,nom"/>
    <form  suffix="ärjandes" tag="VBpres_part,gen"/>
    <form  suffix="ärjas" tag="VBinf,s-form"/>
    <form  suffix="ärjd" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ärjda" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ärjda" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ärjda" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ärjdas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ärjdas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ärjdas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ärjde" tag="VBpret,ind,aktiv"/>
    <form  suffix="ärjde" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ärjdes" tag="VBpret,ind,s-form"/>
    <form  suffix="ärjdes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ärjds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ärje" tag="VBpres,konj,aktiv"/>
    <form  suffix="ärjer" tag="VBpres,ind,aktiv"/>
    <form  suffix="ärjes" tag="VBpres,ind,s-form"/>
    <form  suffix="ärjes" tag="VBpres,konj,s-form"/>
    <form  suffix="ärjs" tag="VBpres,ind,s-form"/>
    <form  suffix="ärjt" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="ärjt" tag="VBsup,aktiv"/>
    <form  suffix="ärjts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ärjts" tag="VBsup,s-form"/>
  </table>
  <table name="VB88" rads=".*(emotst|återuppst|utst|uppst)" fast="-">
    <!-- 4 members -->
    <form  suffix="å" tag="VBc"/>
    <form  suffix="od" tag="VBpret,ind,aktiv"/>
    <form  suffix="ode" tag="VBpret,konj,aktiv"/>
    <form  suffix="odes" tag="VBpret,konj,s-form"/>
    <form  suffix="ods" tag="VBpret,ind,s-form"/>
    <form  suffix="å" tag="VBimper"/>
    <form  suffix="å" tag="VBinf,aktiv"/>
    <form  suffix="å-" tag="VBc"/>
    <form  suffix="å-" tag="VBsms"/>
    <form  suffix="ående" tag="VBpres_part,nom"/>
    <form  suffix="åendes" tag="VBpres_part,gen"/>
    <form  suffix="ånden" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="åndens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="åndet" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="åndets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="åndna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="åndna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="åndna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="åndnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="åndnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="åndnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="åndne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="åndnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="år" tag="VBpres,ind,aktiv"/>
    <form  suffix="ås" tag="VBinf,s-form"/>
    <form  suffix="ås" tag="VBpres,ind,s-form"/>
    <form  suffix="ått" tag="VBsup,aktiv"/>
    <form  suffix="åtts" tag="VBsup,s-form"/>
  </table>
  <table name="VB89" rads=".*(föranl|gr|l|storgr)" fast="-">
    <!-- 4 members -->
    <form  suffix="åta" tag="VBinf,aktiv"/>
    <form  suffix="ät" tag="VBpret,ind,aktiv"/>
    <form  suffix="äte" tag="VBpret,konj,aktiv"/>
    <form  suffix="ätes" tag="VBpret,konj,s-form"/>
    <form  suffix="äts" tag="VBpret,ind,s-form"/>
    <form  suffix="åt" tag="VBc"/>
    <form  suffix="åt" tag="VBimper"/>
    <form  suffix="åt-" tag="VBc"/>
    <form  suffix="åt-" tag="VBsms"/>
    <form  suffix="åtande" tag="VBpres_part,nom"/>
    <form  suffix="åtandes" tag="VBpres_part,gen"/>
    <form  suffix="åtas" tag="VBinf,s-form"/>
    <form  suffix="åte" tag="VBpres,konj,aktiv"/>
    <form  suffix="åter" tag="VBpres,ind,aktiv"/>
    <form  suffix="åtes" tag="VBpres,ind,s-form"/>
    <form  suffix="åtes" tag="VBpres,konj,s-form"/>
    <form  suffix="åtit" tag="VBsup,aktiv"/>
    <form  suffix="åtits" tag="VBsup,s-form"/>
    <form  suffix="åts" tag="VBpres,ind,s-form"/>
  </table>
  <table name="VB90" rads=".*(x-a|sms\:a|x\:a)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="VBc"/>
    <form  suffix="" tag="VBimper"/>
    <form  suffix="" tag="VBinf,aktiv"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="d" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="de" tag="VBpret,ind,aktiv"/>
    <form  suffix="de" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="de" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="de" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="de" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="des" tag="VBpret,ind,s-form"/>
    <form  suffix="des" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="des" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="des" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="des" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="nde" tag="VBpres_part,nom"/>
    <form  suffix="ndes" tag="VBpres_part,gen"/>
    <form  suffix="r" tag="VBpres,ind,aktiv"/>
    <form  suffix="s" tag="VBinf,s-form"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="t" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="t" tag="VBsup,aktiv"/>
    <form  suffix="ts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB91" rads=".*(lyster|lär|värdes)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="VBpres,ind,aktiv"/>
  </table>
  <table name="VB92" rads=".*(sprätt|rist|vält)" fast="-">
    <!-- 3 members -->
    <form  suffix="a" tag="VBimper"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="" tag="VBimper"/>
    <form  suffix="" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="" tag="VBsup,aktiv"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="a" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="a" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="a" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ad" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ade" tag="VBpret,ind,aktiv"/>
    <form  suffix="ade" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ade" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ade" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ade" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ades" tag="VBpret,ind,s-form"/>
    <form  suffix="ades" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ades" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ades" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ades" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ads" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ande" tag="VBpres_part,nom"/>
    <form  suffix="andes" tag="VBpres_part,gen"/>
    <form  suffix="ar" tag="VBpres,ind,aktiv"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="as" tag="VBpres,ind,s-form"/>
    <form  suffix="as" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="as" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="as" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="at" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="at" tag="VBsup,aktiv"/>
    <form  suffix="ats" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ats" tag="VBsup,s-form"/>
    <form  suffix="e" tag="VBpret,ind,aktiv"/>
    <form  suffix="e" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="er" tag="VBpres,ind,aktiv"/>
    <form  suffix="es" tag="VBpres,ind,s-form"/>
    <form  suffix="es" tag="VBpret,ind,s-form"/>
    <form  suffix="es" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="s" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="s" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="s" tag="VBsup,s-form"/>
  </table>
  <table name="VB93" rads=".*(väg|ring|skyl)" fast="-">
    <!-- 3 members -->
    <form  suffix="a" tag="VBimper"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="" tag="VBimper"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="ad" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ade" tag="VBpret,ind,aktiv"/>
    <form  suffix="ade" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ade" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ade" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ade" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ades" tag="VBpret,ind,s-form"/>
    <form  suffix="ades" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ades" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ades" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ades" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ads" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ande" tag="VBpres_part,nom"/>
    <form  suffix="andes" tag="VBpres_part,gen"/>
    <form  suffix="ar" tag="VBpres,ind,aktiv"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="as" tag="VBpres,ind,s-form"/>
    <form  suffix="at" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="at" tag="VBsup,aktiv"/>
    <form  suffix="ats" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ats" tag="VBsup,s-form"/>
    <form  suffix="d" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="da" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="da" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="da" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="das" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="das" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="das" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="de" tag="VBpret,ind,aktiv"/>
    <form  suffix="de" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="des" tag="VBpret,ind,s-form"/>
    <form  suffix="des" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="er" tag="VBpres,ind,aktiv"/>
    <form  suffix="es" tag="VBpres,ind,s-form"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="t" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="t" tag="VBsup,aktiv"/>
    <form  suffix="ts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB94" rads=".*(vänt|önsk|skick)" fast="-">
    <!-- 3 members -->
    <form  suffix="a" tag="VBimper"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="ad" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ade" tag="VBpret,ind,aktiv"/>
    <form  suffix="ade" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ade" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ade" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ade" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ades" tag="VBpret,ind,s-form"/>
    <form  suffix="ades" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ades" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ades" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ades" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ads" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ande" tag="VBpres_part,nom"/>
    <form  suffix="andes" tag="VBpres_part,gen"/>
    <form  suffix="ar" tag="VBpres,ind,aktiv"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="as" tag="VBpres,ind,s-form"/>
    <form  suffix="at" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="at" tag="VBsup,aktiv"/>
    <form  suffix="ats" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ats" tag="VBsup,s-form"/>
    <form  suffix="e" tag="VBc"/>
    <form  suffix="e-" tag="VBc"/>
    <form  suffix="e-" tag="VBsms"/>
  </table>
  <table name="VB95" rads=".*(halvsov|småsov|sov)" fast="-">
    <!-- 3 members -->
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="" tag="VBimper"/>
    <form  suffix="" tag="VBpret,ind,aktiv"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="ande" tag="VBpres_part,nom"/>
    <form  suffix="andes" tag="VBpres_part,gen"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="e" tag="VBpres,konj,aktiv"/>
    <form  suffix="e" tag="VBpret,konj,aktiv"/>
    <form  suffix="er" tag="VBpres,ind,aktiv"/>
    <form  suffix="es" tag="VBpres,konj,s-form"/>
    <form  suffix="es" tag="VBpret,konj,s-form"/>
    <form  suffix="it" tag="VBsup,aktiv"/>
    <form  suffix="its" tag="VBsup,s-form"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="s" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB96" rads=".*(hytt|hött|mist)" fast="-">
    <!-- 3 members -->
    <form  suffix="a" tag="VBinf,aktiv"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="" tag="VBimper"/>
    <form  suffix="" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="" tag="VBsup,aktiv"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="a" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="a" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="a" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ad" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ade" tag="VBpret,ind,aktiv"/>
    <form  suffix="ade" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ade" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ade" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ade" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ades" tag="VBpret,ind,s-form"/>
    <form  suffix="ades" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ades" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ades" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ades" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ads" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ande" tag="VBpres_part,nom"/>
    <form  suffix="andes" tag="VBpres_part,gen"/>
    <form  suffix="as" tag="VBinf,s-form"/>
    <form  suffix="as" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="as" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="as" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="at" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="at" tag="VBsup,aktiv"/>
    <form  suffix="ats" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ats" tag="VBsup,s-form"/>
    <form  suffix="e" tag="VBpret,ind,aktiv"/>
    <form  suffix="e" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="er" tag="VBpres,ind,aktiv"/>
    <form  suffix="es" tag="VBpres,ind,s-form"/>
    <form  suffix="es" tag="VBpret,ind,s-form"/>
    <form  suffix="es" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="s" tag="VBpres,ind,s-form"/>
    <form  suffix="s" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="s" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="s" tag="VBsup,s-form"/>
  </table>
  <table name="VB97" rads=".*(erf|fortf|förf)" fast="-">
    <!-- 3 members -->
    <form  suffix="ara" tag="VBinf,aktiv"/>
    <form  suffix="ar" tag="VBc"/>
    <form  suffix="ar" tag="VBimper"/>
    <form  suffix="ar" tag="VBpres,ind,aktiv"/>
    <form  suffix="ar-" tag="VBc"/>
    <form  suffix="ar-" tag="VBsms"/>
    <form  suffix="arande" tag="VBpres_part,nom"/>
    <form  suffix="arandes" tag="VBpres_part,gen"/>
    <form  suffix="aras" tag="VBinf,s-form"/>
    <form  suffix="are" tag="VBpres,konj,aktiv"/>
    <form  suffix="ares" tag="VBpres,konj,s-form"/>
    <form  suffix="arit" tag="VBsup,aktiv"/>
    <form  suffix="arits" tag="VBsup,s-form"/>
    <form  suffix="ars" tag="VBpres,ind,s-form"/>
    <form  suffix="or" tag="VBpret,ind,aktiv"/>
    <form  suffix="ore" tag="VBpret,konj,aktiv"/>
    <form  suffix="ores" tag="VBpret,konj,s-form"/>
    <form  suffix="ors" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB98" rads=".*(bän|hän|avhän)" fast="-">
    <!-- 3 members -->
    <form  suffix="da" tag="VBinf,aktiv"/>
    <form  suffix="d" tag="VBc"/>
    <form  suffix="d" tag="VBimper"/>
    <form  suffix="d-" tag="VBc"/>
    <form  suffix="d-" tag="VBsms"/>
    <form  suffix="dande" tag="VBpres_part,nom"/>
    <form  suffix="dandes" tag="VBpres_part,gen"/>
    <form  suffix="das" tag="VBinf,s-form"/>
    <form  suffix="de" tag="VBpret,ind,aktiv"/>
    <form  suffix="der" tag="VBpres,ind,aktiv"/>
    <form  suffix="des" tag="VBpres,ind,s-form"/>
    <form  suffix="des" tag="VBpret,ind,s-form"/>
    <form  suffix="ds" tag="VBpres,ind,s-form"/>
    <form  suffix="t" tag="VBsup,aktiv"/>
    <form  suffix="ts" tag="VBsup,s-form"/>
  </table>
  <table name="VB99" rads=".*(b|tillb|omb)" fast="-">
    <!-- 3 members -->
    <form  suffix="e" tag="VBimper"/>
    <form  suffix="ad" tag="VBpret,ind,aktiv"/>
    <form  suffix="ade" tag="VBpret,konj,aktiv"/>
    <form  suffix="ades" tag="VBpret,konj,s-form"/>
    <form  suffix="ads" tag="VBpret,ind,s-form"/>
    <form  suffix="e" tag="VBinf,aktiv"/>
    <form  suffix="ed" tag="VBimper"/>
    <form  suffix="eda" tag="VBinf,aktiv"/>
    <form  suffix="edande" tag="VBpres_part,nom"/>
    <form  suffix="edandes" tag="VBpres_part,gen"/>
    <form  suffix="edas" tag="VBinf,s-form"/>
    <form  suffix="edd" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="edda" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="edda" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="edda" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="eddas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="eddas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="eddas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="edde" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="eddes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="edds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ede" tag="VBpres,konj,aktiv"/>
    <form  suffix="eder" tag="VBpres,ind,aktiv"/>
    <form  suffix="edes" tag="VBpres,ind,s-form"/>
    <form  suffix="edes" tag="VBpres,konj,s-form"/>
    <form  suffix="edj" tag="VBimper"/>
    <form  suffix="edja" tag="VBinf,aktiv"/>
    <form  suffix="edjande" tag="VBpres_part,nom"/>
    <form  suffix="edjandes" tag="VBpres_part,gen"/>
    <form  suffix="edjas" tag="VBinf,s-form"/>
    <form  suffix="edje" tag="VBpres,konj,aktiv"/>
    <form  suffix="edjer" tag="VBpres,ind,aktiv"/>
    <form  suffix="edjes" tag="VBpres,ind,s-form"/>
    <form  suffix="edjes" tag="VBpres,konj,s-form"/>
    <form  suffix="eds" tag="VBpres,ind,s-form"/>
    <form  suffix="eende" tag="VBpres_part,nom"/>
    <form  suffix="eendes" tag="VBpres_part,gen"/>
    <form  suffix="er" tag="VBpres,ind,aktiv"/>
    <form  suffix="es" tag="VBinf,s-form"/>
    <form  suffix="es" tag="VBpres,ind,s-form"/>
    <form  suffix="ett" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="ett" tag="VBsup,aktiv"/>
    <form  suffix="etts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="etts" tag="VBsup,s-form"/>
  </table>
  <table name="VB100" rads=".*(avv|v|hopv)" fast="-">
    <!-- 3 members -->
    <form  suffix="ika" tag="VBinf,aktiv"/>
    <form  suffix="ek" tag="VBpret,ind,aktiv"/>
    <form  suffix="eke" tag="VBpret,konj,aktiv"/>
    <form  suffix="ekes" tag="VBpret,konj,s-form"/>
    <form  suffix="eks" tag="VBpret,ind,s-form"/>
    <form  suffix="ik" tag="VBc"/>
    <form  suffix="ik" tag="VBimper"/>
    <form  suffix="ik-" tag="VBc"/>
    <form  suffix="ik-" tag="VBsms"/>
    <form  suffix="ikande" tag="VBpres_part,nom"/>
    <form  suffix="ikandes" tag="VBpres_part,gen"/>
    <form  suffix="ikas" tag="VBinf,s-form"/>
    <form  suffix="ike" tag="VBpres,konj,aktiv"/>
    <form  suffix="iken" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ikens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="iker" tag="VBpres,ind,aktiv"/>
    <form  suffix="ikes" tag="VBpres,ind,s-form"/>
    <form  suffix="ikes" tag="VBpres,konj,s-form"/>
    <form  suffix="iket" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="ikets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ikit" tag="VBsup,aktiv"/>
    <form  suffix="ikits" tag="VBsup,s-form"/>
    <form  suffix="ikna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ikna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ikna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="iknas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="iknas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="iknas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ikne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="iknes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="iks" tag="VBpres,ind,s-form"/>
    <form  suffix="ikt" tag="VBsup,aktiv"/>
    <form  suffix="ikts" tag="VBsup,s-form"/>
  </table>
  <table name="VB101" rads=".*(f|bef|föref)" fast="-">
    <!-- 3 members -->
    <form  suffix="innas" tag="VBinf,s-form"/>
    <form  suffix="anns" tag="VBpret,ind,s-form"/>
    <form  suffix="innes" tag="VBimper"/>
    <form  suffix="innes" tag="VBpres,ind,s-form"/>
    <form  suffix="inns" tag="VBimper"/>
    <form  suffix="inns" tag="VBpres,ind,s-form"/>
    <form  suffix="unnits" tag="VBsup,s-form"/>
  </table>
  <table name="VB102" rads=".*(försl|avsl|sl)" fast="-">
    <!-- 3 members -->
    <form  suffix="ita" tag="VBinf,aktiv"/>
    <form  suffix="et" tag="VBpret,ind,aktiv"/>
    <form  suffix="ets" tag="VBpret,ind,s-form"/>
    <form  suffix="it" tag="VBc"/>
    <form  suffix="it" tag="VBimper"/>
    <form  suffix="it-" tag="VBc"/>
    <form  suffix="it-" tag="VBsms"/>
    <form  suffix="itande" tag="VBpres_part,nom"/>
    <form  suffix="itandes" tag="VBpres_part,gen"/>
    <form  suffix="itas" tag="VBinf,s-form"/>
    <form  suffix="ite" tag="VBpres,konj,aktiv"/>
    <form  suffix="ite" tag="VBpret,konj,aktiv"/>
    <form  suffix="iten" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="itens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="iter" tag="VBpres,ind,aktiv"/>
    <form  suffix="ites" tag="VBpres,ind,s-form"/>
    <form  suffix="ites" tag="VBpres,konj,s-form"/>
    <form  suffix="ites" tag="VBpret,konj,s-form"/>
    <form  suffix="itet" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="itets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="itit" tag="VBsup,aktiv"/>
    <form  suffix="itits" tag="VBsup,s-form"/>
    <form  suffix="itna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="itna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="itna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="itnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="itnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="itnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="itne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="itnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="its" tag="VBpres,ind,s-form"/>
  </table>
  <table name="VB103" rads=".*(bekom|undkom|vidkom)" fast="-">
    <!-- 3 members -->
    <form  suffix="ma" tag="VBinf,aktiv"/>
    <form  suffix="" tag="VBc"/>
    <form  suffix="" tag="VBpret,ind,aktiv"/>
    <form  suffix="-" tag="VBc"/>
    <form  suffix="-" tag="VBsms"/>
    <form  suffix="mande" tag="VBpres_part,nom"/>
    <form  suffix="mandes" tag="VBpres_part,gen"/>
    <form  suffix="mas" tag="VBinf,s-form"/>
    <form  suffix="me" tag="VBpres,konj,aktiv"/>
    <form  suffix="me" tag="VBpret,konj,aktiv"/>
    <form  suffix="mer" tag="VBpres,ind,aktiv"/>
    <form  suffix="mes" tag="VBpres,konj,s-form"/>
    <form  suffix="mes" tag="VBpret,konj,s-form"/>
    <form  suffix="mit" tag="VBsup,aktiv"/>
    <form  suffix="mits" tag="VBsup,s-form"/>
    <form  suffix="s" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB104" rads=".*(slam|flim|glim)" fast="-">
    <!-- 3 members -->
    <form  suffix="ra" tag="VBimper"/>
    <form  suffix="mer" tag="VBc"/>
    <form  suffix="mer-" tag="VBc"/>
    <form  suffix="mer-" tag="VBsms"/>
    <form  suffix="ra" tag="VBinf,aktiv"/>
    <form  suffix="rade" tag="VBpret,ind,aktiv"/>
    <form  suffix="rades" tag="VBpret,ind,s-form"/>
    <form  suffix="rande" tag="VBpres_part,nom"/>
    <form  suffix="randes" tag="VBpres_part,gen"/>
    <form  suffix="rar" tag="VBpres,ind,aktiv"/>
    <form  suffix="ras" tag="VBinf,s-form"/>
    <form  suffix="ras" tag="VBpres,ind,s-form"/>
    <form  suffix="rat" tag="VBsup,aktiv"/>
    <form  suffix="rats" tag="VBsup,s-form"/>
  </table>
  <table name="VB105" rads=".*(illtj|tj|galltj)" fast="-">
    <!-- 3 members -->
    <form  suffix="uta" tag="VBinf,aktiv"/>
    <form  suffix="ut" tag="VBc"/>
    <form  suffix="ut" tag="VBimper"/>
    <form  suffix="ut-" tag="VBc"/>
    <form  suffix="ut-" tag="VBsms"/>
    <form  suffix="utande" tag="VBpres_part,nom"/>
    <form  suffix="utandes" tag="VBpres_part,gen"/>
    <form  suffix="utas" tag="VBinf,s-form"/>
    <form  suffix="ute" tag="VBpres,konj,aktiv"/>
    <form  suffix="uter" tag="VBpres,ind,aktiv"/>
    <form  suffix="utes" tag="VBpres,konj,s-form"/>
    <form  suffix="utit" tag="VBsup,aktiv"/>
    <form  suffix="utits" tag="VBsup,s-form"/>
    <form  suffix="uts" tag="VBpres,ind,s-form"/>
    <form  suffix="öt" tag="VBpret,ind,aktiv"/>
    <form  suffix="öte" tag="VBpret,konj,aktiv"/>
    <form  suffix="ötes" tag="VBpret,konj,s-form"/>
    <form  suffix="öts" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB106" rads=".*(insm|fl|sm)" fast="-">
    <!-- 3 members -->
    <form  suffix="yga" tag="VBinf,aktiv"/>
    <form  suffix="ugen" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="ugens" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="uget" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="ugets" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ugit" tag="VBsup,aktiv"/>
    <form  suffix="ugits" tag="VBsup,s-form"/>
    <form  suffix="ugna" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="ugna" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="ugna" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="ugnas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="ugnas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="ugnas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ugne" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="ugnes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="yg" tag="VBc"/>
    <form  suffix="yg" tag="VBimper"/>
    <form  suffix="yg-" tag="VBc"/>
    <form  suffix="yg-" tag="VBsms"/>
    <form  suffix="ygande" tag="VBpres_part,nom"/>
    <form  suffix="ygandes" tag="VBpres_part,gen"/>
    <form  suffix="ygas" tag="VBinf,s-form"/>
    <form  suffix="yge" tag="VBpres,konj,aktiv"/>
    <form  suffix="yger" tag="VBpres,ind,aktiv"/>
    <form  suffix="yges" tag="VBpres,ind,s-form"/>
    <form  suffix="yges" tag="VBpres,konj,s-form"/>
    <form  suffix="ygs" tag="VBpres,ind,s-form"/>
    <form  suffix="ög" tag="VBpret,ind,aktiv"/>
    <form  suffix="öge" tag="VBpret,konj,aktiv"/>
    <form  suffix="öges" tag="VBpret,konj,s-form"/>
    <form  suffix="ögs" tag="VBpret,ind,s-form"/>
  </table>
  <table name="VB107" rads=".*(efters|s|avs)" fast="-">
    <!-- 3 members -->
    <form  suffix="äga" tag="VBinf,aktiv"/>
    <form  suffix="a" tag="VBpret,ind,aktiv"/>
    <form  suffix="ade" tag="VBpret,ind,aktiv"/>
    <form  suffix="ades" tag="VBpret,ind,s-form"/>
    <form  suffix="agd" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="agda" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="agda" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="agda" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="agdas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="agdas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="agdas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="agde" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="agdes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="agds" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="agt" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="agt" tag="VBsup,aktiv"/>
    <form  suffix="agts" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="agts" tag="VBsup,s-form"/>
    <form  suffix="as" tag="VBpret,ind,s-form"/>
    <form  suffix="äg" tag="VBc"/>
    <form  suffix="äg" tag="VBimper"/>
    <form  suffix="äg-" tag="VBc"/>
    <form  suffix="äg-" tag="VBsms"/>
    <form  suffix="ägande" tag="VBpres_part,nom"/>
    <form  suffix="ägandes" tag="VBpres_part,gen"/>
    <form  suffix="ägas" tag="VBinf,s-form"/>
    <form  suffix="äger" tag="VBpres,ind,aktiv"/>
    <form  suffix="äges" tag="VBpres,ind,s-form"/>
    <form  suffix="ägs" tag="VBpres,ind,s-form"/>
    <form  suffix="äj" tag="VBc"/>
    <form  suffix="äj" tag="VBimper"/>
    <form  suffix="äj-" tag="VBc"/>
    <form  suffix="äj-" tag="VBsms"/>
    <form  suffix="äja" tag="VBinf,aktiv"/>
    <form  suffix="äjande" tag="VBpres_part,nom"/>
    <form  suffix="äjandes" tag="VBpres_part,gen"/>
    <form  suffix="äjas" tag="VBinf,s-form"/>
    <form  suffix="äjer" tag="VBpres,ind,aktiv"/>
    <form  suffix="äjes" tag="VBpres,ind,s-form"/>
    <form  suffix="äjs" tag="VBpres,ind,s-form"/>
  </table>
  <table name="VB108" rads=".*(tillv|v|avv)" fast="-">
    <!-- 3 members -->
    <form  suffix="änja" tag="VBinf,aktiv"/>
    <form  suffix="and" tag="VBpret_part,indef,sg,u,nom"/>
    <form  suffix="anda" tag="VBpret_part,def,pl,nom"/>
    <form  suffix="anda" tag="VBpret_part,def,sg,no_masc,nom"/>
    <form  suffix="anda" tag="VBpret_part,indef,pl,nom"/>
    <form  suffix="andas" tag="VBpret_part,def,pl,gen"/>
    <form  suffix="andas" tag="VBpret_part,def,sg,no_masc,gen"/>
    <form  suffix="andas" tag="VBpret_part,indef,pl,gen"/>
    <form  suffix="ande" tag="VBpret,ind,aktiv"/>
    <form  suffix="ande" tag="VBpret_part,def,sg,masc,nom"/>
    <form  suffix="andes" tag="VBpret,ind,s-form"/>
    <form  suffix="andes" tag="VBpret_part,def,sg,masc,gen"/>
    <form  suffix="ands" tag="VBpret_part,indef,sg,u,gen"/>
    <form  suffix="ant" tag="VBpret_part,indef,sg,n,nom"/>
    <form  suffix="ant" tag="VBsup,aktiv"/>
    <form  suffix="ants" tag="VBpret_part,indef,sg,n,gen"/>
    <form  suffix="ants" tag="VBsup,s-form"/>
    <form  suffix="änj" tag="VBc"/>
    <form  suffix="änj" tag="VBimper"/>
    <form  suffix="änj-" tag="VBc"/>
    <form  suffix="änj-" tag="VBsms"/>
    <form  suffix="änjande" tag="VBpres_part,nom"/>
    <form  suffix="änjandes" tag="VBpres_part,gen"/>
    <form  suffix="änjas" tag="VBinf,s-form"/>
    <form  suffix="änjer" tag="VBpres,ind,aktiv"/>
    <form  suffix="änjes" tag="VBpres,ind,s-form"/>
    <form  suffix="änjs" tag="VBpres,ind,s-form"/>
  </table>
  <table name="VBA1" rads=".*(obs\.|jfr|obs)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="VBAinvar"/>
    <form  suffix="-" tag="VBAc"/>
    <form  suffix="-" tag="VBAsms"/>
  </table>
</description>
